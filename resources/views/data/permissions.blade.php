@extends('layouts.admin')

@section('content-header')
@endsection

@section('content')
<?php
$message = (isset($message)) ? $message : null;
$permissions = (isset($permissions)) ? $permissions : null;
$selRoles = (isset($selRoles)) ? $selRoles : array();
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Role</div>
                <div class="panel-body">
                    {!! Form::open(['url' => url('/permissions', [], true),
                    'method' => 'POST', 'class' => 'form-horizontal',
                    'permission' => 'form']) !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('selRoles', 'Roles: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::select('selRoles', $selRoles) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('url', 'URL: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('url', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('permission', 'Permission (CRUD): ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('permission', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Create', ['class' => 'btn btn-primary fa fa-btn']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ ($message != null) ? ' has-error' : '' }}">
                        <div class="col-md-6">
                            @if ($message != null)
                            <span class="help-block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-10">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role Name</th>
                                    <th>URL</th>
                                    <th>Permission</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($permissions == null)
                                <tr>
                                    <td>*</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                    @foreach($permissions as $permission)
                                    <tr>
                                        <td>{!! $permission->id !!}</td>
                                        <td>{!! $permission->name !!}</td>
                                        <td>{!! $permission->url !!}</td>
                                        <td>{!! $permission->permission !!}</td>
                                        <td>
                                            {!! Form::open(['url' =>url('/permissions/' . $permission->id, [], true), 'method' => 'delete', 'id' => 'form_' . $permission->id]) !!}
                                            <a href='javascript:void(0);'
                                               class='glyphicon glyphicon-trash'
                                               onclick='document.getElementById("form_" + {!! $permission->id !!}).submit();'></a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
