@extends('layouts.admin')

@section('content')
<?php
$message = (isset($message)) ? $message : null;
$dealerships = (isset($dealerships)) ? $dealerships : null;
?>
<script>
    function enableDMSUsername(dmsSelection)
    {
        if (dmsSelection.value == "None")
        {
            document.getElementById("dms_username").disabled = true;
        }
        else
        {
            document.getElementById("dms_username").disabled = false;
        }
    }
</script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Role</div>
                <div class="panel-body">
                    @if(isset($dealership->id))
                        {!! Form::open(['url' => url('/dealerships/' . $dealership->id, [], true),
                        'method' => 'PUT', 'class' => 'form-horizontal',
                        'role' => 'form']) !!}
                        {!! Form::hidden('id', $dealership->id) !!}
                    @else
                        {!! Form::open(['url' => url('/dealerships', [], true),
                        'method' => 'POST', 'class' => 'form-horizontal',
                        'role' => 'form']) !!}
                    @endif

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Dealership Name: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('name',
                                (isset($dealership->name) ? $dealership->name : '' ),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('address_line1') ? ' has-error' : '' }}">
                        {!! Form::label('address_line1', 'Address Line 1: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('address_line1',
                                (isset($dealership->address_line1) ? $dealership->address_line1 : ''),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('address_line2') ? ' has-error' : '' }}">
                        {!! Form::label('address_line2', 'Address Line 2: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('address_line2',
                                (isset($dealership->address_line2) ? $dealership->address_line2 : ''),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        {!! Form::label('city', 'City: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('city',
                                (isset($dealership->city) ? $dealership->city : ''),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                        {!! Form::label('state', 'State: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            <span class='form-control'>{!! Form::select('state', ['  ' => 'Select a state',
                                'AL' => 'Alabama',
                                'AK' => 'Alaska',
                                'AZ' => 'Arizona',
                                'AR' => 'Arkansas',
                                'CA' => 'California',
                                'CO' => 'Colorado',
                                'CT' => 'Connecticut',
                                'DE' => 'Delaware',
                                'DC' => 'District Of Columbia',
                                'FL' => 'Florida',
                                'GA' => 'Georgia',
                                'HI' => 'Hawaii',
                                'ID' => 'Idaho',
                                'IL' => 'Illinois',
                                'IN' => 'Indiana',
                                'IA' => 'Iowa',
                                'KS' => 'Kansas',
                                'KY' => 'Kentucky',
                                'LA' => 'Louisiana',
                                'ME' => 'Maine',
                                'MD' => 'Maryland',
                                'MA' => 'Massachusetts',
                                'MI' => 'Michigan',
                                'MN' => 'Minnesota',
                                'MS' => 'Mississippi',
                                'MO' => 'Missouri',
                                'MT' => 'Montana',
                                'NE' => 'Nebraska',
                                'NV' => 'Nevada',
                                'NH' => 'New Hampshire',
                                'NJ' => 'New Jersey',
                                'NM' => 'New Mexico',
                                'NY' => 'New York',
                                'NC' => 'North Carolina',
                                'ND' => 'North Dakota',
                                'OH' => 'Ohio',
                                'OK' => 'Oklahoma',
                                'OR' => 'Oregon',
                                'PA' => 'Pennsylvania',
                                'RI' => 'Rhode Island',
                                'SC' => 'South Carolina',
                                'SD' => 'South Dakota',
                                'TN' => 'Tennessee',
                                'TX' => 'Texas',
                                'UT' => 'Utah',
                                'VT' => 'Vermont',
                                'VA' => 'Virginia',
                                'WA' => 'Washington',
                                'WV' => 'West Virginia',
                                'WI' => 'Wisconsin',
                                'WY' => 'Wyoming'],
                                (isset($dealership->state) ? $dealership->state : null)) !!}</span>
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                        {!! Form::label('zip', 'Zip: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('zip',
                                (isset($dealership->zip) ? $dealership->zip : ''),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('merchant_account_id') ? ' has-error' : '' }}">
                        {!! Form::label('merchant_account_id', 'Merchant Account #: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('merchant_account_id',
                                (isset($dealership->merchant_account_id) ? $dealership->merchant_account_id : ''),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('dms') ? ' has-error' : '' }}">
                        {!! Form::label('dms_vendor', 'Credit Service Provider: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            <span class='form-control' onchange='enableDMSUsername(this);'>{!! Form::select('credit_provider', ['  ' => 'None',
                                '700CREDIT' => '700 Credit',
                                'CBC' => 'CBC Credit Services',
                                'EQUIFAX' => 'Equifax',
                                'EXPERIAN' => 'Experian',
                                'IPRECHECK' => 'iPreCheck',
                                'TRANSUNION' => 'TransUnion'], (isset($dealership->credit_provider) ? $dealership->credit_provider : null)) !!}</span>
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('credit_username') ? ' has-error' : '' }}">
                        {!! Form::label('credit_username', 'Credit Service Username: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('credit_username',
                                (isset($dealership->credit_username) ? $dealership->credit_username : ''),
                                ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('credit_password') ? ' has-error' : '' }}">
                        {!! Form::label('credit_password', 'Credit Service Password: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::password('credit_password', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('credit_password_confirmation') ? ' has-error' : '' }}">
                        {!! Form::label('credit_password_confirmation', 'Confirm Password: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::password('credit_password_confirmation', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('dms') ? ' has-error' : '' }}">
                        {!! Form::label('dms_vendor', 'Dealer Management System: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            <span class='form-control' onchange='enableDMSUsername(this);'>{!! Form::select('dms_vendor', ['  ' => 'None',
                                'CDK' => 'CDK',
                                'COX' => 'COX',
                                'REYNOLDS' => 'REYNOLDS'], (isset($dealership->dms_vendor) ? $dealership->dms_vendor : null)) !!}</span>
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('dms_username') ? ' has-error' : '' }}">
                        {!! Form::label('dms_username', 'DMS Username / ID: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('dms_username',
                                (isset($dealership->dms_username) ? $dealership->dms_username : ''),
                                ((null !== isset($dealership->dms_username)) || (null !== isset($dealership->dms_vendor))) ? 
                                ['class' => 'form-control'] :
                                ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ (count($errors) > 0) ? ' has-error' : '' }}">
                            @if(count($errors) > 0)
                                {!! Form::label('', 'Red fields are required.',
                                    ['class' => 'col-md-4 control-label']) !!}
                            @else
                                <span class='col-md-4'>&nbsp;</span>
                            @endif
                        <div class="col-md-6">
                            @if(isset($dealership->id))
                                {!! Form::submit('Update', ['class' => 'btn btn-primary fa fa-btn']) !!}
                                &nbsp;
                                <a href="{!! url('/dealerships', [], true) !!}">Create New Dealership</a>
                            @else
                                {!! Form::submit('Create', ['class' => 'btn btn-primary fa fa-btn']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ ($message != null) ? ' has-error' : '' }}">
                        <div class="col-md-6">
                            @if ($message != null)
                            <span class="help-block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Dealership</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Zip</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($dealerships == null)
                                <tr>
                                    <td>*</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                    @foreach($dealerships as $dealership)
                                    <tr>
                                        <td>{!! $dealership->id !!}</td>
                                        <td>{!! $dealership->name !!}</td>
                                        <td>{!! $dealership->address_line1 !!}<br />
                                            {!! $dealership->address_line2 !!}</td>
                                        <td>{!! $dealership->city !!}</td>
                                        <td>{!! $dealership->state !!}</td>
                                        <td>{!! $dealership->zip !!}</td>
                                        <td style="width: 5%;">
                                            @if ($currentRole->name == \App\Role\Names::Administrator)
                                                {!! Form::open(['url' =>url('/dealerships/' . $dealership->id . '/edit', [], true), 'method' => 'get', 'id' => 'form_edit_' . $dealership->id]) !!}
                                                <a href='javascript:void(0);'
                                                   class='glyphicon glyphicon-pencil'
                                                   onclick='document.getElementById("form_edit_" + {!! $dealership->id !!}).submit();'></a>
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($currentRole->name == \App\Role\Names::Administrator)
                                                {!! Form::open(['url' =>url('/dealerships/' . $dealership->id, [], true), 'method' => 'delete', 'id' => 'form_' . $dealership->id]) !!}
                                                <a href='javascript:void(0);'
                                                   class='glyphicon glyphicon-trash'
                                                   onclick='document.getElementById("form_" + {!! $dealership->id !!}).submit();'></a>
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
