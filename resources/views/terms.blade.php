@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="/css/one-page-wonder.css" rel="stylesheet">

<style>
    .header-image
    {
        background-image: url('images/terms.jpg');
        background-repeat: repeat;
        background-size: initial;
    }
    div.paragraph
    {
        margin-bottom: 10px;
    }
    div.bold-underlined
    {
        font-weight: bold;
        text-decoration: underline;
        margin-bottom: 10px;
    }
    div.bold
    {
        font-weight: bold;
        margin-bottom: 10px;
    }
</style>
<!-- Full Width Image Header -->
<header class="header-image">
    <div class="headline">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Terms of Use</div>
                        <div class="panel-body text-left">
                            <h3>SERVISELL Privacy Policy and Terms of Use</h3>

                            <h4>Terms of Use</h4>

                            <div class="paragraph">
                                Copyright 2016-2017, BSD Partners, LLC dba SERVISELL (“SERVISELL”). All
                                rights reserved. All text, images, graphics, animation, videos, music, sounds, and
                                other materials on this website (&quot;site&quot;) are subject to the copyrights and other
                                intellectual property rights of SERVISELL, its affiliated companies, and its
                                licensors. SERVISELL owns the copyrights in the selection, coordination, and
                                arrangement of the materials on this site. These materials may not be copied for
                                commercial use or distribution, nor may these materials be modified or reposted
                                to other sites.
                            </div>

                            <h4>SERVISELL Websites Privacy Policy</h4>

                            <div class="bold-underlined">Effective February 20 th , 2017</div></p>

                            <div class="bold">
                                Statement
                            </div>

                            <div class="paragraph">
                                SERVISELL respects the privacy and information of all SERVISELL Website
                                users (“Users”) and their respective customers.  This Policy sets forth how
                                SERVISELL will manage individually identifiable information and other
                                information about its Users (“User Information”) and data that Users input, store
                                or transmit through the use of the SERVISELL.com website in the normal course
                                of business about their respective customers (“Customer Information”).  Neither
                                User Information nor Customer Information is ever willingly disclosed to any third
                                party without first receiving express permission or as authorized by the applicable
                                Dealer Subscription Agreement or this “Terms of Use Policy” as incorporated into
                                these agreements and in compliance with all applicable laws and regulations.
                            </div>
                            <div class="bold">
                                Cookies
                            </div>
                            <div class="paragraph">
                                SERVISELL will collect information called Cookies that aids the User in their
                                access and use of SERVISELL.com.
                            </div>
                            <div class="bold">What are Cookies?</div>
                            <div class="paragraph">
                                Cookies are small amounts of data that a website can send to a web browser on
                                a User’s machine, e.g., personal computer or laptop, tablet, mobile device, ect.,
                                (“Computing Device”) where it is stored for some period of time. A cookie may be
                                used to authenticate the User or the Computing Device accessing the website.
                                Information gathered from using cookies may include general information about
                                the Computing Device used to access the site.
                            </div>
                            <div class="bold">Types of Cookies</div>
                            <div class="paragraph">Cookies can be set to expire:</div>
                            <ul>
                                <li>On a specified date</li>
                                <li>After a specific period of time</li>
                                <li>When a transaction has been completed</li>
                                <li>Or when a user closes his/her browser</li>
                            </ul>
                            <div class="paragraph">
                                A cookie that is erased from memory when a visitor’s browser closes is called a
                                “session” cookie. Cookies that expire based on a time set by the Web Server are
                                called “persistent” cookies.
                            </div>
                            <div class="paragraph">
                                Persistent cookies may be used in some cases, for example, to authenticate
                                Computing Devices that have accessed the website previously or otherwise
                                recognize when a visitor returns to a web site. The web browser will send the
                                cookie information from the initial visit back to the website. This is useful to the
                                visitor if he or she has established secure access or a password at a particular
                                website and wants to be identified by that website to perform a function or
                                transaction.
                            </div>
                            <div class="paragraph">
                                SERVISELL will use both persistent and session cookies to implement a method
                                of Muti-Factor Authentication. The method to be used is Machine Authentication.
                            </div>
                            <div class="paragraph">
                                Machine authentication provides validation of the User’s Computing Device in a
                                way that secures against a variety of threats while minimizing User impact. This
                                is an especially attractive method where Users typically access their account
                                from a regular set of Computing Devices, allowing for authentication to be
                                performed without any noticeable impact to the User experience.
                            </div>
                            <div class="bold">
                                Why we Collect Information
                            </div>
                            <div class="paragraph">
                                SERVISELL will collect certain User Information for the purpose to establish:
                                login credentials and communication through SERVISELL.com with the User, as
                                well as information that will aide User access to SERVISELL.com and other
                                information required to provide the services authorized by the Dealer
                                Subscription Agreement as incorporated into these agreements.
                            </div>
                            <div class="paragraph">
                                SERVISELL does not independently collect dealer or finance source Customer
                                Information.
                            </div>
                            <div class="bold">
                                Notice of the Privacy Policy and Your Consent to the Privacy Policy
                            </div>
                            <div class="paragraph">
                                SERVISELL provides notice of its Privacy Policy through its web based systems
                                (“SERVISELL.com”). Each page of the SERVISELL Website contains a hypertext
                                link to the SERVISELL Privacy Policy and is available to all visitors or Users at
                                any time while accessing the SERVISELL Website by clicking on the Privacy
                                Policy link.
                            </div>
                            <div class="paragraph">
                                By accessing  the SERVISELL Website, you consent to the collection and use of
                                your User Information as we have outlined in this policy and use of your
                                Customer Information to provide the services authorized by the applicable Dealer
                                Subscription Agreement as incorporated into these agreements.  SERVISELL
                                may decide to change this Privacy Policy from time to time. When we do, we will
                                post those changes on the SERVISELL Website so that you are always aware of
                                the information we collect, how we use it, and under what circumstances we
                                disclose it.
                            </div>
                            <div class="bold">
                                What Information Do We Collect?
                            </div>
                            <div class="paragraph">
                                SERVISELL collects User Information and processes Customer Information input
                                by its Users as required to provide the services authorized by the applicable
                                Dealer Subscription Agreement as incorporated into these agreements.
                                SERVISELL does not collect any unique information about Users (such as your
                                name, email address, etc.) except when you specifically and knowingly provide
                                such information.
                            </div>
                            <div class="bold">
                                Use, Retention and Disposal
                            </div>
                            <div class="paragraph">
                                SERVISELL will only use and retain Customer Information or User Information to
                                provide the services (e.g., process the customer information through
                                SERVISELL.com or aide User access to SERVISELL.com) authorized by the
                                applicable Dealer Subscription Agreement as incorporated into these
                                agreements.
                            </div>
                            <div class="paragraph">
                                Disposal of Customer Information is performed in a secure manner in compliance
                                with all applicable laws and regulations.
                            </div>
                            <div class="bold">
                                Access
                            </div>
                            <div class="paragraph">
                                Access to Customer Information or User Information through the SERVISELL
                                Website is restricted to SERVISELL and its Users. SERVISELL does not grant
                                third parties access to Customer Information or User Information without the
                                User's express authorization or specifically authorized by the applicable Dealer
                                Subscription Agreement and shall do so only in compliance with all applicable
                                laws and regulations.
                            </div>
                            <div class="bold">
                                With Whom Does SERVISELL Share Information
                            </div>
                            <div class="paragraph">
                                SERVISELL will not share User Information or Customer Information with third
                                parties other than in the performance of those services without the User&#39;s
                                express authorization or as authorized by the applicable Dealer Subscription
                                Agreement as incorporated into these agreements and shall do so only in
                                compliance with all applicable laws and regulations. We will, however, release
                                specific information if required to do so in order to comply with any valid legal
                                process such as a search warrant, subpoena, statute, or court order.
                            </div>
                            <div class="bold">
                                Security for User and Customer Information
                            </div>
                            <div class="paragraph">
                                SERVISELL has agreed with each of its dealer customers to maintain
                                commercially reasonable physical, electronic, and procedural controls and
                                safeguards designed to comply with all applicable laws and regulations to protect
                                the Customer Information received from dealer from unauthorized disclosure. 
                                The controls include, but are not limited to, maintaining appropriate safeguards to
                                restrict access to the Customer Information to those employees, agents, or
                                service providers of SERVISELL who need such information to provide the
                                services authorized by the applicable Dealer Subscription Agreement as
                                incorporated into these agreements.  For data disclosed in electronic form to
                                SERVISELL, the safeguards also include electronic barriers (e.g., firewalls or
                                similar barriers) and password-protected access and for information disclosed in
                                written form, such safeguards include secured storage.
                            </div>
                            <div class="bold">
                                Quality
                            </div>
                            <div class="paragraph">
                                All Customer Information and User Information is input into SERVISELL Website
                                by its Users and is not independently altered at any time by SERVISELL’s
                                employees, agents, or service providers unless expressly authorized by the
                                respective dealer;  therefore the quality of the Customer Information and User
                                Information is the responsibility of such dealers and finance sources.
                            </div>
                            <div class="bold">
                                Who Can I Ask If I Have Additional Questions?
                            </div>
                            <div class="paragraph">
                                SERVISELL monitors the SERVISELL Website usage for all purposes deemed
                                necessary by SERVISELL, including without limitation, to ensure proper working
                                order, appropriate use, and the security of data.
                            </div>
                            <div class="paragraph">
                                Feel free to contact us any time and we&#39;ll answer any additional questions you
                                may have. Our Dealer Support email address is support@SERVISELL.com.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
