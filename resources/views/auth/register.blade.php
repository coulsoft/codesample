@extends('layouts.admin')

@section('content')
<?php
$message = isset($message) ? $message : '';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    @if(isset($user->id))
                        {!! Form::open(['url' =>url($baseURL . '/' . $user->id, [], true), 'method' => 'put', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                        {!! Form::hidden('id', $user->id) !!}
                    @else
                    <form class="form-horizontal" role="form" method="POST" action="{{ $baseURL }}">
                        {!! csrf_field() !!}
                    @endif

                        @if(isset($selDealerships) && (count($selDealerships) > 0))
                        <div class="form-group{{ $errors->has('selDealerships') ? ' has-error' : '' }}">
                            {!! Form::label('selDealerships', 'Dealership: ',
                            ['class' => 'col-md-4 control-label']) !!}

                            <div class="col-md-6">
                                {!! Form::select('selDealerships', $selDealerships,
                                    isset($dealership_id) ? $dealership_id : "",
                                    ['onChange' => 'window.location="' . $baseURL .  '/" + this.value + "/byDealership";']) !!}
                            </div>
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('selRoles') ? ' has-error' : '' }}">
                            {!! Form::label('selRoles', 'Role: ',
                            ['class' => 'col-md-4 control-label']) !!}

                            <div class="col-md-6">
                                {!! Form::select('selRoles', $selRoles, isset($user->role_id) ? $user->role_id : "") !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" value="{{ isset($user->title) ? $user->title : '' }}">

                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="first_name" value="{{ isset($user->first_name) ? $user->first_name : '' }}">

                                @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="last_name" value="{{ isset($user->last_name) ? $user->last_name : '' }}">

                                @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ isset($user->email) ? $user->email : '' }}">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md4 control-label">{{ $message }}</label>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>
                                    @if(isset($user->id))
                                    Update
                                    @else
                                    Register
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>
                    <hr />
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Title</th>
                                    @if ($currentRole->name == \App\Role\Names::Administrator)
                                    <th>Role</th>
                                    @endif
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($users == null)
                                <tr>
                                    <td>*</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    @if ($currentRole->name == \App\Role\Names::Administrator)
                                    <td>Role</td>
                                    @endif
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{!! $user->id !!}</td>
                                        <td>{!! $user->name !!}</td>
                                        <td>{!! $user->title !!}</td>
                                        @if ($currentRole->name == \App\Role\Names::Administrator)
                                        <td>{!! $user->role !!}</td>
                                        @endif
                                        <td style="width: 5%;">
                                            @if ($currentRole->name == \App\Role\Names::Administrator)
                                                {!! Form::open(['url' =>url($baseURL .'/' . $user->id . '/edit', [], true), 'method' => 'get', 'id' => 'form_edit_' . $user->id]) !!}
                                                <a href='javascript:void(0);'
                                                   class='glyphicon glyphicon-pencil'
                                                   onclick='document.getElementById("form_edit_" + {!! $user->id !!}).submit();'></a>
                                                {!! Form::close() !!}
                                                }
                                            @endif
                                        </td>
                                        <td style="width: 5%;">
                                            @if ($currentRole->name == \App\Role\Names::Administrator)
                                                {!! Form::open(['url' =>url($baseURL . '/' . $user->id, [], true), 'method' => 'delete', 'id' => 'form_delete_' . $user->id]) !!}
                                                <a href='javascript:void(0);'
                                                   class='glyphicon glyphicon-trash'
                                                   onclick='document.getElementById("form_delete_" + {!! $user->id !!}).submit();'></a>
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
