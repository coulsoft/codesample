Click here to reset your password: <a href="{{ $link = url('password/reset', $token, true).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
