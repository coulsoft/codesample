@extends('layouts.single')

@section('content')
    <!-- Custom CSS -->
    <link href="/css/one-page-wonder.css" rel="stylesheet">
    
    <!-- Full Width Image Header -->
    <header class="header-image">
        <div class="headline">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {!! Form::open(['url' => url('/password/email', [], true),
                        'method' => 'POST', 'class' => 'form-horizontal',
                        'role' => 'form']) !!}
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('email', 'E-Mail Address',
                            ['class' => 'col-md-4 control-label']) !!}

                            <div class="col-md-6">
                                {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                                
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ (count($errors) > 0) ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                {!! Form::submit('Send Password Reset Link', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
           </div>
    </header>
@endsection
