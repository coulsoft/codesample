@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/nada') }}">Nada Search</a></li>
                        <li><a href="{{ url('/service') }}">Service</a></li>
                        <li><a href="{{ url('/sales') }}">Sales</a></li>
                        <li><a href="{{ url('/roles/create') }}">Roles</a></li>
                        <li><a href="{{ url('/permissions/create') }}">Permissions</a></li>
                        <li><a href="{{ url('/profiles') }}">Profiles</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection