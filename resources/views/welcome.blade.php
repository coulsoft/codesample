@extends('layouts.app')

@section('content')
<style>
    .cont
</style>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <div class="container" style="text-align: center; display: table-cell; vertical-align: middle;">
                        <h1>SERVISELL</h1>
                        <p class="lead">Converting Service Customers to Sales Customers in the blink of an eye…</p>
                        <div>
                            <p>Ask yourself this one question: What is my daily average RO count?</p>

                            <p>Now add that number to your total UNIT SALES for the month and imagine how that would feel.</p>

                            <p>THAT’S WHAT SERVISELL WILL DO FOR YOU</p>

                            <p><a href="{!! url('/contact', [], true) !!}">CLICK HERE TO LEARN MORE</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
