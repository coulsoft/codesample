@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="css/full-slider.css" rel="stylesheet">

<!-- Full Page Image Background Carousel Header -->
<div class="">
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="14000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <img src="images/intro.png" alt="Chania" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        Every day you see 50 or more service customers
                        running through your Service Drive 25 OR MORE
                        of those customers are in a favorable buying
                        position and you don’t even know it.
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/technology_dim.png" alt="Chania" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        By utilizing the SERVISELL Instant Pre-Screen,
                        Pre-Qualification Sales Alert Tool AMAZING
                        THINGS START TO HAPPEN
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/data_dim.png" alt="Flower" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        Using only the service customers Name, Address,
                        VIN and Mileage, NADA and Credit Bureau Data are
                        INSTANTLY accessed and organized
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/data_entry_dim.png" alt="Flower" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        Within Seconds of Service Department Data Entry
                        your Sales Manager receives a<br/ >
                            <br />
                        SALES ALERT<br />
                        <br />
                        Outlining the QUALIFIED SALES OPPORTUNITY
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/finance_dim.png" alt="Flower" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        ONLY ON &nbsp;&nbsp; Credit-Worthy Customers<br />
                        WITH &nbsp;&nbsp; Equity in their service vehicle.<br />
                        <br />
                        Your Sales Manager INSTANTLY receives the following information:<br />
                        <br />
                        YEAR, MAKE AND MODEL OF THE SERVICE VEHICLE<br />
                        CREDIT QUALITY (You set the minimum credit score to access)<br />
                        TRADE EQUITY (Clean Trade less Balance Owed)<br />
                        NUMBER OF PAYMENTS MADE<br />
                        CURRENT INTEREST RATE ON LOAN<br />
                        CURRENT PAYMENT<br />
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/numbers_dim.png" alt="Flower" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        Within SECONDS of the Service Manager submitting
                        the data, your Sales Manager knows that there is
                        VEHICLE EQUITY sitting in the Service Drive AND
                        a CREDIT-WORTHY CUSTOMER waiting
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/future_dim.png" alt="Flower" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        With each SALES ALERT, your Sales Manager has the opportunity to:<br />
                        <br />
                        <p style="left-padding: 8px;">
                            Sell the Customer a BETTER VEHICLE<br />
                            Take in a QUALITY TRADE<br />
                            Improve the CUSTOMERS INTEREST RATE<br />
                            Improve the CUSTOMERS PAYMENT<br />
                            Increase CUSTOMER SATISFACTION<br />
                        </p>
                    </h3>
                </div>
            </div>

            <div class="item">
                <img src="images/imagine_dim.png" alt="Flower" width="100%" height="345">
                <div class="carousel-caption">
                    <h3 class="featurette-heading">
                        Imagine this process happening 25 OR MORE times a day.<br />
                        Imagine improving your MONTHLY UNIT SALES by<br />
                        50% of your AVERAGE DAILY R.O. COUNT<br />
                        That’s what SERVISELL can do for YOU<br />
                        <br />
                        <a href="{!! url('/contact') !!}" style="color: gold;">Click here</a> to have a SERVISELL representative contact you<br />
                        within 24 hours to get the ball rolling.<br />
                        <br />
                        HAPPY SELLING!!
                    </h3>
                </div>
            </div>

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
@endsection