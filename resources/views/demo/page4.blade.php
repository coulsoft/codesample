<? php
use Auth;
use App\Profile;
use App\Role;

$currentUser = isset($currentUser) ? $currentUser : Auth::user();
$currentRole = isset($currentRole) ? $currentRole : Profile::getCurrentRole();
$currentProfile = isset($currentProfile) ? $currentProfile : Profile::getCurrentProfile();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Servisell</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/dist/css/skins/skin-servisell.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-93252040-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body class="hold-transition skin-servisell sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="/images/servisell_mini.png" /></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="/images/logo_horizontal.png" /></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="/logout">
                                    <span class="hidden-xs">Logout</span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                &nbsp;
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image" style="padding-top: 10px; padding-bottom: 10px; padding-left: 1px; padding-right: 1px;">
                            &nbsp;
                        </div>
                        <div class="pull-left info">
                            <p>Sam Arlington</p>
                            <p>Sales Manager</p>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="#"><i class="fa fa-car"></i> <span>Sales</span></a></li>
                    </ul>
                    <ul class="sidebar-menu">
                        <li class="header">Documentation</li>
                        <!--<li><a href="{!! url('/downloads/ServisellTrainingGuide.pdf', [], true) !!}">Training Guide</a></li>-->
                        <li><a href="{!! url('/demo', [], true) !!}">*** Restart Demo ***</a></li>
                    </ul>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>

                <!-- Main content -->
                <section class="content">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
                    <script>
            var intArrow = null;
            function showArrow()
            {
                clearInterval(intArrow);
                spnArrow = document.getElementById("spnArrow");
                if (spnArrow != null)
                {
                    spnArrow.innerHTML = "&nbsp;<img src=\"/images/ClickHereBlink.gif\" />"
                    location.hash = "#spnArrow";
                }
            }
            intArrow = setInterval(showArrow, 7000);
                    </script>
                    <style>
                        h5
                        {
                            font-family: "Myriad Pro", Arial, Helvetica, sans-serif;
                            font-weight: normal;
                            font-size: 12px;
                            margin: 0px;
                            border: none;
                            color: rgb(0, 0, 0);
                        }
                        .nada
                        {
                            color: #144ab0;
                        }
                    </style>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Sales Opportunities</div>
                                    <div class="panel-body">
                                        <iframe id="tradeAlert" name="tradeAlert" height=0 width=0 style="visibility: hidden;" src="/demo/5"></iframe>
                                        <div class="col-lg-12 user-panel">
                                            <form><input name="_token" type="hidden" value="ymiNBOiSZXboikU1OI8DSrJWb179ZETJpwnsE2Yw">
                                                <div class="row">
                                                    <div class="text-center">
                                                        <img src="/images/demo4image1.png" style="width: 75%; width: 625"/>
                                                        <div class="row" style="background-color: lightgray; margin: -5px 0px 20px 140px; width: 75%; font-family: Myriad Pro, Arial, Helvetica, sans-serif;">
                                                            <div class="QSautoSummary" style="padding: 0px; width: 651px; margin-left: 24px;">
                                                                <h2 style="margin: 0px; padding: 0px; height: 26px; line-height: 26px; text-align: center; color: rgb(255, 255, 255); font-family: Myriad Pro, Arial, Helvetica, sans-serif; font-size: 15px; font-weight: bold; text-shadow: rgb(76, 76, 76) 1px 1px 1px; letter-spacing: 0.5px; background-color: rgb(245, 111, 61);">
                                                                    Service Vehicle Summary</h2>
                                                                <ul class="autoSummaryHead" style="margin: 0px; padding: 10px 0px 10px 35px; list-style: none; float: left; color: rgb(0, 0, 0); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">
                                                                    <li class="first" style="margin: 0px; padding: 0px; list-style: none; float: left; width: 340px;">
                                                                        <label style="margin: 0px 10px 0px 0px; padding: 0px 0px 0px 20px; float: left; font-size: 15px; font-weight: bold; color: rgb(0, 0, 0); line-height: 25px;">Vehicle in Service:
                                                                        </label>
                                                                        <span style="margin: 0px; padding: 0px; float: left; font-size: 18px; color: rgb(55, 102, 186);">
                                                                            2014&nbsp;JEEP &nbsp;Grand Cherokee-V6
                                                                        </span>
                                                                    </li>
                                                                </ul>
                                                                <div class="autoSummaryDetail" style="margin: 0px 0px 20px 0px; padding: 0px 22px; background-color: white; height: 160px;">
                                                                    <table class="summaryTab" style="margin: 0px 0px 20px; padding: 0px; border-width: 1px 0px 1px 1px; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(190, 190, 190); border-bottom-color: rgb(190, 190, 190); border-left-color: rgb(190, 190, 190); border-image: initial; width: 606px; float: left; border-right-style: initial; border-right-color: initial; background: rgb(247, 247, 247);"

                                                                           cellpadding="0" cellspacing="0">
                                                                        <tbody style="margin: 0px; padding: 0px;">
                                                                            <tr style="margin: 0px; padding: 0px;">
                                                                                <th colspan="3" style="margin: 0px; padding: 0px 0px 0px 10px; border-top: 1px solid rgb(255, 255, 255); height: 23px; font-weight: bold; text-align: left; font-size: 13px; color: rgb(0, 0, 0); border-right: 1px solid rgb(190, 190, 190); border-bottom: 1px solid rgb(190, 190, 190); background-color: rgb(228, 228, 228);">
                                                                                    Current NADA Values</th>
                                                                            </tr>
                                                                            <tr style="margin: 0px; padding: 0px;">
                                                                                <td style="margin: 0px; padding: 15px 10px 2px; border-right: 1px solid rgb(190, 190, 190); font-size: 13px; color: rgb(0, 0, 0); width: 209px;">
                                                                                    <table class="summaryTabInn" style="margin: 0px; padding: 0px; width: 584px; height: 50px;"

                                                                                           cellpadding="0" cellspacing="0">
                                                                                        <tbody style="margin: 0px; padding: 0px;">
                                                                                            <tr style="margin: 0px; padding: 0px;">
                                                                                                <td>
                                                                                                    <h5>Mileage:</h5>
                                                                                                    <span class="nada">48,582</span></td>
                                                                                                <td>
                                                                                                    <h5>Trade:</h5>
                                                                                                    <span class="nada">$25,000.00</span></td>
                                                                                                <td>
                                                                                                    <h5>Retail:</h5>
                                                                                                    <span class="nada">$27,575.00</span></td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                            <div class="row">
                                                <div class="form-group">
                                                    <span class="col-md-2 control-label">&nbsp;</span>
                                                    <div class="col-md-3">
                                                        <span class="">&nbsp;</span>
                                                    </div>
                                                    <span class="col-md2 control-label">&nbsp;</span>
                                                    <div class="col-md-3">
                                                        <a href='#' onclick='if (window.open("/demo/5") != null) {
                                                                    window.location = "/demo/6";
                                                                }
                                                                return false;'>
                                                            <button><span style='color: black;'>Trade Alert</span></button><span id="spnArrow"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                        <div class="col-lg-12 box">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr style="text-align: center; font-weight: bold;">
                                                        <td colspan="5">&nbsp;</td>
                                                        <td style="width: 15%" colspan="3">Trade Probability</td>
                                                        <td colspan="3">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th style="width: 5%">Auto Inquires</th>
                                                        <th style="width: 5%">Credit<br />Quality</th>
                                                        <th style="width: 5%">Trade<br />Value</th>
                                                        <th style="width: 5%">Score #1</th>
                                                        <th style="width: 5%">Score #2</th>
                                                        <th style="width: 5%">Score #3</th>
                                                        <th>Assigned To</th>
                                                        <th>Status</th>
                                                        <th style="width: 15%">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>James</td>
                                                        <td>Miller</td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td>$25,000.00</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">
                                                                    82%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <!--                                        <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                        <td>
                                                                                                        &nbsp;
                                                                                                    </td>-->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="78">
                                                                <input name="sales_status" type="hidden" value="New Opportunity">
                                                                <select onchange="this.form.submit()" name="selSalesPeople"><option value="0" selected="selected">unassigned</option><option value="4">Sam Arlington</option><option value="6">Sal Parsons</option><option value="7">Gerald Manning</option><option value="8">John Smith</option></select>
                                                            </form>
                                                        </td>
                                                        <!-- <td>23:59</td> -->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="78">
                                                                <input name="assigned_to" type="hidden" value="0">
                                                                <select onchange="this.form.submit()" name="selStatus"><option value="0" selected="selected">New Opportunity</option><option value="1">Open Opportunity</option><option value="2">Not Interested</option><option value="3">Sold No Trade</option><option value="4">Sold With Trade</option></select>
                                                            </form></td>
                                                        <td><a href="#"><span class='glyphicon glyphicon-search' /></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>John</td>
                                                        <td>Smith</td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: white">0</div>
                                                            </div></td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td>$27,875.00</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">
                                                                    62%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <!--                                        <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                        <td>
                                                                                                        &nbsp;
                                                                                                    </td>-->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="76">
                                                                <input name="sales_status" type="hidden" value="New Opportunity">
                                                                <select onchange="this.form.submit()" name="selSalesPeople"><option value="0" selected="selected">unassigned</option><option value="4">Sam Arlington</option><option value="6">Sal Parsons</option><option value="7">Gerald Manning</option><option value="8">John Smith</option></select>
                                                            </form>
                                                        </td>
                                                        <!-- <td>19:53</td> -->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="76">
                                                                <input name="assigned_to" type="hidden" value="0">
                                                                <select onchange="this.form.submit()" name="selStatus"><option value="0" selected="selected">New Opportunity</option><option value="1">Open Opportunity</option><option value="2">Not Interested</option><option value="3">Sold No Trade</option><option value="4">Sold With Trade</option></select>
                                                            </form></td>
                                                        <td><a href="#"><span class='glyphicon glyphicon-search' /></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jon</td>
                                                        <td>Doe</td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: white">0</div>
                                                            </div></td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td>$20,225.00</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">
                                                                    52%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <!--                                        <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                        <td>
                                                                                                        &nbsp;
                                                                                                    </td>-->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="77">
                                                                <input name="sales_status" type="hidden" value="New Opportunity">
                                                                <select onchange="this.form.submit()" name="selSalesPeople"><option value="0" selected="selected">unassigned</option><option value="4">Sam Arlington</option><option value="6">Sal Parsons</option><option value="7">Gerald Manning</option><option value="8">John Smith</option></select>
                                                            </form>
                                                        </td>
                                                        <!-- <td>19:53</td> -->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="77">
                                                                <input name="assigned_to" type="hidden" value="0">
                                                                <select onchange="this.form.submit()" name="selStatus"><option value="0" selected="selected">New Opportunity</option><option value="1">Open Opportunity</option><option value="2">Not Interested</option><option value="3">Sold No Trade</option><option value="4">Sold With Trade</option></select>
                                                            </form></td>
                                                        <td><a href="#"><span class='glyphicon glyphicon-search' /></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="row">
                    <img src="/images/PoweredBy.png" style="width: 37.5%; height; auto;" />
                </div>
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1
                </div>
                <strong>Copyright &copy; 2017 <a href="#">Servisell, LLC</a>.</strong> All rights
                reserved.
            </footer>
    </body>
</html>
