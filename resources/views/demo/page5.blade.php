<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Trade Alert</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    </head>
    <style>
        body
        {
            background-color: white;
            font-size: small;
        }
        screen
        {
            background-color: white;
        }
        .paper
        {
            background-color: white;
            width: 8.5in;
            height: 11.0in;
            margin: auto;
        }
        .main-content
        {
            font-family: sans-serif;
        }
        .text-center
        {
            text-align: center;
        }
        .text-right
        {
            text-align: right;
        }
        .text-bold
        {
            font-weight: bold;
        }
        .text-underlined
        {
            text-decoration: underline;
        }
        .box
        {
            border: black solid 1px;
            width: 3.0in;
            padding:  3px;
        }
        .float-left
        {
            float: left;
            clear: left;
        }
        .float-right
        {
            float: right;
            clear: right;
        }
        .no-float
        {
            clear:left;
        }
        table.label-value-pair
        {
            width: 66%;
        }
        td.value
        {
            width: 50%;
            border: 1px solid black;
            text-align: center;
        }
        td.scale
        {
            width: 20%;
            border: 1px solid black;
        }
        td.label
        {
            width: 50%;
            border: none;
        }
        .floating
        {
            display: inline-block; 
        }
        .floating-box
        {
            display: inline-block;
            width: 150px;
            border: 2px solid black;  
            padding: 3px;
            text-align: center;
        }
        .text-servisell
        {
            color: #0f4568;
        }
        td.top-row
        {
            border-width: 2px 2px 1px 2px;
            border-style: solid;
            border-color: black;
            width: 50%;
            text-align: center;
        }
        td.middle-row
        {
            border-width: 1px 2px 1px 2px;
            border-style: solid;
            border-color: black;
            width: 50%;
            text-align: center;
        }
        td.bottom-row
        {
            border-width: 1px 2px 2px 2px;
            border-style: solid;
            border-color: black;
            width: 50%;
            text-align: center;
        }
        td.top-left
        {
            border-width: 2px 1px 1px 2px;
            border-style: solid;
            border-color: black;
            text-align: center;
        }
        td.top-right
        {
            border-width: 2px 2px 1px 1px;
            border-style: solid;
            border-color: black;
            text-align: center;
        }
        td.middle-left
        {
            border-width: 1px 1px 1px 2px;
            border-style: solid;
            border-color: black;
            text-align: center;
        }
        td.middle-right
        {
            border-width: 1px 2px 1px 1px;
            border-style: solid;
            border-color: black;
            text-align: center;
        }
        td.bottom-left
        {
            border-width: 1px 1px 2px 2px;
            border-style: solid;
            border-color: black;
            text-align: center;
        }
        td.bottom-right
        {
            border-width: 1px 2px 2px 1px;
            border-style: solid;
            border-color: black;
            text-align: center;
        }
        .btnPrint
        {
            display: none;
        }
        @media screen
        {
            body
            {
                background-color: gray;
            }
            .screen
            {
                text-align: center;
            }
            .main-content
            {
                margin: 0 auto;
                padding: 0.25in;
                text-align: initial;
                background-color: white;
            }
            .btnPrint
            {
                display: inline;
                margin: 8px auto;
            }
        }
    </style>
    <body>
        <div class="screen">
            <button class="btnPrint" onclick="window.print();">Print</button>&nbsp<button class="btnPrint" onclick="window.close();">Continue Demo</button>
            <div class="main-content" style="width: 8.5in; height: 11.0in;">
                <div>
                    <img src="/images/PoweredBy.png" style="width: 7.5in; height: auto;"/>
                </div>
                <div class="text-center">
                    <br />
                    <span class="text-bold text-servisell" style="font-size: large;">TRADE ALERT</span>
                </div>
                <div>
                    <br />
                    <div class="floating" style="width: 33%;">
                        ASSIGNED TO:
                    </div>
                    <div class="floating-box" style="margin-left: -3px;">
                        Unassigned
                    </div>
                    <div class="floating" style="margin-left: 8px; margin-right: 8px;">
                        WHEN ASSIGNED: 
                    </div>
                    <div class="floating-box">
                        2017-03-24 09:53:39
                    </div>
                </div>
                <div>
                    <div class="no-float">
                        <br /><br />
                        <span class="text-bold text-underlined text-servisell">CUSTOMER INFORMATION</span>
                    </div>
                    <div>
                        <br /><br />
                        <table cellspacing="0" cellpadding="3px" class="label-value-pair">
                            <tr>
                                <td class="label">
                                    CUSTOMER NAME:
                                </td>
                                <td class="top-row">
                                    James&nbsp;Miller
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    CUSTOMER PHONE:
                                </td>
                                <td class="middle-row">
                                    (616) 555-4321
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    CUSTOMER E-MAIL:
                                </td>
                                <td class="bottom-row">
                                    james.miller@mailinator.com
                                </td>
                            </tr>
                        </table>
                    </div><br />
                    <div class="floating text-bold text-servisell" style="width: 33%;">
                        CREDIT QUALITY:
                    </div>
                    <div class="floating-box text-bold" style="margin-left: -3px; width: 5%; background-color: lime;">
                        10
                    </div>
                    <div class="floating text-bold text-servisell" style="margin-left: 5%; margin-right: 5%;">
                        AUTO INQUIRIES IN LAST 30 DAYS: 
                    </div>
                    <div class="floating-box text-bold" style="width: 5%; background-color: lime;">
                        Yes
                    </div>
                </div>
                <div style="float: left; width: 74%;">
                    <div>
                        <br /><br />
                        <span class="text-bold text-underlined text-servisell">SERVICE VEHICLE INFORMATION</span>
                    </div>
                    <div>
                        <br /><br />
                        <table class="label-value-pair"  cellspacing="0" cellpadding="3px" style="width: 80%">
                            <tr>
                                <td class="label" style="width: 55%;">
                                    TRADE YEAR:
                                </td>
                                <td class="top-row">
                                    2014
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    TRADE MAKE:
                                </td>
                                <td class="middle-row">
                                    JEEP
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    TRADE MODEL:
                                </td>
                                <td class="middle-row">
                                    Grand Cherokee-V6
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    TRADE MILEAGE:
                                </td>
                                <td class="middle-row">
                                    48,582
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    NADA TRADE:
                                </td>
                                <td class="middle-row">
                                    $25,000.00
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    NADA RETAIL:
                                </td>
                                <td class="bottom-row">
                                    $27,575.00
                                </td>
                            </tr>                        
                        </table>
                    </div>
                </div>
                <div class="text-bold text-servisell" style="float: right; width: 21%; text-align: center; margin-right: 4%">
                    <br /><br /><br /><br /><br />This Service Vehicle is<br />
                    related to Open Trade Line<br />
                    <div style="border: 2px solid black; width; 60%; height: 60px; margin-left: auto;">&nbsp;</div>
                    Listed Below
                </div>
                <div>
                    <div class="no-float">
                        <br /><br />
                        <span class="text-bold text-underlined text-servisell">TRADE LIKELIHOOD SCORING - CUSTOMER OPEN AUTO LOANS</span>
                    </div>
                    <br />
                    <table style="width: 100%;" cellspacing="0" cellpadding="3px" >
                        <tbody>
                            <tr>
                                <td class="text-left text-bold" style="width: 25%;">
                                    OPEN TRADE LINE NUMBER
                                </td>
                                <th style="width: 25%;" colspan="3">
                                    <span class="text-underlined">1</span>
                                </th>
                                <th style="width: 25%;" colspan="3">
                                    <span class="text-underlined">2</span>
                                </th>
                                <th style="width: 25%;" colspan="3">
                                    <span class="text-underlined">3</span>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="8">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="text-bold">
                                    AUTO INQUIRIES - LAST 30
                                </td>
                                                                                                <td class="top-left text-bold" style="width: 5%; background-color: lime;">
                                    10
                                </td>
                                <td class="top-right" style="margin-right: 2%;">
                                    One or More
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="top-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="top-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="top-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="top-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td class="text-bold">
                                    CREDIT SCORE
                                </td>
                                                                                                <td class="middle-left text-bold" style="width: 5%; background-color: lime;">
                                    10
                                </td>
                                <td class="middle-right"">
                                    625 to 680
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td class="text-bold">
                                    TRADE EQUITY
                                </td>
                                                                                                <td class="middle-left text-bold" style="width: 5%; background-color: lime;">
                                    10
                                </td>
                                <td class="middle-right">
                                    Trade equity present
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td class="text-bold">
                                    % OF PAYMENTS MADE
                                </td>
                                                                                                <td class="middle-left text-bold" style="width: 5%; background-color: yellow;">
                                    3
                                </td>
                                <td class="middle-right">
                                    20% to 59%
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td class="text-bold">
                                    LOAN INTEREST RATE
                                </td>
                                                                                                <td class="middle-left text-bold" style="width: 5%; background-color: yellow;">
                                    3
                                </td>
                                <td class="middle-right">
                                    3% TO 8.99%
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="middle-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="middle-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td class="text-bold">
                                    LOAN PAYMENT AMOUNT
                                </td>
                                                                                                <td class="bottom-left text-bold" style="width: 5%; background-color: lime;">
                                    5
                                </td>
                                <td class="bottom-left">
                                    More than $500
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="bottom-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="bottom-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td class="bottom-left" style="width: 5%;">
                                    &nbsp;
                                </td>
                                <td class="bottom-right">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td colspan="8">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="text-bold text-right" style="padding-right: 2%;">
                                    TOTAL SCORE
                                </td>
                                                                                                                                <td class="text-center text-bold" style="border: 2px solid black; width: 5%; background-color: lime;">
                                    41
                                </td>
                                <td style="border: none;">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td style="border: 2px solid black; width: 5%;">
                                    &nbsp;
                                </td>
                                <td style="border: none;">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                <td style="border: 2px solid black; width: 5%;">
                                    &nbsp;
                                </td>
                                <td style="border: none;">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                            <tr>
                                <td colspan="8">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="text-bold text-servisell" style="font-size: large;">
                                    TRADE PROBABILITY:
                                </td>
                                                                                                                                <td class="text-center text-bold" style="border: 2px solid black; width: 5%; background-color: lime;">
                                    82%
                                </td>
                                <td style="border: none;">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                                                <td style="border: 2px solid black; width: 5%;">
                                    &nbsp;
                                </td>
                                <td style="border: none;">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                                                                                                <td style="border: 2px solid black; width: 5%;">
                                    &nbsp;
                                </td>
                                <td style="border: none;">
                                    &nbsp;
                                </td>
                                                                <td style="width: 1%;"></td>
                                                            </tr>
                        </tbody>
                    </table>
                    <div>
                        <br />
                        Trade Probability is calculated based on a score of zero to 50 points per open auto loan.<br />
                        The higher the total score, the more likely the customer will be to trade into a new vehicle.
                        <br /><br />
                        <div class="float-left">
                            <span class="text-bold text-servisell">SALES MANAGERS RECOMMENDED NEW PURCHASE: </span>
                        </div>
                        <div class="float-right text-center" style="width: 50%">
                            <br />
                            <hr />
                            <span class="text-bold text-servisell">STOCK NUMBER</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>