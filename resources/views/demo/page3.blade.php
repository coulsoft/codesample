<? php
use Auth;
use App\Profile;
use App\Role;

$currentUser = isset($currentUser) ? $currentUser : Auth::user();
$currentRole = isset($currentRole) ? $currentRole : Profile::getCurrentRole();
$currentProfile = isset($currentProfile) ? $currentProfile : Profile::getCurrentProfile();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Servisell</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/dist/css/skins/skin-servisell.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-93252040-1', 'auto');
            ga('send', 'pageview');

        </script>
        <script>
            var intArrow = null;
            function showArrow()
            {
                clearInterval(intArrow);
                spnArrow = document.getElementById("spnArrow");
                if (spnArrow != null)
                {
                    spnArrow.innerHTML = "&nbsp;<img src=\"/images/ClickHereBlink.gif\" />"
                }
            }
            intArrow = setInterval(showArrow, 7000);
        </script>
    </head>
    <body class="hold-transition skin-servisell sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="servisell_mini.png" /></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="/images/logo_horizontal.png" /></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="/logout">
                                    <span class="hidden-xs">Logout</span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                &nbsp;
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image" style="padding-top: 10px; padding-bottom: 10px; padding-left: 1px; padding-right: 1px;">
                            &nbsp;
                        </div>
                        <div class="pull-left info">
                            <p>Sam Arlington</p>
                            <p>Sales Manager</p>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="#"><i class="fa fa-car"></i> <span>Sales</span></a></li>
<!--                        <li><a href="#"><i class="fa fa-bar-chart"></i> <span>Monthly Report</span></a></li>-->
                    </ul>
                    <ul class="sidebar-menu">
                        <li class="header">Documentation</li>
                        <!--<li><a href="{!! url('/downloads/ServisellTrainingGuide.pdf', [], true) !!}">Training Guide</a></li>-->
                        <li><a href="{!! url('/demo', [], true) !!}">*** Restart Demo ***</a></li>
                    </ul>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>

                <!-- Main content -->
                <section class="content">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
                    <style>
                        h5
                        {
                            font-family: "Myriad Pro", Arial, Helvetica, sans-serif;
                            font-weight: normal;
                            font-size: 12px;
                            margin: 0px;
                            border: none;
                            color: rgb(0, 0, 0);
                        }
                        .nada
                        {
                            color: #144ab0;
                        }
                    </style>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Sales Opportunities</div>
                                    <div class="panel-body">
                                        <div class="col-lg-12 box">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr style="text-align: center; font-weight: bold;">
                                                        <td colspan="5">&nbsp;</td>
                                                        <td style="width: 15%" colspan="3">Trade Probability</td>
                                                        <td colspan="3">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th style="width: 5%">Auto Inquires</th>
                                                        <th style="width: 5%">Credit<br />Quality</th>
                                                        <th style="width: 5%">Trade<br />Value</th>
                                                        <th style="width: 5%">Score #1</th>
                                                        <th style="width: 5%">Score #2</th>
                                                        <th style="width: 5%">Score #3</th>
                                                        <th>Assigned To</th>
                                                        <th>Status</th>
                                                        <th style="width: 15%">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>James</td>
                                                        <td>Miller</td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td>$25,000.00</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">
                                                                    82%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
            <!--                                        <td>
                                                            &nbsp;
                                                        </td>
            <td>
                                                            &nbsp;
                                                        </td>-->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="78">
                                                                <input name="sales_status" type="hidden" value="New Opportunity">
                                                                <select onchange="this.form.submit()" name="selSalesPeople"><option value="0" selected="selected">unassigned</option><option value="4">Sam Arlington</option><option value="6">Sal Parsons</option><option value="7">Gerald Manning</option><option value="8">John Smith</option></select>
                                                            </form>
                                                        </td>
                                                        <!-- <td>23:59</td> -->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="78">
                                                                <input name="assigned_to" type="hidden" value="0">
                                                                <select onchange="this.form.submit()" name="selStatus"><option value="0" selected="selected">New Opportunity</option><option value="1">Open Opportunity</option><option value="2">Not Interested</option><option value="3">Sold No Trade</option><option value="4">Sold With Trade</option></select>
                                                            </form></td>
                                                        <td><a href="/demo/4"><span class='glyphicon glyphicon-search' /></a><span id="spnArrow"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>John</td>
                                                        <td>Smith</td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: white">0</div>
                                                            </div></td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td>$27,875.00</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">
                                                                    62%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
            <!--                                        <td>
                                                            &nbsp;
                                                        </td>
            <td>
                                                            &nbsp;
                                                        </td>-->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="76">
                                                                <input name="sales_status" type="hidden" value="New Opportunity">
                                                                <select onchange="this.form.submit()" name="selSalesPeople"><option value="0" selected="selected">unassigned</option><option value="4">Sam Arlington</option><option value="6">Sal Parsons</option><option value="7">Gerald Manning</option><option value="8">John Smith</option></select>
                                                            </form>
                                                        </td>
                                                        <!-- <td>19:53</td> -->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="76">
                                                                <input name="assigned_to" type="hidden" value="0">
                                                                <select onchange="this.form.submit()" name="selStatus"><option value="0" selected="selected">New Opportunity</option><option value="1">Open Opportunity</option><option value="2">Not Interested</option><option value="3">Sold No Trade</option><option value="4">Sold With Trade</option></select>
                                                            </form></td>
                                                        <td><a href="#"><span class='glyphicon glyphicon-search' /></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jon</td>
                                                        <td>Doe</td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: white">0</div>
                                                            </div></td>
                                                        <td><div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">10</div>
                                                            </div></td>
                                                        <td>$20,225.00</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div style="text-align: center; color: black; background-color: lime">
                                                                    52%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
            <!--                                        <td>
                                                            &nbsp;
                                                        </td>
            <td>
                                                            &nbsp;
                                                        </td>-->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="77">
                                                                <input name="sales_status" type="hidden" value="New Opportunity">
                                                                <select onchange="this.form.submit()" name="selSalesPeople"><option value="0" selected="selected">unassigned</option><option value="4">Sam Arlington</option><option value="6">Sal Parsons</option><option value="7">Gerald Manning</option><option value="8">John Smith</option></select>
                                                            </form>
                                                        </td>
                                                        <!-- <td>19:53</td> -->
                                                        <td><form>
                                                                <input type="hidden" name="_method" value="PUT">
                                                                <input name="id" type="hidden" value="77">
                                                                <input name="assigned_to" type="hidden" value="0">
                                                                <select onchange="this.form.submit()" name="selStatus"><option value="0" selected="selected">New Opportunity</option><option value="1">Open Opportunity</option><option value="2">Not Interested</option><option value="3">Sold No Trade</option><option value="4">Sold With Trade</option></select>
                                                            </form></td>
                                                        <td><a href="#"><span class='glyphicon glyphicon-search' /></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="row">
                    <img src="/images/PoweredBy.png" style="width: 37.5%; height; auto;" />
                </div>
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1
                </div>
                <strong>Copyright &copy; 2017 <a href="#">Servisell, LLC</a>.</strong> All rights
                reserved.
            </footer>
    </body>
</html>
