<? php
use Auth;
use App\Profile;
use App\Role;

$currentUser = isset($currentUser) ? $currentUser : Auth::user();
$currentRole = isset($currentRole) ? $currentRole : Profile::getCurrentRole();
$currentProfile = isset($currentProfile) ? $currentProfile : Profile::getCurrentProfile();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Servisell</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/dist/css/skins/skin-servisell.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-93252040-1', 'auto');
            ga('send', 'pageview');

        </script>
        <script>
            var intArrow = null;
            function showArrow()
            {
                clearInterval(intArrow);
                spnArrow = document.getElementById("spnArrow");
                if (spnArrow != null)
                {
                    spnArrow.innerHTML = "&nbsp;<img src=\"/images/ClickHereBlink.gif\" />"
                }
            }
            intArrow = setInterval(showArrow, 7000);
        </script>
    </head>
    <body class="hold-transition skin-servisell sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="/images/servisell_mini.png" /></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="/images/logo_horizontal.png" /></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="/logout">
                                    <span class="hidden-xs">Logout</span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                &nbsp;
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image" style="padding-top: 10px; padding-bottom: 10px; padding-left: 1px; padding-right: 1px;">
                            &nbsp;
                        </div>
                        <div class="pull-left info">
                            <p>Steve Williams</p>
                            <p>Service Writer</p>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="#"><i class="fa fa-wrench"></i> <span>Service</span></a></li>
                    </ul>
                    <ul class="sidebar-menu">
                        <li class="header">Documentation</li>
                        <!--<li><a href="{!! url('/downloads/ServisellTrainingGuide.pdf', [], true) !!}">Training Guide</a></li>-->
                        <li><a href="{!! url('/demo', [], true) !!}">*** Restart Demo ***</a></li>
                    </ul>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>

                <!-- Main content -->
                <section class="content">
                    <style>
                        .main-heading
                        {
                            background-color: rgb(245, 111, 61);
                            color: white;
                            text-align: center;
                            height: 26px;
                            padding: 0px;
                            line-height: 26px;
                            font-family: Myriad Pro, Arial, Helvetica, sans-serif;
                            font-size: 15px;
                            font-weight: bold;
                            text-shadow: rgb(76, 76, 76) 1px 1px 1px;
                            letter-spacing: 0.5px;
                        }
                        .sub-heading
                        {
                            background-color: rgb(228, 228, 228);
                            color: rgb(0, 0, 0);
                            text-align: left;
                            height: 23px;
                            padding: 0px 0px 0px 10px;
                            font-family: Myriad Pro, Arial, Helvetica, sans-serif;
                            font-weight: bold;
                            font-size: 13px;
                            margin: 0px;
                            border-top: 1px solid rgb(255, 255, 255);
                            border-right: 1px solid rgb(190, 190, 190);
                            border-bottom: 1px solid rgb(190, 190, 190);
                        }
                        .sub-body
                        {
                            background: rgb(247, 247, 247);
                            color: rgb(0, 0, 0);
                            font-size: 13px;
                            color: rgb(20, 74, 176);
                            font-weight: 500;
                            border-width: 1px 0px 1px 1px;
                            border-top-style: solid;
                            border-bottom-style: solid;
                            border-left-style: solid;
                            border-top-color: rgb(190, 190, 190);
                            border-bottom-color: rgb(190, 190, 190);
                            border-left-color: rgb(190, 190, 190);
                            border-image: initial;
                            border-right-style: initial;
                            border-right-color: initial;
                        }
                        h5
                        {
                            font-family: "Myriad Pro", Arial, Helvetica, sans-serif;
                            font-size: 12px;
                            font-weight: normal;
                            margin: 0px;
                            padding: 0px 0px 2px;
                            border: none;
                            color: rgb(0, 0, 0);
                        }
                        input, select
                        {
                            font-family: "Myriad Pro", Arial, Helvetica, sans-serif;
                            font-size: 12px;
                            font-weight: bold;
                            margin: 0px 10px 15px 0px;
                            line-height: normal;
                            height: 26px;
                            width: 100%;
                            padding-top: 0px;
                        }
                        .form-inline .form-control
                        {
                            margin: 0px 10px 15px 0px;
                            line-height: auto;
                            height: 26px;
                            width: 100%;
                            border: 1px solid black;
                            background-color: white;
                            padding: auto;
                        }
                        .panel-default
                        {
                            max-width: 768px;
                        }
                    </style>
                    <div class="container" style="width: 95%;">
                        <div class="panel panel-default float-left">
                            <div class="main-heading">Service Information</div>
                            <div class="panel-body">
                                <form method="GET" action="/demo/2" accept-charset="UTF-8" class="form-inline" permission="form" onsubmit="getElementById( & quot; btnSubmit & quot; ).disabled = true;">
                                    <div class="panel panel-default">
                                        <div class="sub-heading">Customer Information</div>
                                        <div class="panel-body sub-body">
                                            <div>
                                                <div class="form-group col-md-4">
                                                    <label for="first_name"><h5>First Name:</h5></label><br />
                                                    <input type="text" class="form-control" id="first-name" value="James" readonly>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="middle_name"><h5>Middle Initial:</h5></label><br />
                                                    <input type="text" class="form-control" id="middle_name">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="last_name"><h5>Last Name:</h5></label><br />
                                                    <input type="text" class="form-control" id="last_name" value="Miller" readonly>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="suffix"><h5>Suffix:</h5></label><br />
                                                    <input type="text" class="form-control" id="suffix">
                                                </div>
                                            </div>
                                            <div>
                                                <div class="form-group col-md-10">
                                                    <label for="address"><h5>Address:</h5></label><br />
                                                    <input type="text" class="form-control" id="address" value="1414 Jefferson St" readonly>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="unit"><h5>Unit:</h5></label><br />
                                                    <input type="text" class="form-control" id="unit">
                                                </div>
                                            </div>
                                            <div>
                                                <div class="form-group col-md-6">
                                                    <label for="city"><h5>City:</h5></label><br />
                                                    <input type="text" class="form-control" id="city" value="Grand Rapids" readonly="">
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="state"><h5>State:</h5></label><br />
                                                    <select style="height: 26px; border-color: black; text-color: black; color: black;" ><option value="MI">Select a state</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI" selected="selected">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option></select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="zip"><h5>Zip:</h5></label><br />
                                                    <input type="text" class="form-control" id="zip" value="49503" readonly>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="form-group col-md-7">
                                                    <label for="email"><h5>E-Mail:</h5></label><br />
                                                    <input type="email" class="form-control" id="email" value="james.miller@mailinator.com" readonly>
                                                </div>
                                                <div class="form-group col-md-5">
                                                    <label for="phone"><h5>Phone:</h5></label><br />
                                                    <input type="tel" class="form-control" id="phone" value="(616) 555-4321" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="sub-heading">Vehicle Information</div>
                                        <div class="panel-body sub-body">
                                            <div>
                                                <div class="form-group col-md-8">
                                                    <label for="vin"><h5>VIN:</h5></label><br />
                                                    <input type="text" class="form-control normal text-uppercase" id="vin" value="1C4RJFBG3EC547108" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="mileage"><h5>Mileage:</h5></label><br />
                                                    <input type="text" class="form-control" id="mileage" value="48,582" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button id='btnSubmit' name='btnSubmit' type="submit" class="btn btn-default">Submit</button><span id="spnArrow"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="row">
                    <img src="/images/PoweredBy.png" style="width: 37.5%; height; auto;" />
                </div>
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1
                </div>
                <strong>Copyright &copy; 2017 <a href="#">Servisell, LLC</a>.</strong> All rights
                reserved.
            </footer>
            
<!--                    <div class="container" style="width: 95%;">
                        <div class="panel panel-default float-left">
                            <div class="main-heading">Service Information</div>
                            <div class="panel-body">-->
            <div id="divDisclaimer" style="background-color: rgba(0, 0, 0, .5); position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1; text-align: center;">
                <div style="position: absolute; left: 25%; top: 25%; width: 50%; height: 50%; border: black solid 3px; background: white;">
                    <h1 style="background-color: lightgray;">FOR DEMONSTRATION PURPOSES ONLY!!!</h1>
                    <div style="margin-top: 10px;">
                        The following pages are for demonstration purposes only.  Click only when you see a prompt to do so, such as the following:<br />
                    </div>
                    <div style="margin-top: 10px;">
                        <img src="/images/ClickHereBlink.gif" /><br />
                    </div>
                    <div style="margin-top: 10px;">
                        For security reasons <span style="font-size: larger; font-weight: bolder">ALL OTHER CONTROLS HAVE BEEN DISABLED!!!</span><br />
                        Unlike the full version, you will only be able to see the data reserved for the demonstration.<br />
                    </div>
                    <button onclick='document.getElementById("divDisclaimer").style.visibility = "hidden";' style="margin-top: 10px;">Continue</button>
                </div>
            </div>
    </body>
</html>
