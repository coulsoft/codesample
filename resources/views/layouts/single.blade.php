<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,follow">
        <meta name="generator" content="WordPress 4.7.3">
        <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">

        <title>ServiSell</title>

        <!-- Links -->
        <link rel="icon" type="image/png" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png">
        <link rel="apple-touch-icon" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/2017/02/servisell-logo-web-76-02.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/2017/02/servisell-logo-web-120-02.png">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="dns-prefetch" href="//s.w.org">
        <link rel="alternate" type="application/rss+xml" title="Servisell » Feed" href="/feed/">
        <link rel="alternate" type="application/rss+xml" title="Servisell » Comments Feed" href="/comments/feed/">
        <link rel="alternate" type="application/rss+xml" title="Servisell » Home Comments Feed" href="/25_main/feed/">
        <link rel="stylesheet" href="/wp-content/uploads//dfd_ronneby_fonts/ronneby/Defaults.css">
        <link rel="stylesheet" href="/wp-content/plugins/revslider/public/assets/css/settings.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/styled-button.css">
        <link rel="stylesheet" href="/wp-content/uploads/smile_fonts/ronneby/Defaults.css">
        <link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.min.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-css/ultimate.min.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/visual-composer.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/app.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/mobile-responsive.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/jquery.isotope.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/prettyPhoto.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby_child/style.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/css/options.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%7CDroid+Serif%3A400%2C700%2C400italic%2C700italic%7CLora%3A400%2C700%2C400italic%2C700italic%7CMontserrat%3A400%2C700&amp;subset=latin">
        <link rel="https://api.w.org/" href="/wp-json/">
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd">
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml"> 
        <link rel="canonical" href="/">
        <link rel="shortlink" href="/">
        <link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=%2F">
        <link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=%2F&amp;format=xml">

        <!-- Scripts -->
        <script type="text/javascript">
                window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"\/wp-includes\/js\/wp-emoji-release.min.js"}};
                !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script"); c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)}var f, g, h, i, j = b.createElement("canvas"), k = j.getContext && j.getContext("2d"); for (i = Array("flag", "emoji4"), c.supports = {everything:!0, everythingExceptFlag:!0}, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]); c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function(){c.DOMReady = !0}, c.supports.everything || (g = function(){c.readyCallback()}, b.addEventListener?(b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)):(a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function(){"complete" === b.readyState && c.readyCallback()})), f = c.source || {}, f.concatemoji?e(f.concatemoji):f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))}(window, document, window._wpemojiSettings);        </script><script src="/wp-includes/js/wp-emoji-release.min.js" type="text/javascript" defer=""></script>
        <script type="text/javascript" src="/wp-includes/js/jquery/jquery.js"></script>
        <script type="text/javascript" src="/wp-includes/js/jquery/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Styles -->
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {}
        </style>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>

        <style type="text/css">
        .a-stats {
                width: auto;
        }
        .a-stats a {
                background: #7CA821;
                background-image:-moz-linear-gradient(0% 100% 90deg,#5F8E14,#7CA821);
                background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#7CA821),to(#5F8E14));
                border: 1px solid #5F8E14;
                border-radius:3px;
                color: #CFEA93;
                cursor: pointer;
                display: block;
                font-weight: normal;
                height: 100%;
                -moz-border-radius:3px;
                padding: 7px 0 8px;
                text-align: center;
                text-decoration: none;
                -webkit-border-radius:3px;
                width: 100%;
        }
        .a-stats a:hover {
                text-decoration: none;
                background-image:-moz-linear-gradient(0% 100% 90deg,#6F9C1B,#659417);
                background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#659417),to(#6F9C1B));
        }
        .a-stats .count {
                color: #FFF;
                display: block;
                font-size: 15px;
                line-height: 16px;
                padding: 0 13px;
                white-space: nowrap;
        }
        </style>

        <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution  - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
        <script type="text/javascript">jQuery(document).ready(function(){
            jQuery(".ult_modal-body iframe").each(function(index, element) {
            var w = jQuery(this).attr("width");
                    var h = jQuery(this).attr("height");
                    var st = '<style type="text/css" id="modal-css">';
                    st += "#" + jQuery(this).closest(".ult-overlay").attr("id") + " iframe{width:" + w + "px !important;height:" + h + "px !important;}";
                    st += ".fluid-width-video-wrapper{padding: 0 !important;}";
                    st += "</style>";
                    jQuery("head").append(st);
            });
            });</script><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1489538112930{background-color: #003a63 !important;}.vc_custom_1489543174247{padding-top: 150px !important;padding-bottom: 150px !important;}.vc_custom_1487803367276{background-color: #003a63 !important;}.vc_custom_1487790156928{padding-right: 7% !important;padding-bottom: 60px !important;padding-left: 7% !important;background-color: #003a63 !important;}.vc_custom_1487809445475{padding-top: 150px !important;padding-bottom: 150px !important;}.vc_custom_1489541129422{padding-top: 15px !important;padding-bottom: 25px !important;background-color: #e9e9e9 !important;}</style><noscript>&lt;style type="text/css"&gt; .wpb_animate_when_almost_visible { opacity: 1; }&lt;/style&gt;</noscript>	

        <style type="text/css"> </style><style>#dfd-image-bg-58f8c5a4f0da7 {background-position: ;background-image: url(/wp-content/uploads/2015/07/servisell-front-darker.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style><style>#dfd-image-bg-58f8c5a51c854 {background-image: url(/wp-content/uploads/2017/02/main-slider-2.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style><style>#dfd-image-bg-58f8c5a534380 {background-image: url(/wp-content/uploads/2015/07/servisell-section-2.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style><style>#dfd-vcard-widget-58f8c5a53e4e8 .dfd-vcard-wrap {background: #003a63;}#dfd-vcard-widget-58f8c5a53e4e8 .dfd-vcard-wrap {border-style: none;}#dfd-vcard-widget-58f8c5a53e4e8 .dfd-vcard-wrap {-webkit-border-radius: 2px;-moz-border-radius: 2px;-o-border-radius: 2px;border-radius: 2px;}</style><style class="keyframe-style" id="boost-keyframe" type="text/css"> .boostKeyframe{-webkit-transform:scale3d(1,1,1);}</style><style>.fluidvids{width:100%;position:relative;}.fluidvids iframe{position:absolute;top:0px;left:0px;width:100%;height:100%;}</style>

        <!-- Bootstrap Core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            body, html
            {
                height: 100%;
            }

            .parallax
            {
                /* The image used */
                background-image: url('/images/driving.png');

                /* Full height */
                height: 33%; 

                /* Create the parallax scrolling effect */
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            ul
            {
                display: inline-block;
                text-align: left;
            }
            .carousel-caption
            {
                top: 50%;
                transform: translateY(-50%);
                bottom: initial;
            }
            .carousel-caption a
            {
                color: gold;
            }
            .text-center a
            {
                color: gold;
            }
            .row
            {
                margin-left: 0px;
                margin-right: 0px;
            }
        </style>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-93252040-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>

    <body class=" hasGoogleVoiceExt">
        <div id="header-container" class="header-style-1 header-layout-fullwidth sticky-header-enabled simple logo-position-left menu-position-top  dfd-enable-mega-menu with-top-panel">
            <section id="header">
                <div class="header-top-panel">
                    <div class="row">
                        <div class="columns twelve header-info-panel">
                            <div class="top-info"><i class="dfd-icon-phone"></i><span class="dfd-top-info-delim-blank"></span><span><span id="gc-number-1" class="gc-cs-link" title="Call with Google Voice">877.473.4280</span></span><span class="dfd-top-info-delim"></span><i class="dfd-icon-email_2"></i><span class="dfd-top-info-delim-blank"></span>contact@servisell.com</div>


                        </div>
                    </div>
                </div>
                <div class="header-wrap">
                    <div class="row decorated">
                        <div class="columns twelve header-main-panel">
                            <div class="header-col-left">
                                <div class="mobile-logo">
                                    <div class="logo-for-panel">
                                        <div class="inline-block">
                                            <a href="/">
                                                <img src="/wp-content/uploads/2017/03/servisell_logo_web.png" alt="Servisell" data-retina="/wp-content/uploads/2017/03/servisell_logo_web.png" data-retina_w="745" data-retina_h="124" style="height: 50px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="logo-for-panel">
                                    <div class="inline-block">
                                        <a href="/">
                                            <img src="/wp-content/uploads/2017/03/servisell_logo_web.png" alt="Servisell" data-retina="/wp-content/uploads/2017/03/servisell_logo_web.png" data-retina_w="745" data-retina_h="124" style="height: 50px;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-col-right text-center clearfix">
                                <div class="header-icons-wrapper">
                                    <div class="dl-menuwrapper">
                                        <a href="#sidr" class="dl-trigger icon-mobile-menu dfd-vertical-aligned" id="mobile-menu">
                                            <span class="icon-wrap dfd-middle-line"></span>
                                            <span class="icon-wrap dfd-top-line"></span>
                                            <span class="icon-wrap dfd-bottom-line"></span>
                                        </a>
                                    </div>																																												</div>
                            </div>
                            <div class="header-col-fluid">
                                <a href="/" title="Home" class="fixed-header-logo">
                                    <img src="/wp-content/uploads/2017/03/servisell_logo_web.png" alt="logo">
                                </a>
                                <nav class="mega-menu clearfix  text-right" id="main_mega_menu" role="navigation">
                                    <ul id="menu-twenty_fifth_onepage" class="nav-menu menu-primary-navigation menu-clonable-for-mobiles"><li id="nav-menu-item-13014-58f8d04fa91d4" class="mega-menu-item nav-item menu-item-depth-0 current-menu-item"><a href="/" class="menu-link main-menu-link item-title" id="accessible-megamenu-1492701265043-1">Home</a></li>
                                        <li id="nav-menu-item-13212-58f8d04fa9312" class="mega-menu-item nav-item menu-item-depth-0"><a href="/test-drive/" class="menu-link main-menu-link item-title" id="accessible-megamenu-1492701265044-2">Test Drive</a></li>
                                        <li id="nav-menu-item-13012-58f8d04fa9489" class="mega-menu-item nav-item menu-item-depth-0"><a href="/contact/" class="menu-link main-menu-link item-title" id="accessible-megamenu-1492701265045-3">Contact</a></li>
                                        <li id="nav-menu-item-13022-58f8d04fa95a7" class="mega-menu-item nav-item menu-item-depth-0"><a href="/login" class="menu-link main-menu-link item-title" id="accessible-megamenu-1492701265046-4">Login</a></li>
                                    </ul>	<i class="carousel-nav prev dfd-icon-left_2"></i>
                                    <i class="carousel-nav next dfd-icon-right_2"></i>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- Navigation -->
<!--        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="height: 107px;">
            <div class="container">
                 Brand and toggle get grouped for better mobile display 
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{!! url('/') !!}"><img src="/images/MediumLogo.png" /></a>
                </div>
                 Collect the nav links, forms, and other content for toggling 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{!! url('/', [], true) !!}">Home</a>
                        </li>
                        <li>
                            <a href="{!! url('/contact', [], true) !!}">Contact</a>
                        </li>
                        <li>
                            <a href="{!! url('/terms', [], true) !!}">Terms of Use / Privacy Policy</a>
                        </li>
                        <li>
                            <a href="{!! url('/login', [], true) !!}">Login</a>
                        </li>
                    </ul>
                </div>
                 /.navbar-collapse 
            </div>
             /.container 
        </nav>-->

        @yield('content')

        <div id="footer-wrap">

            <section id="footer" class="footer-style-2 dfd-background-dark">

                <div class="row">
                    <div class="three columns">
                        <section id="crum_contacts_widget-17" class="widget widget_crum_contacts_widget"><h3 class="widget-title">About company</h3>SERVISELL is dedicated to developing intuitive and highly effective sales solutions for our dealer clients nationwide. The success of our client is paramount. SERVISELL strives to provide the cutting-edge sales systems needed to give our dealers the advantage in today’s highly competitive marketplace.</section>			</div>
                    <div class="three columns">
                        <section id="dfd_vcard_simple-12" class="widget widget_dfd_vcard_simple"><h3 class="widget-title">Contact us</h3>
                            <div id="dfd-vcard-widget-58f9133d45f16" class="row">
                                <div class="twelve columns">
                                    <div class="dfd-vcard-wrap">
                                        <div class="vcard-field">
                                            <i class="dfd-icon-tablet2"></i>
                                            <p><font color="#ffffff"><span id="gc-number-1" class="gc-cs-link" title="Call with Google Voice" style="color: white;">877.473.4280</span></font></p>
                                        </div>
                                        <div class="vcard-field">
                                            <i class="dfd-icon-email_1"></i>
                                            <p>
                                                <a href="mailto:info@servisell.com" title="">info@servisell.com</a>
                                            </p></div>
                                        <div class="vcard-field">
                                            <i class="dfd-icon-navigation"></i>
                                            <p>1001 Woodward Ave.<br />5th Floor<br />Detroit, MI 48226</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                    (function($) {
                            $('head').append('<style>#dfd-vcard-widget-58f9133d45f16 .dfd-vcard-wrap {background: #003a63;}#dfd-vcard-widget-58f9133d45f16 .dfd-vcard-wrap {border-style: none;}#dfd-vcard-widget-58f9133d45f16 .dfd-vcard-wrap {-webkit-border-radius: 2px;-moz-border-radius: 2px;-o-border-radius: 2px;border-radius: 2px;}</style>');
                    })(jQuery);
                            </script>

                        </section>			</div>
                    <div class="three columns">
                        <section id="text-32" class="widget widget_text"><h3 class="widget-title">Quick Links</h3>			<div class="textwidget"><a href="/login">Client Login</a><br><br>

                                <a href="/contact/">Email Servisell</a><br><br>

                                <a href="/privacy-policy-terms-use/">Privacy Policy</a>
                            </div>
                        </section>			</div>
                    <div class="three columns">
                        <section id="text-33" class="widget widget_text"><h3 class="widget-title">Servisell powered by:</h3>			<div class="textwidget"><img src="/images/CDKGlobalLogo_color.png" alt="CDK Global logo" style="width:100px;height:80px;">
                                <br>
                                <img src="/wp-content/uploads/2017/02/NADA-Values_rgb-Jan-2017.png" alt="NADA" style="width:100px;height:72px;">



                            </div>
                        </section>			</div>
                </div>

            </section>

            <section id="sub-footer" class=" dfd-background-dark">
                <div class="row">
                    <div class="twelve columns subfooter-copyright text-center">
                        © copyright 2017 Servisell				</div>
                </div>
            </section>

        </div>

        <!-- jQuery -->
        <script src="/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/js/bootstrap.min.js"></script>

    </body>

</html>
