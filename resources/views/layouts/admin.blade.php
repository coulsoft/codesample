<?php
$currentUser = isset($currentUser) ? $currentUser : \Auth::user();
$currentRole = isset($currentRole) ? $currentRole : \App\Profile::getCurrentRole();
$currentProfile = isset($currentProfile) ? $currentProfile : \App\Profile::getCurrentProfile();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Servisell&trade;</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/dist/css/skins/skin-servisell.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="{!! url('/plugins/iCheck/flat/blue.css', [], true) !!}">
        <!-- Morris chart -->
        <link rel="stylesheet" href="{!! url('/plugins/morris/morris.css') !!}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{!! url('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{!! url('/plugins/datepicker/datepicker3.css') !!}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{!! url('/plugins/daterangepicker/daterangepicker.css') !!}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{!! url('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-93252040-1', 'auto');
            ga('send', 'pageview');
            
            var previousWidth = 1920;
            function resizeLeftNav()
            {
                var currentWidth = window.outerWidth;
                var objBody = document.getElementsByTagName("body")[0];
                
                if ((previousWidth >= 1440) && (currentWidth < 1440) &&
                    (!objBody.className.includes("sidebar-collapse")))
                {
                    objBody.className += " sidebar-collapse";
                }
                else if ((previousWidth < 1440) && (currentWidth >= 1440) &&
                    (objBody.className.includes("sidebar-collapse")))
                {
                    objBody.className =
                        objBody.className.replace(" sidebar-collapse", "");
                }
                previousWidth = currentWidth;
            }
        </script>
    </head>
    <body class="hold-transition skin-servisell sidebar-mini"
        onresize="resizeLeftNav();" onload="resizeLeftNav();">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="{!! url('/') !!}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="{!! url('/images/servisell_mini.png', [], true) !!}" /></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="{!! url('/images/logo_horizontal.png', [], true) !!}" /></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="{!! url('/logout', [], true) !!}">
                                    <span class="hidden-xs">Logout</span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                &nbsp;
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image" style="padding-top: 10px; padding-bottom: 10px; padding-left: 1px; padding-right: 1px;">
                            &nbsp;
                        </div>
                        <div class="pull-left info">
                            <p>{!! $currentUser->name !!}</p>
                            <p>{!! $currentProfile->title !!}</p>
                        </div>
                    </div>
                    @if($currentRole->name == \App\Role\Names::Administrator)
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/register', [], true) }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
                        <li><a href="{{ url('/dealerships', [], true) }}"><i class="fa fa-building"></i> <span>Dealerships</span></a></li>
                        <li><a href="{{ url('/nada', [], true) }}"><i class="fa fa-book"></i> <span>Nada Search</span></a></li>
                        <li><a href="{{ url('/credit', [], true) }}"><i class="fa fa-book"></i> <span>Credit Search</span></a></li>
                        <li><a href="{{ url('/service', [], true) }}"><i class="fa fa-wrench"></i> <span>Service</span></a></li>
                        <li><a href="{{ url('/sales', [], true) }}"><i class="fa fa-car"></i> <span>Sales</span></a></li>
                        <li><a href="{{ url('/roles/create', [], true) }}"><i class="fa fa-gears"></i> <span>Roles</span></a></li>
                        <li><a href="{{ url('/permissions/create', [], true) }}"><i class="fa fa-gears"></i> <span>Permissions</span></a></li>
                        <li><a href="{{ url('/profiles', [], true) }}"><i class="fa fa-gears"></i> <span>Profiles</span></a></li>
                        <li class="header">Reports</li>
                        <li><a href="{{ url('/reports/NADAMonthly', [], true) }}"><i class="fa fa-file"></i><span>NADA Monthly</span></a></li>
                    </ul>
                    @elseif($currentRole->name == \App\Role\Names::Salesperson)
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/sales', [], true) }}"><i class="fa fa-car"></i> <span>Sales</span></a></li>
                    </ul>
                    @elseif($currentRole->name == \App\Role\Names::ServiceWriter)
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/service', [], true) }}"><i class="fa fa-wrench"></i> <span>Service</span></a></li>
                    </ul>
                    @elseif(($currentRole->name == \App\Role\Names::SalesManager) ||
                        ($currentRole->name == \App\Role\Names::Finance))
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/sales', [], true) }}"><i class="fa fa-car"></i> <span>Sales</span></a></li>
<!--                        <li><a href="{{ url('/sales', [], true) }}"><i class="fa fa-bar-chart"></i> <span>Monthly Report</span></a></li>-->
                    </ul>
                    @elseif($currentRole->name == \App\Role\Names::ServiceManager)
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/service', [], true) }}"><i class="fa fa-wrench"></i> <span>Service</span></a></li>
<!--                        <li><a href="{{ url('/service', [], true) }}"><i class="fa fa-bar-chart"></i> <span>Monthly Report</span></a></li>-->
                    </ul>
                    @elseif($currentRole->name == \App\Role\Names::SiteAdmin)
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/service', [], true) }}"><i class="fa fa-wrench"></i> <span>Service</span></a></li>
                        <li><a href="{{ url('/sales', [], true) }}"><i class="fa fa-car"></i> <span>Sales</span></a></li>
                        <li><a href="{{ url('/dealer', [], true) }}"><i class="fa fa-cogs"></i> <span>Settings</span></a></li>
                        <!-- <li><a href="{{ url('/dealer/register', [], true) }}"><i class="fa fa-users"></i> <span>Users</span></a></li> -->
                    </ul>
                    @elseif($currentRole->name == \App\Role\Names::DealershipAdministrator)
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/dealerships', [], true) }}"><i class="fa fa-building"></i> <span>Dealerships</span></a></li>
                        <li><a href="{{ url('/register', [], true) }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
                        <li><a href="{{ url('/logout', [], true) }}"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        <!-- <li><a href="{{ url('/dealer/register', [], true) }}"><i class="fa fa-users"></i> <span>Users</span></a></li> -->
                    </ul>
                    @else
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="{{ url('/logout', [], true) }}"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                    </ul>
                    @endif
                    <ul class="sidebar-menu">
                        <li class="header">Documentation</li>
                        <!--<li><a href="{!! url('/downloads/ServisellTrainingGuide.pdf', [], true) !!}"><i class="fa fa-book"></i> <span>Training Guide</span></a></li>-->
                    </ul>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    @yield('content-header')
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="row">
                    <img src="{!! url('/images/PoweredBy.png', [], true) !!}" style="width: 37.5%; height; auto;" />
                </div>
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2017 <a href="http://www.servisell.com">Servisell&trade;, LLC</a>.</strong> All rights
                reserved.
            </footer>
            
        <!-- jQuery 2.2.3 -->
        <script src="{!! url('/plugins/jQuery/jquery-2.2.3.min.js', [], true) !!}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="{!! url('bootstrap/js/bootstrap.min.js', [], true) !!}"></script>
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{!! url('/plugins/morris/morris.min.js', [], true) !!}"></script>
        <!-- Sparkline -->
        <script src="{!! url('/plugins/sparkline/jquery.sparkline.min.js', [], true) !!}"></script>
        <!-- jvectormap -->
        <script src="{!! url('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', [], true) !!}"></script>
        <script src="{!! url('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js', [], true) !!}"></script>
        <!-- jQuery Knob Chart -->
        <script src="{!! url('/plugins/knob/jquery.knob.js', [], true) !!}"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="{!! url('/plugins/daterangepicker/daterangepicker.js', [], true) !!}"></script>
        <!-- datepicker -->
        <script src="{!! url('/plugins/datepicker/bootstrap-datepicker.js', [], true) !!}"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{!! url('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js', [], true) !!}"></script>
        <!-- Slimscroll -->
        <script src="{!! url('/plugins/slimScroll/jquery.slimscroll.min.js', [], true) !!}"></script>
        <!-- FastClick -->
        <script src="{!! url('/plugins/fastclick/fastclick.js', [], true) !!}"></script>
        <!-- AdminLTE App -->
        <script src="{!! url('/dist/js/app.min.js', [], true) !!}"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{!! url('/dist/js/pages/dashboard.js', [], true) !!}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{!! url('/dist/js/demo.js', [], true) !!}"></script>
    </body>
</html>
