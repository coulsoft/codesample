@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="/css/one-page-wonder.css" rel="stylesheet">

<!-- Full Width Image Header -->
<header class="header-image">
    <div class="headline">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Accept Terms of Use and Privacy Policy</div>
                        <div class="panel-body">
                            <div>
                                By checking, "I agree", below you agree to the Servisell <a href="{!! url('/terms', [], true) !!}" target="_blank">"Terms of Service" and "Privacy Policy"</a>.
                            </div>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/terms', [], true) }}">
                                {!! csrf_field() !!}
                                
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input id="cbAccept" name="cbAccept" type="checkbox" onclick="
                                                        if (this.checked)
                                                        {
                                                            btnContinue = document.getElementById('btnContinue');
                                                            if (btnContinue != null)
                                                            {
                                                                btnContinue.className = btnContinue.className.replace('disabled', '').trim();
                                                                btnContinue.onclick = 'this.parent.submit();';
                                                            }
                                                        }
                                                    ">I agree.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <input id="btnContinue" type="submit" class="btn btn-primary disabled" onclick="return false;" value="Continue" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
