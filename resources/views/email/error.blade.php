<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js ie lt-ie10" lang="en-US"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-ie" lang="en-US"> <!--<![endif]-->

    <head>

        <meta charset="utf-8">

        <title>Servisell | Contact</title>

        <link rel="icon" type="image/png" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png" />
        <link rel="apple-touch-icon" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/2017/02/servisell-logo-web-76-02.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/2017/02/servisell-logo-web-120-02.png">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--[if lte IE 9]>
            <script src="/wp-content/themes/ronneby/assets/js/html5shiv.js"></script>
        <![endif]-->
        <!--[if lte IE 8]>
            <script src="/wp-content/themes/ronneby/assets/js/excanvas.compiled.js"></script>
        <![endif]-->

        <style type="text/css">
            body {


                background-repeat: repeat !important;

            }
        </style>

        <meta name='robots' content='noindex,follow' />
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <link rel='dns-prefetch' href='//s.w.org' />
        <link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Feed" href="/feed/" />
        <link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Comments Feed" href="/comments/feed/" />
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/", "ext":".png", "svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/", "svgExt":".svg", "source":{"concatemoji":"\/wp-includes\/js\/wp-emoji-release.min.js"}};
                    !function(a, b, c){function d(a){var b, c, d, e, f = String.fromCharCode; if (!k || !k.fillText)return!1; switch (k.clearRect(0, 0, j.width, j.height), k.textBaseline = "top", k.font = "600 32px Arial", a){case"flag":return k.fillText(f(55356, 56826, 55356, 56819), 0, 0), !(j.toDataURL().length < 3e3) && (k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57331, 55356, 57096), 0, 0), c = j.toDataURL(), b !== c); case"emoji4":return k.fillText(f(55357, 56425, 55356, 57341, 8205, 55357, 56507), 0, 0), d = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55357, 56425, 55356, 57341, 55357, 56507), 0, 0), e = j.toDataURL(), d !== e}return!1}function e(a){var c = b.createElement("script"); c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)}var f, g, h, i, j = b.createElement("canvas"), k = j.getContext && j.getContext("2d"); for (i = Array("flag", "emoji4"), c.supports = {everything:!0, everythingExceptFlag:!0}, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]); c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function(){c.DOMReady = !0}, c.supports.everything || (g = function(){c.readyCallback()}, b.addEventListener?(b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)):(a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function(){"complete" === b.readyState && c.readyCallback()})), f = c.source || {}, f.concatemoji?e(f.concatemoji):f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))}(window, document, window._wpemojiSettings);        </script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel="stylesheet" href="/wp-content/uploads//dfd_ronneby_fonts/ronneby/Defaults.css">
        <link rel="stylesheet" href="/wp-content/plugins/revslider/public/assets/css/settings.css">
        <style id='rs-plugin-settings-inline-css' type='text/css'>
            #rs-demo-id {}
        </style>
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/styled-button.css">
        <link rel="stylesheet" href="/wp-content/uploads/smile_fonts/ronneby/Defaults.css">
        <link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.min.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-css/ultimate.min.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/visual-composer.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/app.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/mobile-responsive.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/jquery.isotope.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/prettyPhoto.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby_child/style.css">
        <link rel="stylesheet" href="/wp-content/themes/ronneby/css/options.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%7CDroid+Serif%3A400%2C700%2C400italic%2C700italic%7CLora%3A400%2C700%2C400italic%2C700italic%7CMontserrat%3A400%2C700&#038;subset=latin">
        <script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
        <script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
        <link rel='https://api.w.org/' href='/wp-json/' />
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
        <meta name="generator" content="WordPress 4.7.3" />
        <link rel="canonical" href="/contact/" />
        <link rel='shortlink' href='/?p=13010' />
        <link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=%2Fcontact%2F" />
        <link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=%2Fcontact%2F&#038;format=xml" />
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>

        <style type="text/css">
            .a-stats {
                width: auto;
            }
            .a-stats a {
                background: #7CA821;
                background-image:-moz-linear-gradient(0% 100% 90deg,#5F8E14,#7CA821);
                background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#7CA821),to(#5F8E14));
                border: 1px solid #5F8E14;
                border-radius:3px;
                color: #CFEA93;
                cursor: pointer;
                display: block;
                font-weight: normal;
                height: 100%;
                -moz-border-radius:3px;
                padding: 7px 0 8px;
                text-align: center;
                text-decoration: none;
                -webkit-border-radius:3px;
                width: 100%;
            }
            .a-stats a:hover {
                text-decoration: none;
                background-image:-moz-linear-gradient(0% 100% 90deg,#6F9C1B,#659417);
                background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#659417),to(#6F9C1B));
            }
            .a-stats .count {
                color: #FFF;
                display: block;
                font-size: 15px;
                line-height: 16px;
                padding: 0 13px;
                white-space: nowrap;
            }
        </style>

        <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
        <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution  - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <script type="text/javascript">jQuery(document).ready(function(){
            jQuery(".ult_modal-body iframe").each(function(index, element) {
            var w = jQuery(this).attr("width");
                    var h = jQuery(this).attr("height");
                    var st = '<style type="text/css" id="modal-css">';
                    st += "#" + jQuery(this).closest(".ult-overlay").attr("id") + " iframe{width:" + w + "px !important;height:" + h + "px !important;}";
                    st += ".fluid-width-video-wrapper{padding: 0 !important;}";
                    st += "</style>";
                    jQuery("head").append(st);
            });
            });</script><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1487951508742{background-color: #e9e9e9 !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	

    </head>
    <body class="page-template page-template-page-custom page-template-page-custom-php page page-id-13010 contact wpb-js-composer js-comp-ver-4.12 vc_responsive" data-directory="/wp-content/themes/ronneby" data-header-responsive-width="1101">


        <div class="form-search-section" style="display: none;">
            <div class="row">
                <form role="search" method="get" id="searchform_58bfb9c391e55" class="form-search" action="/">
                    <i class="dfdicon-header-search-icon inside-search-icon"></i>
                    <input type="text" value="" name="s" id="s_58bfb9c391e0c" class="search-query" placeholder="Search on site...">
                    <input type="submit" value="Search" class="btn">
                    <i class="header-search-switcher close-search"></i>
                </form>	</div>
        </div><div id="header-container" class="header-style-1 header-layout-fullwidth sticky-header-enabled simple logo-position-left menu-position-top  dfd-enable-mega-menu dfd-enable-headroom with-top-panel">
            <section id="header">
                <div class="header-top-panel">
                    <div class="row">
                        <div class="columns twelve header-info-panel">
                            <div class="top-info"><i class="dfd-icon-phone"></i><span class="dfd-top-info-delim-blank"></span>877.473.4280<span class="dfd-top-info-delim"></span><i class="dfd-icon-email_2"></i><span class="dfd-top-info-delim-blank"></span>contact@servisell.com</div>


                        </div>
                    </div>
                </div>
                <div class="header-wrap">
                    <div class="row decorated">
                        <div class="columns twelve header-main-panel">
                            <div class="header-col-left">
                                <div class="mobile-logo">
                                    <div class="logo-for-panel">
                                        <div class="inline-block">
                                            <a href="/">
                                                <img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="logo-for-panel">
                                    <div class="inline-block">
                                        <a href="/">
                                            <img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-col-right text-center clearfix">
                                <div class="header-icons-wrapper">
                                    <div class="dl-menuwrapper">
                                        <a href="#sidr" class="dl-trigger icon-mobile-menu dfd-vertical-aligned" id="mobile-menu">
                                            <span class="icon-wrap dfd-middle-line"></span>
                                            <span class="icon-wrap dfd-top-line"></span>
                                            <span class="icon-wrap dfd-bottom-line"></span>
                                        </a>
                                    </div>																																												</div>
                            </div>
                            <div class="header-col-fluid">
                                <a href="/" title="Home" class="fixed-header-logo">
                                    <img src="/wp-content/uploads/2017/02/servisell-logo-web.png" alt="logo"/>
                                </a>
                                <nav class="mega-menu clearfix  text-right" id="main_mega_menu">
                                    <ul id="menu-twenty_fifth_onepage" class="nav-menu menu-primary-navigation menu-clonable-for-mobiles"><li id="nav-menu-item-13014-58bfb9c3ab0ec" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/" class="menu-link main-menu-link item-title">Home</a></li>
                                        <li id="nav-menu-item-13212-58bfb9c3ab25c" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/test-drive/" class="menu-link main-menu-link item-title">Test Drive</a></li>
                                        <li id="nav-menu-item-13012-58bfb9c3ab396" class="mega-menu-item nav-item menu-item-depth-0 current-menu-item "><a href="/contact/" class="menu-link main-menu-link item-title">Contact</a></li>
                                        <li id="nav-menu-item-13022-58bfb9c3ab4b0" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/login" class="menu-link main-menu-link item-title">Login</a></li>
                                    </ul>	<i class="carousel-nav prev dfd-icon-left_2"></i>
                                    <i class="carousel-nav next dfd-icon-right_2"></i>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>	
        <div id="main-wrap" class="">

            <div id="change_wrap_div">



                <section id="layout" class="no-title">


                    <div  class="vc-row-wrapper wpb_row dfd-background-dark mobile-destroy-equal-heights aligh-content-verticaly" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
                            <div class="twelve columns" data-parallax_sense="30">
                                <div class="wpb_wrapper">
                                    <div class="ult-spacer spacer-58bfb9c418461" data-id="58bfb9c418461" data-height="250" data-height-mobile="60" data-height-tab="80" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb9c418600" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h5 class="widget-title uvc-main-heading" style="font-family:'Montserrat';font-size:47px;line-height:56px;letter-spacing:-1px;">contact</h5><h2 class="widget-sub-title uvc-sub-heading" style="font-size:28px;color:#ffffff;line-height:32px;">Unleash your service conversion today</h2></div><div class="ult-spacer spacer-58bfb9c4187fe" data-id="58bfb9c4187fe" data-height="250" data-height-mobile="60" data-height-tab="80" style="clear:both;display:block;"></div>
                                </div> 
                            </div> 
                        </div><div class="dfd-row-bg-wrap dfd-row-bg-image dfd_vertical_parallax" id="dfd-image-bg-58bfb9c41bdb7"  data-parallax_offset="" data-parallax_sense="30" data-mobile_enable="1"></div><script type="text/javascript">
                                    (function($) {
                                    $("head").append("<style>#dfd-image-bg-58bfb9c41bdb7 {background-image: url(/wp-content/uploads/2017/02/slider-main-1-1.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style>");
                                    })(jQuery);                        </script><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div id="contacts" class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1487951508742" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
                            <div class="six dfd_col-tabletop-6 dfd_col-laptop-6 dfd_col-mobile-12 columns" data-parallax_sense="30">
                                <div class="wpb_wrapper">
                                    <div class="ult-spacer spacer-58bfb9c424f9a" data-id="58bfb9c424f9a" data-height="100" data-height-mobile="60" data-height-tab="80" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb9c4250af" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">Contact</h3></div><div class="ult-spacer spacer-58bfb9c425189" data-id="58bfb9c425189" data-height="15" data-height-mobile="10" data-height-tab="10" style="clear:both;display:block;"></div><div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns six"><div class="wpb_wrapper"><div id="info-box-58bfb9c4455df" class="aio-icon-component   style_1"><div class="aio-icon-box left-icon  cr-animate-gen" data-animate-type = "transition.slideRightIn"  style=""><div class="aio-icon-left"><div class="align-icon" style="text-align: center;">
                                                                    <div class="aio-icon none  " style="color:#333333;font-size:24px;display:inline-block !important;" >
                                                                        <i class="dfd-icon-email_doc"></i>
                                                                    </div></div></div><div class="aio-ibd-block"><div class="aio-icon-header"><div class="feature-title" style="">For your contacts</div></div><!-- header --><div class="aio-icon-description" style="font-weight:bold;color:#595959;"><p>Phone: 877.473.4280</p>
                                                                    <p>Email: contact@servisell.com</p>
                                                                </div> <!-- description --></div> <!-- aio-ibd-block --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --></div></div><div class="columns six"><div class="wpb_wrapper"><div id="info-box-58bfb9c449044" class="aio-icon-component   style_1"><div class="aio-icon-box left-icon  cr-animate-gen" data-animate-type = "transition.slideRightIn"  style=""><div class="aio-icon-left"><div class="align-icon" style="text-align: center;">
                                                                    <div class="aio-icon none  " style="color:#333333;font-size:24px;display:inline-block !important;" >
                                                                        <i class="dfd-icon-signpost_2"></i>
                                                                    </div></div></div><div class="aio-ibd-block"><div class="aio-icon-header"><div class="feature-title" style="">Visit our office</div></div><!-- header --><div class="aio-icon-description" style="font-weight:bold;color:#595959;"><p>1001 Woodward Ave.<br />5th Floor<br />Detroit, MI 48226</p>
                                                                </div> <!-- description --></div> <!-- aio-ibd-block --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --></div></div></div></div>
                                </div> 
                            </div> 

                            <div class="six columns" data-parallax_sense="30">
                                <div class="wpb_wrapper">
                                    <div class="ult-spacer spacer-58bfb9c44ce80" data-id="58bfb9c44ce80" data-height="100" data-height-mobile="30" data-height-tab="80" style="clear:both;display:block;"></div>
<div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Message Not Sent</div>
                        <div class="panel-body">
                            <div>
                                                                <p>Due to technical difficulties beyond our control, your message was not sent.</p>
                                <p>Please try again later.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                    <!-- TODO: Move to Template File. -->
                                    <!-- <script>
                                                var formDisplay = 1;
                                                /* Maybe initialize nfForms object */
                                                var nfForms = nfForms || [];
                                                /* Build Form Data */
                                                var form = [];
                                                form.id = '1';
                                                form.settings = {"objectType":"Form Setting", "editActive":true, "title":"Send SERVISELL a message", "key":"", "created_at":"2016-08-24 16:39:20", "default_label_pos":"above", "_seq_num":"3", "conditions":[], "show_title":"1", "clear_complete":"1", "hide_complete":"1", "wrapper_class":"", "element_class":"", "add_submit":"1", "logged_in":"", "not_logged_in_msg":"", "sub_limit_number":"", "sub_limit_msg":"", "calculations":[], "formContentData":["name", "email", "phone_number_1487879262614", "message", "submit"], "container_styles_background-color":"", "container_styles_border":"", "container_styles_border-style":"", "container_styles_border-color":"", "container_styles_color":"", "container_styles_height":"", "container_styles_width":"", "container_styles_font-size":"", "container_styles_margin":"", "container_styles_padding":"", "container_styles_display":"", "container_styles_float":"", "container_styles_show_advanced_css":"0", "container_styles_advanced":"", "title_styles_background-color":"", "title_styles_border":"", "title_styles_border-style":"", "title_styles_border-color":"", "title_styles_color":"", "title_styles_height":"", "title_styles_width":"", "title_styles_font-size":"", "title_styles_margin":"", "title_styles_padding":"", "title_styles_display":"", "title_styles_float":"", "title_styles_show_advanced_css":"0", "title_styles_advanced":"", "row_styles_background-color":"", "row_styles_border":"", "row_styles_border-style":"", "row_styles_border-color":"", "row_styles_color":"", "row_styles_height":"", "row_styles_width":"", "row_styles_font-size":"", "row_styles_margin":"", "row_styles_padding":"", "row_styles_display":"", "row_styles_show_advanced_css":"0", "row_styles_advanced":"", "row-odd_styles_background-color":"", "row-odd_styles_border":"", "row-odd_styles_border-style":"", "row-odd_styles_border-color":"", "row-odd_styles_color":"", "row-odd_styles_height":"", "row-odd_styles_width":"", "row-odd_styles_font-size":"", "row-odd_styles_margin":"", "row-odd_styles_padding":"", "row-odd_styles_display":"", "row-odd_styles_show_advanced_css":"0", "row-odd_styles_advanced":"", "success-msg_styles_background-color":"", "success-msg_styles_border":"", "success-msg_styles_border-style":"", "success-msg_styles_border-color":"", "success-msg_styles_color":"", "success-msg_styles_height":"", "success-msg_styles_width":"", "success-msg_styles_font-size":"", "success-msg_styles_margin":"", "success-msg_styles_padding":"", "success-msg_styles_display":"", "success-msg_styles_show_advanced_css":"0", "success-msg_styles_advanced":"", "error_msg_styles_background-color":"", "error_msg_styles_border":"", "error_msg_styles_border-style":"", "error_msg_styles_border-color":"", "error_msg_styles_color":"", "error_msg_styles_height":"", "error_msg_styles_width":"", "error_msg_styles_font-size":"", "error_msg_styles_margin":"", "error_msg_styles_padding":"", "error_msg_styles_display":"", "error_msg_styles_show_advanced_css":"0", "error_msg_styles_advanced":"", "currency":"", "ninjaForms":"Ninja Forms", "changeEmailErrorMsg":"Please enter a valid email address!", "confirmFieldErrorMsg":"These fields must match!", "fieldNumberNumMinError":"Number Min Error", "fieldNumberNumMaxError":"Number Max Error", "fieldNumberIncrementBy":"Please increment by ", "fieldTextareaRTEInsertLink":"Insert Link", "fieldTextareaRTEInsertMedia":"Insert Media", "fieldTextareaRTESelectAFile":"Select a file", "formErrorsCorrectErrors":"Please correct errors before submitting this form.", "validateRequiredField":"This is a required field.", "honeypotHoneypotError":"Honeypot Error", "fileUploadOldCodeFileUploadInProgress":"File Upload in Progress.", "fileUploadOldCodeFileUpload":"FILE UPLOAD", "currencySymbol":false, "fieldsMarkedRequired":"Fields marked with an <span class=\"ninja-forms-req-symbol\">*<\/span> are required", "thousands_sep":",", "decimal_point":".", "currency_symbol":"&#36;", "beforeForm":"", "beforeFields":"", "afterFields":"", "afterForm":""};
                                                form.fields = [{"objectType":"Field", "objectDomain":"fields", "editActive":false, "order":1, "label":"Name", "key":"name", "type":"textbox", "created_at":"2016-08-24 16:39:20", "label_pos":"above", "required":1, "input_limit_type":"characters", "input_limit_msg":"Character(s) left", "wrap_styles_show_advanced_css":0, "label_styles_show_advanced_css":0, "element_styles_show_advanced_css":0, "cellcid":"c3277", "manual_key":false, "placeholder":"", "default":"", "container_class":"", "element_class":"", "input_limit":"", "disable_input":"", "admin_label":"", "help_text":"", "desc_text":"", "disable_browser_autocomplete":"", "mask":"", "custom_mask":"", "id":1, "beforeField":"", "afterField":"", "parentType":"textbox", "element_templates":["textbox", "input"], "old_classname":"", "wrap_template":"wrap"}, {"objectType":"Field", "objectDomain":"fields", "editActive":false, "order":2, "label":"Email", "key":"email", "type":"email", "created_at":"2016-08-24 16:39:20", "label_pos":"above", "required":1, "wrap_styles_show_advanced_css":0, "label_styles_show_advanced_css":0, "element_styles_show_advanced_css":0, "cellcid":"c3281", "default":"", "placeholder":"", "container_class":"", "element_class":"", "admin_label":"", "help_text":"", "desc_text":"", "id":2, "beforeField":"", "afterField":"", "parentType":"email", "element_templates":["email", "input"], "old_classname":"", "wrap_template":"wrap"}, {"objectType":"Field", "objectDomain":"fields", "editActive":false, "order":3, "label":"Phone Number", "type":"phone", "key":"phone_number_1487879262614", "label_pos":"above", "required":1, "input_limit_type":"characters", "input_limit_msg":"Character(s) left", "manual_key":false, "placeholder":"", "default":"", "container_class":"", "element_class":"", "input_limit":"", "disable_input":"", "admin_label":"", "help_text":"", "desc_text":"", "disable_browser_autocomplete":"", "mask":"", "custom_mask":"", "id":5, "beforeField":"", "afterField":"", "parentType":"textbox", "element_templates":["tel", "textbox", "input"], "old_classname":"", "wrap_template":"wrap"}, {"objectType":"Field", "objectDomain":"fields", "editActive":false, "order":4, "label":"Message", "key":"message", "type":"textarea", "created_at":"2016-08-24 16:39:20", "label_pos":"above", "required":0, "input_limit_type":"characters", "input_limit_msg":"Character(s) left", "wrap_styles_show_advanced_css":0, "label_styles_show_advanced_css":0, "element_styles_show_advanced_css":0, "cellcid":"c3284", "manual_key":false, "placeholder":"", "default":"", "container_class":"", "element_class":"", "input_limit":"", "disable_input":"", "admin_label":"", "help_text":"", "desc_text":"", "disable_browser_autocomplete":"", "textarea_rte":"", "disable_rte_mobile":"", "textarea_media":"", "id":3, "beforeField":"", "afterField":"", "parentType":"textarea", "element_templates":["textarea", "input"], "old_classname":"", "wrap_template":"wrap"}, {"objectType":"Field", "objectDomain":"fields", "editActive":false, "order":5, "label":"Submit", "key":"submit", "type":"submit", "created_at":"2016-08-24 16:39:20", "processing_label":"Processing", "wrap_styles_show_advanced_css":0, "label_styles_show_advanced_css":0, "element_styles_show_advanced_css":0, "submit_element_hover_styles_show_advanced_css":0, "cellcid":"c3287", "container_class":"", "element_class":"", "id":4, "beforeField":"", "afterField":"", "label_pos":"above", "parentType":"textbox", "element_templates":["submit", "button", "input"], "old_classname":"", "wrap_template":"wrap-no-label"}];
                                                /* Add Form Data to nfForms object */
                                                nfForms.push(form);                                    </script> -->

                                    <div class="ult-spacer spacer-58bfb9c46dfc9" data-id="58bfb9c46dfc9" data-height="100" data-height-mobile="60" data-height-tab="80" style="clear:both;display:block;"></div>
                                </div> 
                            </div> 
                        </div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div>


                </section>			
            </div>

            <div class="body-back-to-top align-right"><i class="dfd-added-font-icon-right-open"></i></div>




            <div id="footer-wrap">

                <section id="footer" class="footer-style-2 dfd-background-dark">

                    <div class="row">
                        <div class="three columns">
                            <section id="crum_contacts_widget-17" class="widget widget_crum_contacts_widget"><h3 class="widget-title">About company</h3>SERVISELL is dedicated to developing intuitive and highly effective sales solutions for our dealer clients nationwide. The success of our client is paramount. SERVISELL strives to provide the cutting-edge sales systems needed to give our dealers the advantage in today’s highly competitive marketplace.</section>			</div>
                        <div class="three columns">
                            <section id="dfd_vcard_simple-12" class="widget widget_dfd_vcard_simple"><h3 class="widget-title">Contact us</h3>
                                <div id="dfd-vcard-widget-58bfb9c47479a" class="row">
                                    <div class="twelve columns">
                                        <div class="dfd-vcard-wrap">
                                            <div class="vcard-field">
                                                <i class="dfd-icon-tablet2"></i>
                                                <p><font color="#ffffff">877.473.4280</font></p>
                                            </div>
                                            <div class="vcard-field">
                                                <i class="dfd-icon-email_1"></i>
                                                <p>
                                                    <a href="mailto:info@servisell.com" title="" >info@servisell.com</a>
                                            </div>
                                            <div class="vcard-field">
                                                <i class="dfd-icon-navigation"></i>
                                                <p>1001 Woodward Ave.<br />5th Floor<br />Detroit, MI 48226</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                            (function($) {
                                            $('head').append('<style>#dfd-vcard-widget-58bfb9c47479a .dfd-vcard-wrap {background: #003a63;}#dfd-vcard-widget-58bfb9c47479a .dfd-vcard-wrap {border-style: none;}#dfd-vcard-widget-58bfb9c47479a .dfd-vcard-wrap {-webkit-border-radius: 2px;-moz-border-radius: 2px;-o-border-radius: 2px;border-radius: 2px;}</style>');
                                            })(jQuery);                                </script>

                            </section>			</div>
                        <div class="three columns">
                            <section id="text-32" class="widget widget_text"><h3 class="widget-title">Quick Links</h3>			<div class="textwidget"><a href="/login">Client Login</a><br></br>

                                    <a href="/contact/">Email Servisell</a><br></br>

                                    <a href="/privacy-policy-terms-use/">Privacy Policy</a>
                                </div>
                            </section>			</div>
                        <div class="three columns">
                            <section id="text-33" class="widget widget_text"><h3 class="widget-title">Servisell powered by:</h3>			<div class="textwidget"><img src="/images/CDKGlobalLogo_color.png" alt="CDK Global logo" style="width:100px;height:80px;">
                                    <br>
                                    <img src="/wp-content/uploads/2017/02/NADA-Values_rgb-Jan-2017.png" alt="NADA" style="width:100px;height:72px;">



                                </div>
                            </section>			</div>
                    </div>

                </section>

                <section id="sub-footer" class=" dfd-background-dark">
                    <div class="row">
                        <div class="twelve columns subfooter-copyright text-center">
                            © copyright 2017 Servisell				</div>
                    </div>
                </section>

            </div>



        </div>

        <div id="sidr">
            <div class="sidr-top">
                <div class="logo-for-panel">
                    <div class="inline-block">
                        <a href="/">
                            <img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="sidr-inner"></div>
        </div>
        <a href="#sidr-close" class="dl-trigger dfd-sidr-close"></a>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat%3A">
        <link rel="stylesheet" href="/wp-includes/css/dashicons.min.css">
        <link rel="stylesheet" href="/wp-content/plugins/ninja-forms/assets/css/display-structure.css">
        <script type='text/javascript' src='/wp-includes/js/jquery/jquery.form.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-js/ultimate.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/plugins.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/hammer.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.smoothscroll.js'></script>
        <script type='text/javascript'>
                                            /* <![CDATA[ */
                                            var ajax_var = {"url":"\/wp-admin\/admin-ajax.php", "nonce":"ac471e7152"};
                                            /* ]]> */
        </script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/post-like.min.js'></script>
        <script type='text/javascript' src='/wp-includes/js/wp-embed.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.run.js'></script>
        <script type='text/javascript' src='/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
        <script type='text/javascript' src='/wp-includes/js/underscore.min.js'></script>
        <script type='text/javascript' src='/wp-includes/js/backbone.min.js'></script>
        <script type='text/javascript' src='/wp-content/plugins/ninja-forms/assets/js/min/front-end-deps.js'></script>
        <script type='text/javascript'>
                                            /* <![CDATA[ */
                                            var nfi18n = {"ninjaForms":"Ninja Forms", "changeEmailErrorMsg":"Please enter a valid email address!", "confirmFieldErrorMsg":"These fields must match!", "fieldNumberNumMinError":"Number Min Error", "fieldNumberNumMaxError":"Number Max Error", "fieldNumberIncrementBy":"Please increment by ", "fieldTextareaRTEInsertLink":"Insert Link", "fieldTextareaRTEInsertMedia":"Insert Media", "fieldTextareaRTESelectAFile":"Select a file", "formErrorsCorrectErrors":"Please correct errors before submitting this form.", "validateRequiredField":"This is a required field.", "honeypotHoneypotError":"Honeypot Error", "fileUploadOldCodeFileUploadInProgress":"File Upload in Progress.", "fileUploadOldCodeFileUpload":"FILE UPLOAD", "currencySymbol":"", "fieldsMarkedRequired":"Fields marked with an <span class=\"ninja-forms-req-symbol\">*<\/span> are required", "thousands_sep":",", "decimal_point":"."};
                                            var nfFrontEnd = {"ajaxNonce":"7cbecfa6d5", "adminAjax":"\/wp-admin\/admin-ajax.php", "requireBaseUrl":"\/wp-content\/plugins\/ninja-forms\/assets\/js\/", "use_merge_tags":{"user":{"address":"address", "textbox":"textbox", "button":"button", "checkbox":"checkbox", "city":"city", "date":"date", "email":"email", "firstname":"firstname", "html":"html", "hidden":"hidden", "lastname":"lastname", "listcheckbox":"listcheckbox", "listcountry":"listcountry", "listmultiselect":"listmultiselect", "listradio":"listradio", "listselect":"listselect", "liststate":"liststate", "note":"note", "number":"number", "password":"password", "passwordconfirm":"passwordconfirm", "product":"product", "quantity":"quantity", "recaptcha":"recaptcha", "shipping":"shipping", "spam":"spam", "starrating":"starrating", "submit":"submit", "terms":"terms", "textarea":"textarea", "total":"total", "unknown":"unknown", "zip":"zip", "hr":"hr"}, "post":{"address":"address", "textbox":"textbox", "button":"button", "checkbox":"checkbox", "city":"city", "date":"date", "email":"email", "firstname":"firstname", "html":"html", "hidden":"hidden", "lastname":"lastname", "listcheckbox":"listcheckbox", "listcountry":"listcountry", "listmultiselect":"listmultiselect", "listradio":"listradio", "listselect":"listselect", "liststate":"liststate", "note":"note", "number":"number", "password":"password", "passwordconfirm":"passwordconfirm", "product":"product", "quantity":"quantity", "recaptcha":"recaptcha", "shipping":"shipping", "spam":"spam", "starrating":"starrating", "submit":"submit", "terms":"terms", "textarea":"textarea", "total":"total", "unknown":"unknown", "zip":"zip", "hr":"hr"}, "system":{"address":"address", "textbox":"textbox", "button":"button", "checkbox":"checkbox", "city":"city", "date":"date", "email":"email", "firstname":"firstname", "html":"html", "hidden":"hidden", "lastname":"lastname", "listcheckbox":"listcheckbox", "listcountry":"listcountry", "listmultiselect":"listmultiselect", "listradio":"listradio", "listselect":"listselect", "liststate":"liststate", "note":"note", "number":"number", "password":"password", "passwordconfirm":"passwordconfirm", "product":"product", "quantity":"quantity", "recaptcha":"recaptcha", "shipping":"shipping", "spam":"spam", "starrating":"starrating", "submit":"submit", "terms":"terms", "textarea":"textarea", "total":"total", "unknown":"unknown", "zip":"zip", "hr":"hr"}, "fields":{"address":"address", "textbox":"textbox", "button":"button", "checkbox":"checkbox", "city":"city", "date":"date", "email":"email", "firstname":"firstname", "html":"html", "hidden":"hidden", "lastname":"lastname", "listcheckbox":"listcheckbox", "listcountry":"listcountry", "listmultiselect":"listmultiselect", "listradio":"listradio", "listselect":"listselect", "liststate":"liststate", "note":"note", "number":"number", "password":"password", "passwordconfirm":"passwordconfirm", "product":"product", "quantity":"quantity", "recaptcha":"recaptcha", "shipping":"shipping", "spam":"spam", "starrating":"starrating", "submit":"submit", "terms":"terms", "textarea":"textarea", "total":"total", "unknown":"unknown", "zip":"zip", "hr":"hr"}, "calculations":{"html":"html", "hidden":"hidden", "note":"note", "unknown":"unknown"}}, "opinionated_styles":""};
                                            /* ]]> */
        </script>
        <script type='text/javascript' src='/wp-content/plugins/ninja-forms/assets/js/min/front-end.js'></script>
    </body>
</html>