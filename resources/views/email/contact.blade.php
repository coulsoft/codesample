<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Contact</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <p>Message from contact page.</p>
        <p><br /></p>
        <p>Name: {{ $name }}</p>
        <p>E-Mail: {{ $email }}</p>
        <p>Phone: {{ $phone }}</p>
        <p>Message: </p>
        <p>{{ $user_message }}</p>
    </body>
</html>
