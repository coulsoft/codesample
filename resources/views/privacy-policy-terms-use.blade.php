<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js ie lt-ie10" lang="en-US"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-ie" lang="en-US"> <!--<![endif]-->

<head>

    <meta charset="utf-8">

    <title>Servisell | Privacy Policy and Terms of Use</title>

			<link rel="icon" type="image/png" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png" />
				<link rel="apple-touch-icon" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png">
				<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/2017/02/servisell-logo-web-76-02.png">
				<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/2017/02/servisell-logo-web-120-02.png">
		
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!--[if lte IE 9]>
        <script src="/wp-content/themes/ronneby/assets/js/html5shiv.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
        <script src="/wp-content/themes/ronneby/assets/js/excanvas.compiled.js"></script>
    <![endif]-->

				<style type="text/css">
				body {
					
					
											background-repeat: repeat !important;
					
									}
			</style>
		
    <meta name='robots' content='noindex,follow' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Feed" href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Comments Feed" href="/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" href="/wp-content/uploads//dfd_ronneby_fonts/ronneby/Defaults.css">
<link rel="stylesheet" href="/wp-content/plugins/revslider/public/assets/css/settings.css">
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/styled-button.css">
<link rel="stylesheet" href="/wp-content/uploads/smile_fonts/ronneby/Defaults.css">
<link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.min.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-css/ultimate.min.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/visual-composer.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/app.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/mobile-responsive.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/jquery.isotope.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/prettyPhoto.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby_child/style.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/css/options.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%7CDroid+Serif%3A400%2C700%2C400italic%2C700italic%7CLora%3A400%2C700%2C400italic%2C700italic%7CMontserrat%3A400%2C700&#038;subset=latin">
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
<link rel='https://api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.3" />
<link rel="canonical" href="/privacy-policy-terms-use/" />
<link rel='shortlink' href='/?p=13056' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=%2Fprivacy-policy-terms-use%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=%2Fprivacy-policy-terms-use%2F&#038;format=xml" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		
<style type="text/css">
.a-stats {
	width: auto;
}
.a-stats a {
	background: #7CA821;
	background-image:-moz-linear-gradient(0% 100% 90deg,#5F8E14,#7CA821);
	background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#7CA821),to(#5F8E14));
	border: 1px solid #5F8E14;
	border-radius:3px;
	color: #CFEA93;
	cursor: pointer;
	display: block;
	font-weight: normal;
	height: 100%;
	-moz-border-radius:3px;
	padding: 7px 0 8px;
	text-align: center;
	text-decoration: none;
	-webkit-border-radius:3px;
	width: 100%;
}
.a-stats a:hover {
	text-decoration: none;
	background-image:-moz-linear-gradient(0% 100% 90deg,#6F9C1B,#659417);
	background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#659417),to(#6F9C1B));
}
.a-stats .count {
	color: #FFF;
	display: block;
	font-size: 15px;
	line-height: 16px;
	padding: 0 13px;
	white-space: nowrap;
}
</style>

<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution  - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">jQuery(document).ready(function(){
					jQuery(".ult_modal-body iframe").each(function(index, element) {
						var w = jQuery(this).attr("width");
						var h = jQuery(this).attr("height");
						var st = '<style type="text/css" id="modal-css">';
							st += "#"+jQuery(this).closest(".ult-overlay").attr("id")+" iframe{width:"+w+"px !important;height:"+h+"px !important;}";
							st += ".fluid-width-video-wrapper{padding: 0 !important;}";
							st += "</style>";
						jQuery("head").append(st);
					});
				});</script><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1487778467065{padding-top: 60px !important;padding-bottom: 60px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	
	
</head>
<body class="page-template page-template-page-custom-with-header page-template-page-custom-with-header-php page page-id-13056 privacy-policy-terms-use wpb-js-composer js-comp-ver-4.12 vc_responsive" data-directory="/wp-content/themes/ronneby" data-header-responsive-width="1101">
		

	<div class="form-search-section" style="display: none;">
	<div class="row">
		<form role="search" method="get" id="searchform_58bfb73cc6bbe" class="form-search" action="/">
	<i class="dfdicon-header-search-icon inside-search-icon"></i>
	<input type="text" value="" name="s" id="s_58bfb73cc6b78" class="search-query" placeholder="Search on site...">
	<input type="submit" value="Search" class="btn">
	<i class="header-search-switcher close-search"></i>
	</form>	</div>
</div><div id="header-container" class="header-style-2 header-layout-fullwidth sticky-header-enabled simple logo-position-left menu-position-top  dfd-enable-mega-menu dfd-enable-headroom dfd-header-layout-fixed with-top-panel">
	<section id="header">
					<div class="header-top-panel">
				<div class="row">
					<div class="columns twelve header-info-panel">
							<div class="top-info"><i class="dfd-icon-phone"></i><span class="dfd-top-info-delim-blank"></span>877.473.4280<span class="dfd-top-info-delim"></span><i class="dfd-icon-email_2"></i><span class="dfd-top-info-delim-blank"></span>contact@servisell.com</div>
						
													<div class="login-header">
			<div id="loginModal" class="reveal-modal">
			<h3 class="login_form_title">Login on site</h3>
			<form name="loginform_58bfb73cc8ed5" id="loginform_58bfb73cc8ed5" action="/wp-login.php" method="post">
				
				<p class="login-username">
					<label for="user_login_58bfb73cc8f45">Username</label>
					<input type="text" name="log" id="user_login_58bfb73cc8f45" class="input" value="" size="20" placeholder="Login" />
				</p>
				<p class="login-password">
					<label for="user_pass_58bfb73cc8f8a">Password</label>
					<input type="password" name="pwd" id="user_pass_58bfb73cc8f8a" class="input" value="" size="20" placeholder="Password"  />
				</p>
				<p class="login-submit">
					<button type="submit" name="wp-submit" id="wp-submit_58bfb73cc9054" class="button"><i class="outlinedicon-lock-closed"></i>Login on site</button>
					<input type="hidden" name="redirect_to" value="/privacy-policy-terms-use/" />
				</p>
				<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme_58bfb73cc8fcf" value="forever" /> Remember Me</label></p><p class="login-lost-password"><label><a href="/wp-login.php?action=lostpassword">Remind the password</a></label></p><p class="clear"></p><p class="login-registration">
					
				</p>
			</form>			<a class="close-reveal-modal">&#215;</a>
		</div>

		<div class="links">
			<a href="/wp-login.php?redirect_to=%2Fprivacy-policy-terms-use%2F" class="drop-login" data-reveal-id="loginModal">
				<i class="dfd-icon-lock"></i>
				<span>Login on site</span>
			</a>
		</div>
	</div>												
													<div class="widget soc-icons dfd-soc-icons-hover-style-26">
								<a href="http://dribbble.com" class="dr soc_icon-dribbble" title="Dribbble" target="_blank"><span class="line-top-left soc_icon-dribbble"></span><span class="line-top-center soc_icon-dribbble"></span><span class="line-top-right soc_icon-dribbble"></span><span class="line-bottom-left soc_icon-dribbble"></span><span class="line-bottom-center soc_icon-dribbble"></span><span class="line-bottom-right soc_icon-dribbble"></span><i class="soc_icon-dribbble"></i></a><a href="http://facebook.com" class="fb soc_icon-facebook" title="Facebook" target="_blank"><span class="line-top-left soc_icon-facebook"></span><span class="line-top-center soc_icon-facebook"></span><span class="line-top-right soc_icon-facebook"></span><span class="line-bottom-left soc_icon-facebook"></span><span class="line-bottom-center soc_icon-facebook"></span><span class="line-bottom-right soc_icon-facebook"></span><i class="soc_icon-facebook"></i></a><a href="http://twitter.com" class="tw soc_icon-twitter-3" title="Twitter" target="_blank"><span class="line-top-left soc_icon-twitter-3"></span><span class="line-top-center soc_icon-twitter-3"></span><span class="line-top-right soc_icon-twitter-3"></span><span class="line-bottom-left soc_icon-twitter-3"></span><span class="line-bottom-center soc_icon-twitter-3"></span><span class="line-bottom-right soc_icon-twitter-3"></span><i class="soc_icon-twitter-3"></i></a><a href="https://vimeo.com/" class="vi soc_icon-vimeo" title="Vimeo" target="_blank"><span class="line-top-left soc_icon-vimeo"></span><span class="line-top-center soc_icon-vimeo"></span><span class="line-top-right soc_icon-vimeo"></span><span class="line-bottom-left soc_icon-vimeo"></span><span class="line-bottom-center soc_icon-vimeo"></span><span class="line-bottom-right soc_icon-vimeo"></span><i class="soc_icon-vimeo"></i></a>							</div>
																							</div>
									</div>
			</div>
						<div class="header-wrap">
			<div class="row decorated">
				<div class="columns twelve header-main-panel">
					<div class="header-col-left">
						<div class="mobile-logo">
																<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
													</div>
																														</div>
					<div class="header-col-right text-center clearfix">
															<div class="header-icons-wrapper">
									<div class="dl-menuwrapper">
	<a href="#sidr" class="dl-trigger icon-mobile-menu dfd-vertical-aligned" id="mobile-menu">
		<span class="icon-wrap dfd-middle-line"></span>
		<span class="icon-wrap dfd-top-line"></span>
		<span class="icon-wrap dfd-bottom-line"></span>
	</a>
</div>																												<div class="form-search-wrap">
		<a href="#" class="header-search-switcher dfd-icon-zoom"></a>
	</div>
																	</div>
																			</div>
					<div class="header-col-fluid">
													<a href="/" title="Home" class="fixed-header-logo">
								<img src="/wp-content/uploads/2017/02/servisell-logo-web.png" alt="logo"/>
							</a>
												<nav class="mega-menu clearfix  text-right" id="main_mega_menu">
	<ul id="menu-twenty_fifth_onepage" class="nav-menu menu-primary-navigation menu-clonable-for-mobiles"><li id="nav-menu-item-13014-58bfb73cdef55" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/" class="menu-link main-menu-link item-title">Home</a></li>
<li id="nav-menu-item-13212-58bfb73cdf0bc" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/test-drive/" class="menu-link main-menu-link item-title">Test Drive</a></li>
<li id="nav-menu-item-13012-58bfb73cdf1d5" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/contact/" class="menu-link main-menu-link item-title">Contact</a></li>
<li id="nav-menu-item-13022-58bfb73cdf2ee" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/login" class="menu-link main-menu-link item-title">Login</a></li>
</ul>	<i class="carousel-nav prev dfd-icon-left_2"></i>
	<i class="carousel-nav next dfd-icon-right_2"></i>
</nav>
					</div>
				</div>
			</div>
		</div>
			</section>
	</div>	
	<div id="main-wrap" class="">

		<div id="change_wrap_div">

			
<div id="stuning-header">
	<div class="dfd-stuning-header-bg-container" style=" background-color: #ffffff; background-size: initial;background-attachment: fixed;background-position: center;">
			</div>
	<div class="stuning-header-inner">
		<div class="row">
			<div class="twelve columns">
				<div class="page-title-inner  text-center" >
					<div class="page-title-inner-wrap">
																			<h1 class="page-title">
								Privacy Policy and Terms of Use							</h1>
																													</div>
									</div>
			</div>
		</div>
	</div>
</div>


<section id="layout" class="no-title">


        	<div  class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1487778467065" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns twelve"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><b>Terms of Use</b></p>
<p>Copyright 2016-2017, BSD Partners, LLC dba SERVISELL (“SERVISELL”). All rights reserved. All text, images, graphics, animation, videos, music, sounds, and other materials on this website (&#8220;site&#8221;) are subject to the copyrights and other intellectual property rights of SERVISELL, its affiliated companies, and its licensors. SERVISELL owns the copyrights in the selection, coordination, and arrangement of the materials on this site. These materials may not be copied for commercial use or distribution, nor may these materials be modified or reposted to other sites.</p>
<p><b>SERVISELL Websites Privacy Policy</b></p>
<p><b>Effective February 20</b><b>th</b><b>, 2017</b></p>
<p><b>Statement</b></p>
<p>SERVISELL respects the privacy and information of all SERVISELL Website users (“Users”) and their respective customers.  This Policy sets forth how SERVISELL will manage individually identifiable information and other information about its Users (“User Information”) and data that Users input, store or transmit through the use of the SERVISELL.com website in the normal course of business about their respective customers (“Customer Information”).  Neither User Information nor Customer Information is ever willingly disclosed to any third party without first receiving express permission or as authorized by the applicable Dealer Subscription Agreement or this “Terms of Use Policy” as incorporated into these agreements and in compliance with all applicable laws and regulations.</p>
<p><b>Cookies</b></p>
<p>SERVISELL will collect information called Cookies that aids the User in their access and use of SERVISELL.com.</p>
<p><b>What are Cookies?</b></p>
<p>Cookies are small amounts of data that a website can send to a web browser on a User’s machine, e.g., personal computer or laptop, tablet, mobile device, ect., (“Computing Device”) where it is stored for some period of time. A cookie may be used to authenticate the User or the Computing Device accessing the website. Information gathered from using cookies may include general information about the Computing Device used to access the site.</p>
<p><b>Types of Cookies</b></p>
<p>Cookies can be set to expire:</p>
<ul>
<li>On a specified date</li>
<li>After a specific period of time</li>
<li>When a transaction has been completed</li>
<li>Or when a user closes his/her browser</li>
</ul>
<p>A cookie that is erased from memory when a visitor’s browser closes is called a “session” cookie. Cookies that expire based on a time set by the Web Server are called “persistent” cookies.</p>
<p>Persistent cookies may be used in some cases, for example, to authenticate Computing Devices that have accessed the website previously or otherwise recognize when a visitor returns to a web site. The web browser will send the cookie information from the initial visit back to the website. This is useful to the visitor if he or she has established secure access or a password at a particular website and wants to be identified by that website to perform a function or transaction.</p>
<p>SERVISELL will use both persistent and session cookies to implement a method of Muti-Factor Authentication. The method to be used is Machine Authentication.</p>
<p>Machine authentication provides validation of the User’s Computing Device in a way that secures against a variety of threats while minimizing User impact. This is an especially attractive method where Users typically access their account from a regular set of Computing Devices, allowing for authentication to be performed without any noticeable impact to the User experience.</p>
<p><b>Why we Collect Information</b></p>
<p>SERVISELL will collect certain User Information for the purpose to establish: login credentials and communication through SERVISELL.com with the User, as well as information that will aide User access to SERVISELL.com and other information required to provide the services authorized by the Dealer Subscription Agreement as incorporated into these agreements.</p>
<p>SERVISELL does not independently collect dealer or finance source Customer Information.</p>
<p><b>Notice of the Privacy Policy and Your Consent to the Privacy Policy</b></p>
<p>SERVISELL provides notice of its Privacy Policy through its web based systems (“SERVISELL.com”). Each page of the SERVISELL Website contains a hypertext link to the SERVISELL Privacy Policy and is available to all visitors or Users at any time while accessing the SERVISELL Website by clicking on the Privacy Policy link.</p>
<p>By accessing  the SERVISELL Website, you consent to the collection and use of your User Information as we have outlined in this policy and use of your Customer Information to provide the services authorized by the applicable Dealer Subscription Agreement as incorporated into these agreements.  SERVISELL may decide to change this Privacy Policy from time to time. When we do, we will post those changes on the SERVISELL Website so that you are always aware of the information we collect, how we use it, and under what circumstances we disclose it.</p>
<p><b>What Information Do We Collect?</b></p>
<p>SERVISELL collects User Information and processes Customer Information input by its Users as required to provide the services authorized by the applicable Dealer Subscription Agreement as incorporated into these agreements. SERVISELL does not collect any unique information about Users (such as your name, email address, etc.) except when you specifically and knowingly provide such information.</p>
<p><b>Use, Retention and Disposal </b></p>
<p>SERVISELL will only use and retain Customer Information or User Information to provide the services (e.g., process the customer information through SERVISELL.com or aide User access to SERVISELL.com) authorized by the applicable Dealer Subscription Agreement as incorporated into these agreements.</p>
<p>Disposal of Customer Information is performed in a secure manner in compliance with all applicable laws and regulations.</p>
<p><b>Access</b></p>
<p>Access to Customer Information or User Information through the SERVISELL Website is restricted to SERVISELL and its Users. SERVISELL does not grant third parties access to Customer Information or User Information without the User&#8217;s express authorization or specifically authorized by the applicable Dealer Subscription Agreement and shall do so only in compliance with all applicable laws and regulations.</p>
<p><b>With Whom Does SERVISELL Share Information</b></p>
<p>SERVISELL will not share User Information or Customer Information with third parties other than in the performance of those services without the User&#8217;s express authorization or as authorized by the applicable Dealer Subscription Agreement as incorporated into these agreements and shall do so only in compliance with all applicable laws and regulations. We will, however, release specific information if required to do so in order to comply with any valid legal process such as a search warrant, subpoena, statute, or court order.</p>
<p><b>Security for User and Customer Information</b></p>
<p>SERVISELL has agreed with each of its dealer customers to maintain commercially reasonable physical, electronic, and procedural controls and safeguards designed to comply with all applicable laws and regulations to protect the Customer Information received from dealer from unauthorized disclosure.  The controls include, but are not limited to, maintaining appropriate safeguards to restrict access to the Customer Information to those employees, agents, or service providers of SERVISELL who need such information to provide the services authorized by the applicable Dealer Subscription Agreement as incorporated into these agreements.  For data disclosed in electronic form to SERVISELL, the safeguards also include electronic barriers (e.g., firewalls or similar barriers) and password-protected access and for information disclosed in written form, such safeguards include secured storage.</p>
<p><b>Quality</b></p>
<p>All Customer Information and User Information is input into SERVISELL Website by its Users and is not independently altered at any time by SERVISELL’s employees, agents, or service providers unless expressly authorized by the respective dealer;  therefore the quality of the Customer Information and User Information is the responsibility of such dealers and finance sources.</p>
<p><b>Who Can I Ask If I Have Additional Questions?</b></p>
<p>SERVISELL monitors the SERVISELL Website usage for all purposes deemed necessary by SERVISELL, including without limitation, to ensure proper working order, appropriate use, and the security of data.</p>
<p>Feel free to contact us any time and we&#8217;ll answer any additional questions you may have. Our Dealer Support email address is <a href="mailto:support@SERVISELL.com">support@SERVISELL.com</a>.</p>

		</div> 
	</div> </div></div></div></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div>
	

</section>
			
		</div>
		
		<div class="body-back-to-top align-right"><i class="dfd-added-font-icon-right-open"></i></div>

		


<div id="footer-wrap">
	
	<section id="footer" class="footer-style-2 dfd-background-dark">

			<div class="row">
					<div class="three columns">
				<section id="crum_contacts_widget-17" class="widget widget_crum_contacts_widget"><h3 class="widget-title">About company</h3>SERVISELL is dedicated to developing intuitive and highly effective sales solutions for our dealer clients nationwide. The success of our client is paramount. SERVISELL strives to provide the cutting-edge sales systems needed to give our dealers the advantage in today’s highly competitive marketplace.</section>			</div>
					<div class="three columns">
				<section id="dfd_vcard_simple-12" class="widget widget_dfd_vcard_simple"><h3 class="widget-title">Contact us</h3>
		<div id="dfd-vcard-widget-58bfb73d6b94d" class="row">
			<div class="twelve columns">
				<div class="dfd-vcard-wrap">
											<div class="vcard-field">
							<i class="dfd-icon-tablet2"></i>
														<p><font color="#ffffff">877.473.4280</font></p>
						</div>
																<div class="vcard-field">
							<i class="dfd-icon-email_1"></i>
														<p>
															<a href="mailto:info@servisell.com" title="" >info@servisell.com</a>
													</div>
																<div class="vcard-field">
							<i class="dfd-icon-navigation"></i>
														<p>1001 Woodward Ave.<br />5th Floor<br />Detroit, MI 48226</p>
						</div>
														</div>
			</div>
		</div>
				<script type="text/javascript">
			(function($) {
				$('head').append('<style>#dfd-vcard-widget-58bfb73d6b94d .dfd-vcard-wrap {background: #003a63;}#dfd-vcard-widget-58bfb73d6b94d .dfd-vcard-wrap {border-style: none;}#dfd-vcard-widget-58bfb73d6b94d .dfd-vcard-wrap {-webkit-border-radius: 2px;-moz-border-radius: 2px;-o-border-radius: 2px;border-radius: 2px;}</style>');
			})(jQuery);
		</script>
				
		</section>			</div>
					<div class="three columns">
				<section id="text-32" class="widget widget_text"><h3 class="widget-title">Quick Links</h3>			<div class="textwidget"><a href="/login">Client Login</a><br></br>

<a href="/contact/">Email Servisell</a><br></br>

<a href="/privacy-policy-terms-use/">Privacy Policy</a>
</div>
		</section>			</div>
					<div class="three columns">
				<section id="text-33" class="widget widget_text"><h3 class="widget-title">Servisell powered by:</h3>			<div class="textwidget"><img src="/images/CDKGlobalLogo_color.png" alt="CDK Global logo" style="width:100px;height:80px;">
<br>
<img src="/wp-content/uploads/2017/02/NADA-Values_rgb-Jan-2017.png" alt="NADA" style="width:100px;height:72px;">



</div>
		</section>			</div>
			</div>

	</section>

			<section id="sub-footer" class=" dfd-background-dark">
			<div class="row">
				<div class="twelve columns subfooter-copyright text-center">
					© copyright 2017 Servisell				</div>
			</div>
		</section>
	
</div>



</div>

<div id="sidr">
	<div class="sidr-top">
						<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
			</div>
	<div class="sidr-inner"></div>
		</div>
<a href="#sidr-close" class="dl-trigger dfd-sidr-close"></a>

<script type='text/javascript' src='/wp-includes/js/jquery/jquery.form.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-js/ultimate.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/plugins.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/hammer.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.smoothscroll.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajax_var = {"url":"\/wp-admin\/admin-ajax.php","nonce":"ac471e7152"};
/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/post-like.min.js'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.run.js'></script>
<script type='text/javascript' src='/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
</body>
</html>
