@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="css/one-page-wonder.css" rel="stylesheet">

<!-- Full Width Image Header -->
<header class="header-image">
    <div class="headline">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">I want SERVISELL in my store.</div>
                        <div class="panel-body">
                            <div>
                                <p>Please provide the following and we'll get the ball rolling!</p>
                            </div>
                            {!! Form::open(['url' => url('/contact', [], true),
                            'method' => 'POST', 'class' => 'form-horizontal',
                            'role' => 'form']) !!}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                {!! Form::label('phone', 'Phone: ',
                                ['class' => 'col-md-4 control-label']) !!}

                                <div class="col-md-6">
                                    {!! Form::text('phone', '', ['class' => 'form-control']) !!}

                                    @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                {!! Form::label('message', 'Note: ',
                                ['class' => 'col-md-4 control-label']) !!}

                                <div class="col-md-6">
                                    {!! Form::textarea('message', '', ['class' => 'form-control',
                                    'rows' => '5']) !!}

                                    @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Previous contact form                            
                                                        <div class="form-group{{ $errors->has('dealership') ? ' has-error' : '' }}">
                                                            {!! Form::label('dealership', 'Dealership Name: ',
                                                            ['class' => 'col-md-4 control-label']) !!}
                            
                                                            <div class="col-md-6">
                                                                {!! Form::text('dealership', '', ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
                                                            {!! Form::label('address1', 'Address Line 1: ',
                                                            ['class' => 'col-md-4 control-label']) !!}
                            
                                                            <div class="col-md-6">
                                                                {!! Form::text('address1', '', ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
                                                            {!! Form::label('address2', 'Address Line 2: ',
                                                            ['class' => 'col-md-4 control-label']) !!}
                            
                                                            <div class="col-md-6">
                                                                {!! Form::text('address2', '', ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                                            {!! Form::label('city', 'City: ',
                                                            ['class' => 'col-md-4 control-label']) !!}
                            
                                                            <div class="col-md-6">
                                                                {!! Form::text('city', '', ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                                            {!! Form::label('state', 'State: ',
                                                            ['class' => 'col-md-4 control-label']) !!}
                            
                                                            <div class="col-md-6">
                                                                <span class='form-control'>{!! Form::select('state', ['  ' => 'Select a state',
                                                                    'AL' => 'Alabama',
                                                                    'AK' => 'Alaska',
                                                                    'AZ' => 'Arizona',
                                                                    'AR' => 'Arkansas',
                                                                    'CA' => 'California',
                                                                    'CO' => 'Colorado',
                                                                    'CT' => 'Connecticut',
                                                                    'DE' => 'Delaware',
                                                                    'DC' => 'District Of Columbia',
                                                                    'FL' => 'Florida',
                                                                    'GA' => 'Georgia',
                                                                    'HI' => 'Hawaii',
                                                                    'ID' => 'Idaho',
                                                                    'IL' => 'Illinois',
                                                                    'IN' => 'Indiana',
                                                                    'IA' => 'Iowa',
                                                                    'KS' => 'Kansas',
                                                                    'KY' => 'Kentucky',
                                                                    'LA' => 'Louisiana',
                                                                    'ME' => 'Maine',
                                                                    'MD' => 'Maryland',
                                                                    'MA' => 'Massachusetts',
                                                                    'MI' => 'Michigan',
                                                                    'MN' => 'Minnesota',
                                                                    'MS' => 'Mississippi',
                                                                    'MO' => 'Missouri',
                                                                    'MT' => 'Montana',
                                                                    'NE' => 'Nebraska',
                                                                    'NV' => 'Nevada',
                                                                    'NH' => 'New Hampshire',
                                                                    'NJ' => 'New Jersey',
                                                                    'NM' => 'New Mexico',
                                                                    'NY' => 'New York',
                                                                    'NC' => 'North Carolina',
                                                                    'ND' => 'North Dakota',
                                                                    'OH' => 'Ohio',
                                                                    'OK' => 'Oklahoma',
                                                                    'OR' => 'Oregon',
                                                                    'PA' => 'Pennsylvania',
                                                                    'RI' => 'Rhode Island',
                                                                    'SC' => 'South Carolina',
                                                                    'SD' => 'South Dakota',
                                                                    'TN' => 'Tennessee',
                                                                    'TX' => 'Texas',
                                                                    'UT' => 'Utah',
                                                                    'VT' => 'Vermont',
                                                                    'VA' => 'Virginia',
                                                                    'WA' => 'Washington',
                                                                    'WV' => 'West Virginia',
                                                                    'WI' => 'Wisconsin',
                                                                    'WY' => 'Wyoming']) !!}</span>
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                                                            {!! Form::label('zip', 'Zip: ',
                                                            ['class' => 'col-md-4 control-label']) !!}
                            
                                                            <div class="col-md-6">
                                                                {!! Form::text('zip', '', ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                            -->

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>Submit
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@endsection
