@extends('layouts.admin')

@section('content-header')
@endsection

@section('content')
<?php
$message = session('message');
$currentYear = \Carbon\Carbon::now()->year;
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Settings</div>
                <div class="panel-body">
                    {!! Form::open(['url' => url('/dealer', [], true),
                    'method' => 'POST', 'class' => 'form-horizontal',
                    'permission' => 'form']) !!}
                    
                    <div class="form-group{{ $errors->has('include_previous') ? ' has-error' : '' }}">
                        {!! Form::label('include_previous', 'Show cars from the following year to present:',
                        ['class' => 'col-md-6 control-label']) !!}

                        <div class="col-md-2">
                            {!! Form::text('include_previous',
                                ($dealership->include_previous > 0) ?
                                ($currentYear - $dealership->include_previous + 1) : '', [
                                'class' => 'form-control',
                                'placeholder' => '0 = All']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('omit_new') ? ' has-error' : '' }}">
                        {!! Form::label('omit_new', 'Exclude cars from the following year to present:',
                        ['class' => 'col-md-6 control-label']) !!}

                        <div class="col-md-2">
                            {!! Form::text('omit_new', 
                                ($dealership->omit_new > 0) ?
                                ($currentYear - $dealership->omit_new + 1) : '', [
                                'class' => 'form-control',
                                'placeholder' => '0 = none']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary fa fa-btn']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    @if(isset($message) && isset($errors) && (count($errors) > 0))
                    <div class='row'>
                        <div class='col-md-12 text-red'>
                            {!! $message !!}
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection