@extends('layouts.admin')

@section('sidebar-menu')
<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li><a href="{{ url('/nada') }}"><i class="fa fa-book"></i> <span>Nada Search</span></a></li>
    <li><a href="{{ url('/service') }}"><i class="fa fa-book"></i> <span>Service</span></a></li>
    <li><a href="{{ url('/sales') }}"><i class="fa fa-book"></i> <span>Sales</span></a></li>
    <li><a href="{{ url('/roles/create') }}"><i class="fa fa-book"></i> <span>Roles</span></a></li>
    <li><a href="{{ url('/permissions/create') }}"><i class="fa fa-book"></i> <span>Permissions</span></a></li>
    <li><a href="{{ url('/profiles') }}"><i class="fa fa-book"></i> <span>Profiles</span></a></li>
</ul>
@endsection

@section('content-header')
@endsection

@section('content')
@endsection