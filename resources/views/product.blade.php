@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="css/one-page-wonder.css" rel="stylesheet">

<div class="text-center" style="background-image: url('./images/product_background.jpg'); width: 100%; margin-top: 107px; color: white; padding-top: 16px; padding-bottom: 16px;">
    <h2>
        Your SERVICE DEPARTMENT is likely to generate ten times the customer traffic
        <br />
        that you see on your SHOWROOM floor every day.<br />
        <br />
        SERVISELL is an automated, web-based service designed to help you
        <br />
        INSTANTLY HARVEST sales customers from your daily service traffic.<br />
        <br />
        By combining the power of Instant Pre-Screen data with NADA trade values,<br />
        SERVISELL will instantly identify:<br />
        <br />
        Service Vehicle Trade Equity<br />
        Customer Credit Attributes<br />
        Vehicle Payment and Loan Data<br />
        <br />
        Within SECONDS of Service Department Check-In, Your Sales Department will be equipped with all the<br />
        information needed to truly help your Service Customer optimize their vehicle ownership position.<br />
        <br />
        <a href="https://docs.google.com/presentation/d/1V1QfvqZRvOam9EY7HKcw0ECggvcFZcw5mHare7YDqEg/present?slide=id.p" target="_blank">Click Here</a>
        <span style="font-size: small; position: relative; top: -16px;"><br/>to see a brief presentation on the SERVISELL process</span>
    </h2>
</div>


<!-- Page Content -->
<!-- /.container -->
@endsection