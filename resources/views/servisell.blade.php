<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js ie lt-ie10" lang="en-US"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-ie" lang="en-US"> <!--<![endif]-->

<head>

    <meta charset="utf-8">

    <title>Servisell</title>

			<link rel="icon" type="image/png" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png" />
				<link rel="apple-touch-icon" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png">
				<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/2017/02/servisell-logo-web-76-02.png">
				<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/2017/02/servisell-logo-web-120-02.png">
		
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!--[if lte IE 9]>
        <script src="/wp-content/themes/ronneby/assets/js/html5shiv.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
        <script src="/wp-content/themes/ronneby/assets/js/excanvas.compiled.js"></script>
    <![endif]-->

				<style type="text/css">
				body {
					
					
											background-repeat: repeat !important;
					
									}
			</style>
		
    <meta name='robots' content='noindex,follow' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Feed" href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Comments Feed" href="/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Home Comments Feed" href="/25_main/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" href="/wp-content/uploads//dfd_ronneby_fonts/ronneby/Defaults.css">
<link rel="stylesheet" href="/wp-content/plugins/revslider/public/assets/css/settings.css">
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/styled-button.css">
<link rel="stylesheet" href="/wp-content/uploads/smile_fonts/ronneby/Defaults.css">
<link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.min.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-css/ultimate.min.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/visual-composer.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/app.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/mobile-responsive.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/jquery.isotope.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/prettyPhoto.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby_child/style.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/css/options.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%7CDroid+Serif%3A400%2C700%2C400italic%2C700italic%7CLora%3A400%2C700%2C400italic%2C700italic%7CMontserrat%3A400%2C700&#038;subset=latin">
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
<link rel='https://api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.3" />
<link rel="canonical" href="/" />
<link rel='shortlink' href='/' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=%2F&#038;format=xml" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		
<style type="text/css">
.a-stats {
	width: auto;
}
.a-stats a {
	background: #7CA821;
	background-image:-moz-linear-gradient(0% 100% 90deg,#5F8E14,#7CA821);
	background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#7CA821),to(#5F8E14));
	border: 1px solid #5F8E14;
	border-radius:3px;
	color: #CFEA93;
	cursor: pointer;
	display: block;
	font-weight: normal;
	height: 100%;
	-moz-border-radius:3px;
	padding: 7px 0 8px;
	text-align: center;
	text-decoration: none;
	-webkit-border-radius:3px;
	width: 100%;
}
.a-stats a:hover {
	text-decoration: none;
	background-image:-moz-linear-gradient(0% 100% 90deg,#6F9C1B,#659417);
	background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#659417),to(#6F9C1B));
}
.a-stats .count {
	color: #FFF;
	display: block;
	font-size: 15px;
	line-height: 16px;
	padding: 0 13px;
	white-space: nowrap;
}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93252040-1', 'auto');
  ga('send', 'pageview');

</script>

<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution  - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">jQuery(document).ready(function(){
					jQuery(".ult_modal-body iframe").each(function(index, element) {
						var w = jQuery(this).attr("width");
						var h = jQuery(this).attr("height");
						var st = '<style type="text/css" id="modal-css">';
							st += "#"+jQuery(this).closest(".ult-overlay").attr("id")+" iframe{width:"+w+"px !important;height:"+h+"px !important;}";
							st += ".fluid-width-video-wrapper{padding: 0 !important;}";
							st += "</style>";
						jQuery("head").append(st);
					});
				});</script><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1488688397650{padding-top: 15px !important;padding-bottom: 25px !important;background-color: #ffffff !important;}.vc_custom_1487815840985{background-color: #e9e9e9 !important;}.vc_custom_1487899642502{padding-top: 150px !important;padding-bottom: 150px !important;}.vc_custom_1488854448663{background-color: #e9e9e9 !important;}.vc_custom_1488855003554{background-color: #e9e9e9 !important;}.vc_custom_1487803367276{background-color: #003a63 !important;}.vc_custom_1487790156928{padding-right: 7% !important;padding-bottom: 60px !important;padding-left: 7% !important;background-color: #003a63 !important;}.vc_custom_1487809445475{padding-top: 150px !important;padding-bottom: 150px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	
	
</head>
<body class="home page-template page-template-page-custom page-template-page-custom-php page wpb-js-composer js-comp-ver-4.12 vc_responsive" data-directory="/wp-content/themes/ronneby" data-header-responsive-width="1101">
		

	<div class="form-search-section" style="display: none;">
	<div class="row">
		<form role="search" method="get" id="searchform_58bfaeff091ea" class="form-search" action="/">
	<i class="dfdicon-header-search-icon inside-search-icon"></i>
	<input type="text" value="" name="s" id="s_58bfaeff091a3" class="search-query" placeholder="Search on site...">
	<input type="submit" value="Search" class="btn">
	<i class="header-search-switcher close-search"></i>
	</form>	</div>
</div><div id="header-container" class="header-style-1 header-layout-fullwidth sticky-header-enabled simple logo-position-left menu-position-top  dfd-enable-mega-menu dfd-enable-headroom with-top-panel">
	<section id="header">
					<div class="header-top-panel">
				<div class="row">
					<div class="columns twelve header-info-panel">
							<div class="top-info"><i class="dfd-icon-phone"></i><span class="dfd-top-info-delim-blank"></span>877.473.4280<span class="dfd-top-info-delim"></span><i class="dfd-icon-email_2"></i><span class="dfd-top-info-delim-blank"></span>contact@servisell.com</div>
						
												
																							</div>
									</div>
			</div>
						<div class="header-wrap">
			<div class="row decorated">
				<div class="columns twelve header-main-panel">
					<div class="header-col-left">
													<div class="mobile-logo">
																		<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
															</div>
																				<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
																	</div>
					<div class="header-col-right text-center clearfix">
															<div class="header-icons-wrapper">
									<div class="dl-menuwrapper">
	<a href="#sidr" class="dl-trigger icon-mobile-menu dfd-vertical-aligned" id="mobile-menu">
		<span class="icon-wrap dfd-middle-line"></span>
		<span class="icon-wrap dfd-top-line"></span>
		<span class="icon-wrap dfd-bottom-line"></span>
	</a>
</div>																																												</div>
																			</div>
					<div class="header-col-fluid">
													<a href="/" title="Home" class="fixed-header-logo">
								<img src="/wp-content/uploads/2017/02/servisell-logo-web.png" alt="logo"/>
							</a>
												<nav class="mega-menu clearfix  text-right" id="main_mega_menu">
	<ul id="menu-twenty_fifth_onepage" class="nav-menu menu-primary-navigation menu-clonable-for-mobiles"><li id="nav-menu-item-13014-58bfaeff1b886" class="mega-menu-item nav-item menu-item-depth-0 current-menu-item "><a href="/" class="menu-link main-menu-link item-title">Home</a></li>
<li id="nav-menu-item-13212-58bfaeff1b9c9" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/test-drive/" class="menu-link main-menu-link item-title">Test Drive</a></li>
<li id="nav-menu-item-13012-58bfaeff1bae3" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/contact/" class="menu-link main-menu-link item-title">Contact</a></li>
<li id="nav-menu-item-13022-58bfaeff1bc12" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/login" class="menu-link main-menu-link item-title">Login</a></li>
</ul>	<i class="carousel-nav prev dfd-icon-left_2"></i>
	<i class="carousel-nav next dfd-icon-right_2"></i>
</nav>
					</div>
				</div>
			</div>
		</div>
			</section>
	</div>	
	<div id="main-wrap" class="">

		<div id="change_wrap_div">

			

<section id="layout" class="no-title">


        	<div  class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="wpb_revslider_element wpb_content_element"><link href="http://fonts.googleapis.com/css?family=Montserrat%3A600" rel="stylesheet" property="stylesheet" type="text/css" media="all" /><link href="http://fonts.googleapis.com/css?family=Lora%3Aitalic" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:transparent;padding:0px;">
<!-- START REVOLUTION SLIDER  fullscreen mode -->
	<div id="rev_slider_3_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="">
<ul>	<!-- SLIDE  -->
	<li data-index="rs-6" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="/wp-content/uploads/2017/02/servisell-front-50x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="/wp-content/plugins/revslider/admin/assets/images/dummy.png"  alt="" title="servisell-front"  width="1920" height="1200" data-lazyload="/wp-content/uploads/2017/02/servisell-front.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption montserrat   tp-resizeme" 
			 id="slide-6-layer-1" 
			 data-x="['center','right','right','center']" data-hoffset="['0','797','230','0']" 
			 data-y="['middle','middle','middle','middle']" data-voffset="['0','-55','-368','-242']" 
						data-fontsize="['80','60','45','55']"
			data-lineheight="['75','65','45','55']"
			data-color="['rgba(255, 255, 255, 1.00)','rgba(35, 35, 35, 1.00)','rgba(35, 35, 35, 1.00)','rgba(255, 255, 255, 1.00)']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
			data-transform_idle="o:1;"
 
			 data-transform_in="x:-50px;opacity:0;s:700;e:Power3.easeInOut;" 
			 data-transform_out="x:-50px;opacity:0;s:700;" 
			data-start="650" 
			data-splitin="none" 
			data-splitout="none" 
			data-responsive_offset="on" 

			
			style="z-index: 5; white-space: nowrap; font-size: 80px; line-height: 75px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:montserrat;border-color:rgba(255, 255, 255, 1.00);">
SERVISELL </div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-2" 
			 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
			 data-y="['top','top','top','top']" data-voffset="['494','494','494','242']" 
						data-fontsize="['35','35','35','30']"
			data-lineheight="['49','49','49','22']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
			data-transform_idle="o:1;"
 
			 data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" 
			 data-transform_out="opacity:0;s:300;" 
			data-start="650" 
			data-splitin="none" 
			data-splitout="none" 
			data-responsive_offset="on" 

			
			style="z-index: 6; white-space: nowrap; font-size: 35px; line-height: 49px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Lora;font-style:italic;">The ultimate service conversion resource </div>
	</li>
	<!-- SLIDE  -->
	<li data-index="rs-7" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="/wp-content/uploads/2017/02/servisell-slide2-50x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="/wp-content/plugins/revslider/admin/assets/images/dummy.png"  alt="" title="servisell-slide2"  width="1920" height="1200" data-lazyload="/wp-content/uploads/2017/02/servisell-slide2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-1" 
			 data-x="['center','center','center','center']" data-hoffset="['-3','-3','-3','0']" 
			 data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','-175']" 
						data-fontsize="['35','35','35','30']"
			data-lineheight="['49','49','49','32']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
			data-transform_idle="o:1;"
 
			 data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" 
			 data-transform_out="opacity:0;s:300;" 
			data-start="500" 
			data-splitin="none" 
			data-splitout="none" 
			data-responsive_offset="on" 

			
			style="z-index: 5; white-space: nowrap; font-size: 35px; line-height: 49px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Lora;text-align:center;font-style:italic;">Convert 3% of your service traffic every day
 </div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-2" 
			 data-x="['center','center','center','center']" data-hoffset="['0','0','0','3']" 
			 data-y="['middle','middle','middle','middle']" data-voffset="['-96','-96','-96','-296']" 
						data-fontsize="['50','50','50','40']"
			data-lineheight="['55','55','55','40']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
			data-transform_idle="o:1;"
 
			 data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" 
			 data-transform_out="opacity:0;s:300;" 
			data-start="500" 
			data-splitin="none" 
			data-splitout="none" 
			data-responsive_offset="on" 

			
			style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 55px; font-weight: 600; color: rgba(255, 255, 255, 1.00);text-align:center;">TRANSFORM YOUR<br>SERVICE DRIVE <br>INTO A SALES GOLDMINE </div>
	</li>
</ul>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
						if(htmlDiv) {
							htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
						}else{
							var htmlDiv = document.createElement("div");
							htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
							document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
						}
					</script>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>
		<script type="text/javascript">
						/******************************************
				-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/

			var setREVStartSize=function(){
				try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
					e.c = jQuery('#rev_slider_3_1');
					e.responsiveLevels = [1240,1024,778,480];
					e.gridwidth = [1800,1240,778,620];
					e.gridheight = [900,750,900,900];
							
					e.sliderLayout = "fullscreen";
					e.fullScreenAutoWidth='off';
					e.fullScreenAlignForce='off';
					e.fullScreenOffsetContainer= '#haeder';
					e.fullScreenOffset='';
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};
			
			setREVStartSize();
			
						var tpj=jQuery;
			
			var revapi3;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_3_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_3_1");
				}else{
					revapi3 = tpj("#rev_slider_3_1").show().revolution({
						sliderType:"standard",
jsFileLocation:"/wp-content/plugins/revslider/public/assets/js/",
						sliderLayout:"fullscreen",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
 							mouseScrollReverse:"default",
							onHoverStop:"off",
							touch:{
								touchenabled:"on",
								swipe_threshold: 75,
								swipe_min_touches: 50,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							}
							,
							arrows: {
								style:"hades",
								enable:true,
								hide_onmobile:false,
								hide_onleave:false,
								tmp:'<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:0,
									v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:0,
									v_offset:0
								}
							}
						},
						responsiveLevels:[1240,1024,778,480],
						visibilityLevels:[1240,1024,778,480],
						gridwidth:[1800,1240,778,620],
						gridheight:[900,750,900,900],
						lazyType:"smart",
						parallax: {
							type:"mouse",
							origo:"slidercenter",
							speed:2000,
							levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
							type:"mouse",
						},
						shadow:0,
						spinner:"spinner3",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						fullScreenAutoWidth:"off",
						fullScreenAlignForce:"off",
						fullScreenOffsetContainer: "#haeder",
						fullScreenOffset: "",
						disableProgressBar:"on",
						hideThumbsOnMobile:"on",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:641,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
			});	/*ready*/
		</script>
		<script>
					var htmlDivCss = '	#rev_slider_3_1_wrapper .tp-loader.spinner3 div { background-color: #FFFFFF !important; } ';
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}
					else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
					</script>
					<script>
					var htmlDivCss = unescape(".hades.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.25%29%3B%0A%09width%3A100px%3B%0A%09height%3A100px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hades.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A30px%3B%0A%09color%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20100px%3B%0A%09text-align%3A%20center%3B%0A%20%20transition%3A%20background%200.3s%2C%20color%200.3s%3B%0A%7D%0A.hades.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hades.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A.hades.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20color%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%7D%0A.hades%20.tp-arr-allwrapper%20%7B%0A%20%20position%3Aabsolute%3B%0A%20%20left%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20background%3A%23888%3B%20%0A%20%20width%3A100px%3Bheight%3A100px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-ms-filter%3A%20%22progid%3Adximagetransform.microsoft.alpha%28opacity%3D0%29%22%3B%0A%20%20filter%3A%20alpha%28opacity%3D0%29%3B%0A%20%20-moz-opacity%3A%200.0%3B%0A%20%20-khtml-opacity%3A%200.0%3B%0A%20%20opacity%3A%200.0%3B%0A%20%20-webkit-transform%3A%20rotatey%28-90deg%29%3B%0A%20%20transform%3A%20rotatey%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%200%25%2050%25%3B%0A%20%20transform-origin%3A%200%25%2050%25%3B%0A%7D%0A.hades.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20left%3Aauto%3B%0A%20%20%20right%3A100%25%3B%0A%20%20%20-webkit-transform-origin%3A%20100%25%2050%25%3B%0A%20%20transform-origin%3A%20100%25%2050%25%3B%0A%20%20%20-webkit-transform%3A%20rotatey%2890deg%29%3B%0A%20%20transform%3A%20rotatey%2890deg%29%3B%0A%7D%0A%0A.hades%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20-ms-filter%3A%20%22progid%3Adximagetransform.microsoft.alpha%28opacity%3D100%29%22%3B%0A%20%20filter%3A%20alpha%28opacity%3D100%29%3B%0A%20%20-moz-opacity%3A%201%3B%0A%20%20-khtml-opacity%3A%201%3B%0A%20%20opacity%3A%201%3B%20%20%0A%20%20%20%20-webkit-transform%3A%20rotatey%280deg%29%3B%0A%20%20transform%3A%20rotatey%280deg%29%3B%0A%0A%20%7D%0A%20%20%20%20%0A.hades%20.tp-arr-iwrapper%20%7B%0A%7D%0A.hades%20.tp-arr-imgholder%20%7B%0A%20%20background-size%3Acover%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3Bleft%3A0px%3B%0A%20%20width%3A100%25%3Bheight%3A100%25%3B%0A%7D%0A.hades%20.tp-arr-titleholder%20%7B%0A%7D%0A.hades%20.tp-arr-subtitleholder%20%7B%0A%7D%0A%0A");
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}
					else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
				  </script>
				</div><!-- END REVOLUTION SLIDER --></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row dfd-background-dark equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488688397650" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaeffb28a2" data-id="58bfaeffb28a2" data-height="30" data-height-mobile="20" data-height-tab="20" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="859" height="74" src="/wp-content/uploads/2015/07/700-Credit-Logo-2016-final-RGB.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/700-Credit-Logo-2016-final-RGB.png 859w, /wp-content/uploads/2015/07/700-Credit-Logo-2016-final-RGB-300x26.png 300w, /wp-content/uploads/2015/07/700-Credit-Logo-2016-final-RGB-768x66.png 768w" sizes="(max-width: 859px) 100vw, 859px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfaeffbfb6a" data-id="58bfaeffbfb6a" data-height="30" data-height-mobile="20" data-height-tab="20" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly dfd-row-full-height dfd-row-content-middle vc_custom_1487815840985" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="six dfd_col-tabletop-6 dfd_col-laptop-6 dfd_col-mobile-12 columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaeffc7a5a" data-id="58bfaeffc7a5a" data-height="0" data-height-mobile="60" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfaeffc7b80" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">The magic of<br />
instant pre-screen</h3></div><div class="ult-spacer spacer-58bfaeffc7c5c" data-id="58bfaeffc7c5c" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><span style="color: #595959;"><strong>SERVISELL combines powerful Instant Pre-Screen credit data with NADA Vehicle Data to instantly display a Trade Probability for every service customer coming through your doors.</strong></span></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfaeffd3e4f" data-id="58bfaeffd3e4f" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div class="dfd-icon-list-wrap  del-width-net-icon style-4  " ><ul class="dfd-icon-list"><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#dd3333; "><i class = "dfd-icon-ban"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; ">No Date of Birth needed</div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#dd3333; "><i class = "dfd-icon-ban"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; ">No Social Security Number needed</div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#dd3333; "><i class = "dfd-icon-ban"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; ">No Adverse Affect on customer credit</div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li></ul></div><div class="ult-spacer spacer-58bfaeffd4846" data-id="58bfaeffd4846" data-height="0" data-height-mobile="60" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaeffd7ee2" data-id="58bfaeffd7ee2" data-height="0" data-height-mobile="0" data-height-tab="0" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="1400" height="1232" src="/wp-content/uploads/2015/07/servisell-step-2.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/servisell-step-2.png 1400w, /wp-content/uploads/2015/07/servisell-step-2-300x264.png 300w, /wp-content/uploads/2015/07/servisell-step-2-768x676.png 768w, /wp-content/uploads/2015/07/servisell-step-2-1024x901.png 1024w" sizes="(max-width: 1400px) 100vw, 1400px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfaeffdcead" data-id="58bfaeffdcead" data-height="25" data-height-mobile="15" data-height-tab="15" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class=" vc-row-delimiter-6"><div class="vc-row-delimiter-top"><div class="vc-row-delimiter-top-left"></div><div class="vc-row-delimiter-top-right"></div></div></div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1487899642502" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns twelve"><div class="wpb_wrapper"><div id="ultimate-heading58bfaf000d1fd" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;color:#ffffff;letter-spacing:-1px;">Did you know?</h3></div><div id="ultimate-heading58bfaf000d367" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-size:28px;color:#ffffff;line-height:49px;">Your Service Drive will generate 8 to 10 times more customer traffic than your Showroom</h2></div></div></div></div></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-wrap dfd-row-bg-image dfd_vertical_parallax" id="dfd-image-bg-58bfaf001076c"  data-parallax_offset="" data-parallax_sense="30" data-mobile_enable="1"></div><script type="text/javascript">
					(function($) {
						$("head").append("<style>#dfd-image-bg-58bfaf001076c {background-image: url(/wp-content/uploads/2017/02/main-slider-2.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style>");
					})(jQuery);
				</script><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div id="how-it-works" class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488854448663" data-parallax_sense="30" data-dfd-dots-title="How It Works"><div class="row" >
	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf0018dab" data-id="58bfaf0018dab" data-height="60" data-height-mobile="50" data-height-tab="50" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bottom-to-top " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="1000" height="1044" src="/wp-content/uploads/2015/07/servisell-smaller-trade-algorithm-example.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/servisell-smaller-trade-algorithm-example.png 1000w, /wp-content/uploads/2015/07/servisell-smaller-trade-algorithm-example-287x300.png 287w, /wp-content/uploads/2015/07/servisell-smaller-trade-algorithm-example-768x802.png 768w, /wp-content/uploads/2015/07/servisell-smaller-trade-algorithm-example-981x1024.png 981w" sizes="(max-width: 1000px) 100vw, 1000px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfaf001df70" data-id="58bfaf001df70" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf0020ce4" data-id="58bfaf0020ce4" data-height="0" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfaf0020e0f" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">The Servisell<br />
Trade Algorithm </h3></div><div class="ult-spacer spacer-58bfaf0020ee3" data-id="58bfaf0020ee3" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p class="m_-6369583052526920779p1"><span style="color: #595959;"><strong><span class="m_-6369583052526920779s1">By combining very specific customer and service vehicle data, the SERVISELL Trade Algorithm instantly calculates a Trade Probably Percentage on each service customer.</span></strong></span></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfaf0024a77" data-id="58bfaf0024a77" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class=" vc-row-delimiter-6"><div class="vc-row-delimiter-top"><div class="vc-row-delimiter-top-left"></div><div class="vc-row-delimiter-top-right"></div></div></div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf002bc7a" data-id="58bfaf002bc7a" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div><div id="ultimate-heading58bfaf002bdc3" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">INSTANT CONTACT</h3></div><div class="ult-spacer spacer-58bfaf002becf" data-id="58bfaf002becf" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><strong><span style="color: #595959;">Within minutes of entering the service drive, your sales department will be directed to the service customers with the highest Trade Probability, while they are still in the store.</span></strong></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfaf002f096" data-id="58bfaf002f096" data-height="0" data-height-mobile="30" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf0032aec" data-id="58bfaf0032aec" data-height="60" data-height-mobile="0" data-height-tab="40" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="800" height="965" src="/wp-content/uploads/2015/07/servisell-clipboard-trade-alert.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/servisell-clipboard-trade-alert.png 800w, /wp-content/uploads/2015/07/servisell-clipboard-trade-alert-249x300.png 249w, /wp-content/uploads/2015/07/servisell-clipboard-trade-alert-768x926.png 768w" sizes="(max-width: 800px) 100vw, 800px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfaf0036b40" data-id="58bfaf0036b40" data-height="25" data-height-mobile="15" data-height-tab="15" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class=" vc-row-delimiter-6"><div class="vc-row-delimiter-top"><div class="vc-row-delimiter-top-left"></div><div class="vc-row-delimiter-top-right"></div></div></div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488855003554" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf003e487" data-id="58bfaf003e487" data-height="100" data-height-mobile="60" data-height-tab="0" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper vc_box_rounded  vc_box_border_grey " ><img width="300" height="300" src="/wp-content/uploads/2017/03/background-servisell-300x300.jpg" class="vc_single_image-img attachment-medium" alt="" srcset="/wp-content/uploads/2017/03/background-servisell-300x300.jpg 300w, /wp-content/uploads/2017/03/background-servisell-150x150.jpg 150w, /wp-content/uploads/2017/03/background-servisell-768x768.jpg 768w, /wp-content/uploads/2017/03/background-servisell.jpg 994w" sizes="(max-width: 300px) 100vw, 300px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfaf004242b" data-id="58bfaf004242b" data-height="100" data-height-mobile="40" data-height-tab="15" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="six dfd_col-tabletop-6 dfd_col-laptop-6 dfd_col-mobile-12 columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf0044769" data-id="58bfaf0044769" data-height="0" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfaf00448a0" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">INCREASED<br />
MONTHLY SALES</h3></div><div class="ult-spacer spacer-58bfaf00449a3" data-id="58bfaf00449a3" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><strong><span style="color: #595959;">By converting only ONE IN TEN service customers that show a high Trade Probability, you&#8217;ll increase your monthly unit sales number by half of your average daily R.O. count.</span></strong></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfaf004735c" data-id="58bfaf004735c" data-height="0" data-height-mobile="60" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div id="benefits" class="vc-row-wrapper wpb_row dfd-background-dark mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1487803367276" data-parallax_sense="30" data-dfd-dots-title="Benefits"><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf004ded8" data-id="58bfaf004ded8" data-height="120" data-height-mobile="60" data-height-tab="80" style="clear:both;display:block;"></div><div id="ultimate-heading58bfaf004e003" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">BENEFITS OF SERVISELL</h3></div><div id="ultimate-heading58bfaf004e156" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-size:28px;color:#ffffff;line-height:49px;">The ultimate service conversion resource</h2></div><div class="ult-spacer spacer-58bfaf004e217" data-id="58bfaf004e217" data-height="60" data-height-mobile="35" data-height-tab="45" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row full_width_content_paddings dfd-background-dark mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1487790156928" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="four dfd_col-tabletop-4 dfd_col-laptop-4 dfd_col-mobile-12 dfd_col-tablet-12 columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf0055b6a" data-id="58bfaf0055b6a" data-height="90" data-height-mobile="40" data-height-tab="50" style="clear:both;display:block;"></div><div id="info-box-58bfaf0055c98" class="aio-icon-component   style_1"><div class="aio-icon-box right-icon  cr-animate-gen" data-animate-type = "transition.slideDownBigIn"  style=""><div class="aio-ibd-block"><div class="aio-icon-header"><div class="feature-title" style="font-family:Montserrat;font-weight:600;font-size:28px;line-height:24px;letter-spacing:-1px;">SERVICE CONVERSION</div></div><!-- header --><div class="aio-icon-description" style=""><p>Convert 3% or more of your Service Traffic EVERY DAY</p>
<div class="read-more-wrap"><a class=" dfd-dotted-link" href="#" ><span class="dfd-left-line"></span><span></span><span class="dfd-right-line"></span></a></div></div> <!-- description --></div> <!-- aio-ibd-block --><div class="aio-icon-right"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-pie_chart_1"></i>
</div></div></div></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --><div id="info-box-58bfaf00560e1" class="aio-icon-component   style_1"><div class="aio-icon-box right-icon  cr-animate-gen" data-animate-type = "transition.slideDownBigIn"  style=""><div class="aio-ibd-block"><div class="aio-icon-header"><div class="feature-title" style="font-family:Montserrat;font-weight:600;font-size:28px;line-height:24px;letter-spacing:-1px;">TAKE IN TRADES</div></div><!-- header --><div class="aio-icon-description" style=""><p>Vastly improve the volume and quality of trades taken in</p>
<div class="read-more-wrap"><a class=" dfd-dotted-link" href="#" ><span class="dfd-left-line"></span><span></span><span class="dfd-right-line"></span></a></div></div> <!-- description --></div> <!-- aio-ibd-block --><div class="aio-icon-right"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-graph_growth"></i>
</div></div></div></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --><div class="ult-spacer spacer-58bfaf00564b6" data-id="58bfaf00564b6" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="four dfd_col-tabletop-4 dfd_col-laptop-4 dfd_col-mobile-12 dfd_col-tablet-12 columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
	<div class="wpb_single_image wpb_content_element vc_align_center " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="1000" height="884" src="/wp-content/uploads/2017/02/servicell-desktop.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2017/02/servicell-desktop.png 1000w, /wp-content/uploads/2017/02/servicell-desktop-300x265.png 300w, /wp-content/uploads/2017/02/servicell-desktop-768x679.png 768w" sizes="(max-width: 1000px) 100vw, 1000px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfaf005dd05" data-id="58bfaf005dd05" data-height="60" data-height-mobile="20" data-height-tab="40" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="four dfd_col-tabletop-4 dfd_col-laptop-4 dfd_col-mobile-12 dfd_col-tablet-12 columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfaf0061c1b" data-id="58bfaf0061c1b" data-height="90" data-height-mobile="40" data-height-tab="50" style="clear:both;display:block;"></div><div id="info-box-58bfaf0061d5d" class="aio-icon-component   style_1"><div class="aio-icon-box left-icon  cr-animate-gen" data-animate-type = "transition.slideDownBigIn"  style=""><div class="aio-icon-left"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-computer_phone"></i>
</div></div></div><div class="aio-ibd-block"><div class="aio-icon-header"><div class="feature-title" style="font-family:Montserrat;font-weight:600;font-size:28px;line-height:24px;letter-spacing:-1px;">CSI</div></div><!-- header --><div class="aio-icon-description" style=""><p>Improve CSI through providing logical and timely vehicle ownership solutions to your Service Customers.</p>
<div class="read-more-wrap"><a class=" dfd-dotted-link" href="#" ><span class="dfd-left-line"></span><span></span><span class="dfd-right-line"></span></a></div></div> <!-- description --></div> <!-- aio-ibd-block --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --><div id="info-box-58bfaf00621f5" class="aio-icon-component   style_1"><div class="aio-icon-box left-icon  cr-animate-gen" data-animate-type = "transition.slideDownBigIn"  style=""><div class="aio-icon-left"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-upload_3"></i>
</div></div></div><div class="aio-ibd-block"><div class="aio-icon-header"><div class="feature-title" style="font-family:Montserrat;font-weight:600;font-size:28px;line-height:24px;letter-spacing:-1px;">BOTTOM LINE</div></div><!-- header --><div class="aio-icon-description" style=""><p>Increase your bottom line</p>
<div class="read-more-wrap"><a class=" dfd-dotted-link" href="#" ><span class="dfd-left-line"></span><span></span><span class="dfd-right-line"></span></a></div></div> <!-- description --></div> <!-- aio-ibd-block --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component -->
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div id="#signup" class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly dfd-row-full-height dfd-row-content-middle vc_custom_1487809445475" data-parallax_sense="30" data-dfd-dots-title="Demo"><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns twelve"><div class="wpb_wrapper"><div id="ultimate-heading58bfaf0071787" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;color:#ffffff;letter-spacing:-1px;">Go on, Try It.</h3></div><div id="ultimate-heading58bfaf00719c9" class="uvc-heading dfd-delim-top   cr-animate-gen heading-config-top dfd-disable-resposive-headings" data-animate-type = "transition.slideRightBigIn"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-size:28px;color:#ffffff;line-height:49px;">Transform your Service Drive into a Sales Goldmine</h2></div></div></div></div></div><div class="ubtn-ctn-center"><a class="ubtn-link ubtn-center tooltip-58bfaf0071b7b"  href = "/test-drive/" target="_self"><span class=" ubtn ubtn-custom ubtn-fade-bg    ubtn-sep-icon ubtn-sep-icon-at-left  ubtn-center  "  data-hover="#003a63" data-border-color="" data-hover-bg="#ffffff" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="width:px;min-height:px;line-height:px;padding:15px 80px;border:none;background: #003a63;color: #ffffff;"><span class="ubtn-data ubtn-icon"><i class="icomoon-dfd_twenty_fifth_socicon_android" style="font-size:21px;color:#4f4a43;"></i></span><span class="ubtn-hover"></span><span class="ubtn-data ubtn-text" data-lang="en">LEARN MORE</span></span></a></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-wrap dfd-row-bg-image dfd_vertical_parallax" id="dfd-image-bg-58bfaf00755fb"  data-parallax_offset="" data-parallax_sense="30" data-mobile_enable="1"></div><script type="text/javascript">
					(function($) {
						$("head").append("<style>#dfd-image-bg-58bfaf00755fb {background-image: url(/wp-content/uploads/2015/07/servisell-section-2.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style>");
					})(jQuery);
				</script><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div>
	

</section>			
		</div>
		
		<div class="body-back-to-top align-right"><i class="dfd-added-font-icon-right-open"></i></div>

		


<div id="footer-wrap">
	
	<section id="footer" class="footer-style-2 dfd-background-dark">

			<div class="row">
					<div class="three columns">
				<section id="crum_contacts_widget-17" class="widget widget_crum_contacts_widget"><h3 class="widget-title">About company</h3>SERVISELL is dedicated to developing intuitive and highly effective sales solutions for our dealer clients nationwide. The success of our client is paramount. SERVISELL strives to provide the cutting-edge sales systems needed to give our dealers the advantage in today’s highly competitive marketplace.</section>			</div>
					<div class="three columns">
				<section id="dfd_vcard_simple-12" class="widget widget_dfd_vcard_simple"><h3 class="widget-title">Contact us</h3>
		<div id="dfd-vcard-widget-58bfaf007ccec" class="row">
			<div class="twelve columns">
				<div class="dfd-vcard-wrap">
											<div class="vcard-field">
							<i class="dfd-icon-tablet2"></i>
														<p><font color="#ffffff">877.473.4280</font></p>
						</div>
																<div class="vcard-field">
							<i class="dfd-icon-email_1"></i>
														<p>
															<a href="mailto:info@servisell.com" title="" >info@servisell.com</a>
													</div>
																<div class="vcard-field">
							<i class="dfd-icon-navigation"></i>
														<p>1001 Woodward Ave.<br />5th Floor<br />Detroit, MI 48226</p>
						</div>
														</div>
			</div>
		</div>
				<script type="text/javascript">
			(function($) {
				$('head').append('<style>#dfd-vcard-widget-58bfaf007ccec .dfd-vcard-wrap {background: #003a63;}#dfd-vcard-widget-58bfaf007ccec .dfd-vcard-wrap {border-style: none;}#dfd-vcard-widget-58bfaf007ccec .dfd-vcard-wrap {-webkit-border-radius: 2px;-moz-border-radius: 2px;-o-border-radius: 2px;border-radius: 2px;}</style>');
			})(jQuery);
		</script>
				
		</section>			</div>
					<div class="three columns">
				<section id="text-32" class="widget widget_text"><h3 class="widget-title">Quick Links</h3>			<div class="textwidget"><a href="/login">Client Login</a><br></br>

<a href="/contact/">Email Servisell</a><br></br>

<a href="/privacy-policy-terms-use/">Privacy Policy</a>
</div>
		</section>			</div>
					<div class="three columns">
				<section id="text-33" class="widget widget_text"><h3 class="widget-title">Servisell powered by:</h3>			<div class="textwidget"><img src="/images/CDKGlobalLogo_color.png" alt="CDK Global logo" style="width:100px;height:80px;">
<br>
<img src="/wp-content/uploads/2017/02/NADA-Values_rgb-Jan-2017.png" alt="NADA" style="width:100px;height:72px;">



</div>
		</section>			</div>
			</div>

	</section>

			<section id="sub-footer" class=" dfd-background-dark">
			<div class="row">
				<div class="twelve columns subfooter-copyright text-center">
					© copyright 2017 Servisell				</div>
			</div>
		</section>
	
</div>



</div>

<div id="sidr">
	<div class="sidr-top">
						<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
			</div>
	<div class="sidr-inner"></div>
		</div>
<a href="#sidr-close" class="dl-trigger dfd-sidr-close"></a>

		<script type="text/javascript">
			function revslider_showDoubleJqueryError(sliderID) {
				var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
				errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
				errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
				errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
				errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
					jQuery(sliderID).show().html(errorMessage);
			}
		</script>
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2Cregular%2C500%2C600%2C700%2C800%2C900">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat%3A600">
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.form.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-js/ultimate.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/plugins.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/hammer.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.smoothscroll.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajax_var = {"url":"\/wp-admin\/admin-ajax.php","nonce":"ac471e7152"};
/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/post-like.min.js'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.run.js'></script>
<script type='text/javascript' src='/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js'></script>
</body>
</html>
