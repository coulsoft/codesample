<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js ie lt-ie10" lang="en-US"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-ie" lang="en-US"> <!--<![endif]-->

<head>

    <meta charset="utf-8">

    <title>Servisell | Test Drive</title>

			<link rel="icon" type="image/png" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png" />
				<link rel="apple-touch-icon" href="/wp-content/uploads/2017/02/servisell-logo-web-16-02.png">
				<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/2017/02/servisell-logo-web-76-02.png">
				<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/2017/02/servisell-logo-web-120-02.png">
		
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!--[if lte IE 9]>
        <script src="/wp-content/themes/ronneby/assets/js/html5shiv.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
        <script src="/wp-content/themes/ronneby/assets/js/excanvas.compiled.js"></script>
    <![endif]-->

				<style type="text/css">
				body {
					
					
											background-repeat: repeat !important;
					
									}
			</style>
		
    <meta name='robots' content='noindex,follow' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Feed" href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Servisell &raquo; Comments Feed" href="/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" href="/wp-content/uploads//dfd_ronneby_fonts/ronneby/Defaults.css">
<link rel="stylesheet" href="/wp-content/plugins/revslider/public/assets/css/settings.css">
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/styled-button.css">
<link rel="stylesheet" href="/wp-content/uploads/smile_fonts/ronneby/Defaults.css">
<link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.min.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-css/ultimate.min.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/visual-composer.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/app.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/mobile-responsive.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/jquery.isotope.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/assets/css/prettyPhoto.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby_child/style.css">
<link rel="stylesheet" href="/wp-content/themes/ronneby/css/options.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%7CDroid+Serif%3A400%2C700%2C400italic%2C700italic%7CLora%3A400%2C700%2C400italic%2C700italic%7CMontserrat%3A400%2C700&#038;subset=latin">
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
<link rel='https://api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.3" />
<link rel="canonical" href="/test-drive/" />
<link rel='shortlink' href='/?p=13151' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=%2Ftest-drive%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=%2Ftest-drive%2F&#038;format=xml" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		
<style type="text/css">
.a-stats {
	width: auto;
}
.a-stats a {
	background: #7CA821;
	background-image:-moz-linear-gradient(0% 100% 90deg,#5F8E14,#7CA821);
	background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#7CA821),to(#5F8E14));
	border: 1px solid #5F8E14;
	border-radius:3px;
	color: #CFEA93;
	cursor: pointer;
	display: block;
	font-weight: normal;
	height: 100%;
	-moz-border-radius:3px;
	padding: 7px 0 8px;
	text-align: center;
	text-decoration: none;
	-webkit-border-radius:3px;
	width: 100%;
}
.a-stats a:hover {
	text-decoration: none;
	background-image:-moz-linear-gradient(0% 100% 90deg,#6F9C1B,#659417);
	background-image:-webkit-gradient(linear,0% 0,0% 100%,from(#659417),to(#6F9C1B));
}
.a-stats .count {
	color: #FFF;
	display: block;
	font-size: 15px;
	line-height: 16px;
	padding: 0 13px;
	white-space: nowrap;
}
</style>

<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution  - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">jQuery(document).ready(function(){
					jQuery(".ult_modal-body iframe").each(function(index, element) {
						var w = jQuery(this).attr("width");
						var h = jQuery(this).attr("height");
						var st = '<style type="text/css" id="modal-css">';
							st += "#"+jQuery(this).closest(".ult-overlay").attr("id")+" iframe{width:"+w+"px !important;height:"+h+"px !important;}";
							st += ".fluid-width-video-wrapper{padding: 0 !important;}";
							st += "</style>";
						jQuery("head").append(st);
					});
				});</script><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1488821596698{background-color: #003a63 !important;}.vc_custom_1488835027460{background-color: #e9e9e9 !important;}.vc_custom_1488851482727{background-color: #e9e9e9 !important;}.vc_custom_1488484853790{padding-top: 150px !important;padding-bottom: 150px !important;}.vc_custom_1488837676148{background-color: #003a63 !important;}.vc_custom_1488838091649{background-color: #e9e9e9 !important;}.vc_custom_1488850259873{background-color: #003a63 !important;}.vc_custom_1488838091649{background-color: #e9e9e9 !important;}.vc_custom_1488757266723{padding-top: 150px !important;padding-bottom: 150px !important;}.vc_custom_1488852523441{padding-right: 15px !important;padding-left: 15px !important;}.vc_custom_1488852639700{padding-right: 15px !important;padding-left: 15px !important;}.vc_custom_1488852794310{padding-right: 15px !important;padding-left: 15px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	
	
</head>
<body class="page-template page-template-tmp-one-page-scroll page-template-tmp-one-page-scroll-php page page-id-13151 test-drive wpb-js-composer js-comp-ver-4.12 vc_responsive" data-directory="/wp-content/themes/ronneby" data-header-responsive-width="1101">
		

	<div class="form-search-section" style="display: none;">
	<div class="row">
		<form role="search" method="get" id="searchform_58bfb42bb9f44" class="form-search" action="/">
	<i class="dfdicon-header-search-icon inside-search-icon"></i>
	<input type="text" value="" name="s" id="s_58bfb42bb9efc" class="search-query" placeholder="Search on site...">
	<input type="submit" value="Search" class="btn">
	<i class="header-search-switcher close-search"></i>
	</form>	</div>
</div><div id="header-container" class="header-style-7 header-layout-fullwidth sticky-header-enabled simple  without-top-panel logo-position-left without-top-panel dfd-header-layout-fixed text-left default dfd-enable-headroom">
	<div class="dfd-top-row dfd-header-responsive-hide">
		<div class="row">
			<div class="twelve columns">
													<a href="/" title="Home" class="fixed-header-logo">
						<img src="/wp-content/uploads/2017/02/servisell-logo-web.png" alt="logo"/>
					</a>
								<a href="#" title="menu" class="dfd-menu-button">
					<span class="icon-wrap dfd-top-line"></span>
					<span class="icon-wrap dfd-middle-line"></span>
					<span class="icon-wrap dfd-bottom-line"></span>
				</a>
			</div>
		</div>
			</div>
	<section id="header">
		<div class="header-wrap">
			<div class="row decorated">
				<div class="columns twelve header-main-panel">
					<div class="header-col-left">
						<div class="mobile-logo">
																<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
													</div>
												<div class="dfd-header-top dfd-header-responsive-hide header-info-panel">
								<div class="top-info"><i class="dfd-icon-phone"></i><span class="dfd-top-info-delim-blank"></span>877.473.4280<span class="dfd-top-info-delim"></span><i class="dfd-icon-email_2"></i><span class="dfd-top-info-delim-blank"></span>contact@servisell.com</div>
						</div>
					</div>
					<div class="header-col-right text-center clearfix">
						<div class="header-icons-wrapper">
							<div class="dl-menuwrapper">
	<a href="#sidr" class="dl-trigger icon-mobile-menu dfd-vertical-aligned" id="mobile-menu">
		<span class="icon-wrap dfd-middle-line"></span>
		<span class="icon-wrap dfd-top-line"></span>
		<span class="icon-wrap dfd-bottom-line"></span>
	</a>
</div>								<div class="form-search-wrap">
		<a href="#" class="header-search-switcher dfd-icon-zoom"></a>
	</div>
																				</div>
					</div>
					<div class="header-col-fluid">
						<nav class="mega-menu clearfix" id="main_mega_menu">
							<ul id="menu-twenty_fifth_onepage" class="nav-menu menu-primary-navigation menu-clonable-for-mobiles"><li id="nav-menu-item-13014-58bfb42bd02f3" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/" class="menu-link main-menu-link item-title">Home</a></li>
<li id="nav-menu-item-13212-58bfb42bd078a" class="mega-menu-item nav-item menu-item-depth-0 current-menu-item "><a href="/test-drive/" class="menu-link main-menu-link item-title">Test Drive</a></li>
<li id="nav-menu-item-13012-58bfb42bd0e26" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/contact/" class="menu-link main-menu-link item-title">Contact</a></li>
<li id="nav-menu-item-13022-58bfb42bd0fee" class="mega-menu-item nav-item menu-item-depth-0 "><a href="/login" class="menu-link main-menu-link item-title">Login</a></li>
</ul>							<i class="carousel-nav prev dfd-icon-left_2"></i>
							<i class="carousel-nav next dfd-icon-right_2"></i>
						</nav>
					</div>
											<div class="dfd-header-bottom dfd-header-responsive-hide">
							<div class="login-button-wrap">
																	<div class="login-header">
			<div class="links">
			<a href="/wp-login.php?redirect_to=%2Ftest-drive%2F" class="drop-login">
				<i class="dfd-icon-lock"></i>
				<span>Login on site</span>
			</a>
		</div>
	</div>															</div>
							<div class="clear"></div>
							<div class="inline-block">
																	<div class="form-search-wrap">
		<a href="#" class="header-search-switcher dfd-icon-zoom"></a>
	</div>
															</div>
							<div class="clear"></div>
															<div class="copyright-soc-icons-wrap">
																			<div class="dfd-copyright">
											<a href="http://rnbtheme.com" title="DFD">© DynamicFrameworks</a>- Elite ThemeForest Author.										</div>
																												<div class="widget soc-icons dfd-soc-icons-hover-style-26">
											<a href="http://dribbble.com" class="dr soc_icon-dribbble" title="Dribbble" target="_blank"><span class="line-top-left soc_icon-dribbble"></span><span class="line-top-center soc_icon-dribbble"></span><span class="line-top-right soc_icon-dribbble"></span><span class="line-bottom-left soc_icon-dribbble"></span><span class="line-bottom-center soc_icon-dribbble"></span><span class="line-bottom-right soc_icon-dribbble"></span><i class="soc_icon-dribbble"></i></a><a href="http://facebook.com" class="fb soc_icon-facebook" title="Facebook" target="_blank"><span class="line-top-left soc_icon-facebook"></span><span class="line-top-center soc_icon-facebook"></span><span class="line-top-right soc_icon-facebook"></span><span class="line-bottom-left soc_icon-facebook"></span><span class="line-bottom-center soc_icon-facebook"></span><span class="line-bottom-right soc_icon-facebook"></span><i class="soc_icon-facebook"></i></a><a href="http://twitter.com" class="tw soc_icon-twitter-3" title="Twitter" target="_blank"><span class="line-top-left soc_icon-twitter-3"></span><span class="line-top-center soc_icon-twitter-3"></span><span class="line-top-right soc_icon-twitter-3"></span><span class="line-bottom-left soc_icon-twitter-3"></span><span class="line-bottom-center soc_icon-twitter-3"></span><span class="line-bottom-right soc_icon-twitter-3"></span><i class="soc_icon-twitter-3"></i></a><a href="https://vimeo.com/" class="vi soc_icon-vimeo" title="Vimeo" target="_blank"><span class="line-top-left soc_icon-vimeo"></span><span class="line-top-center soc_icon-vimeo"></span><span class="line-top-right soc_icon-vimeo"></span><span class="line-bottom-left soc_icon-vimeo"></span><span class="line-bottom-center soc_icon-vimeo"></span><span class="line-bottom-right soc_icon-vimeo"></span><i class="soc_icon-vimeo"></i></a>										</div>
																	</div>
													</div>
									</div>
			</div>
		</div>
	</section>
</div>	
	<div id="main-wrap" class="">

		<div id="change_wrap_div">

			

<section id="layout" class="no-title one-page-scroll  dfd-enable-onepage-animation dfd-3d-style-1" data-enable-dots="false" data-enable-animation="true">


		<div  class="vc-row-wrapper wpb_row dfd-background-dark equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c45bec" data-id="58bfb42c45bec" data-height="40" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42c45dab" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:70px;line-height:71px;letter-spacing:-1px;">This is Servisell</h3></div><div class="ult-spacer spacer-58bfb42c45ef3" data-id="58bfb42c45ef3" data-height="20" data-height-mobile="20" data-height-tab="20" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42c46031" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-style:italic;font-weight:400;font-size:35px;color:#ffffff;line-height:49px;">The ultimate service conversion resource</h2></div><div class="ult-spacer spacer-58bfb42c46180" data-id="58bfb42c46180" data-height="45" data-height-mobile="20" data-height-tab="40" style="clear:both;display:block;"></div><div id="info-box-58bfb42c462c1" class="aio-icon-component   style_1"><div class="aio-icon-box top-icon "  style=""><div class="aio-icon-top"><div class="align-icon" style="text-align: center;">
<div class="aio-icon none  " style="color:#ffffff;font-size:32px;display:inline-block !important;" >
	<i class="dfd-icon-up_down_1"></i>
</div></div></div><div class="aio-icon-description" style="color:#595959;"><p style="text-align: center;">Use the up and down arrows on your keyboard or scroll down to navigate the presentation</p>
</div> <!-- description --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --><div class="ult-spacer spacer-58bfb42c467a7" data-id="58bfb42c467a7" data-height="0" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-wrap dfd-row-bg-image dfd_simple_image" id="dfd-image-bg-58bfb42c486b8"  data-mobile_enable="1"></div><script type="text/javascript">
					(function($) {
						$("head").append("<style>#dfd-image-bg-58bfb42c486b8 {background-position: ;background-image: url(/wp-content/uploads/2017/03/servisell-test-drive-1.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style>");
					})(jQuery);
				</script><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row dfd-background-dark mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488821596698" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c4d8b9" data-id="58bfb42c4d8b9" data-height="60" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42c4d9ee" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">What can SERVISELL do for me?</h3></div><div class="ult-spacer spacer-58bfb42c4dace" data-id="58bfb42c4dace" data-height="35" data-height-mobile="40" data-height-tab="40" style="clear:both;display:block;"></div><div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns four"><div class="wpb_wrapper"><div class="dfd-heading-shortcode"><div class="dfd-heading-module-wrap  text-center style_05 dfd-disable-resposive-headings" id="dfd-heading-58bfb42c5dfa4" ><div class="inline-block"><div class="dfd-heading-module"><h1 class="widget-title " style="font-size: 60px; color: #2f5e7f; ">SERVICE</h1><div class="dfd-heading-delimiter"></div><h3 class="widget-sub-title " style="font-family:Raleway; font-weight:600; font-style:normal; font-size: 20px; color: #ffffff; line-height: 25px; ">Take your Daily Average R.O. Count</h3></div></div></div><script type="text/javascript">
									(function($) {
										$("head").append("<style>#dfd-heading-58bfb42c5dfa4 .dfd-heading-delimiter {border-bottom-style:solid;border-bottom-width:1px;width:200px;border-bottom-color:#2f5e7f;}#dfd-heading-58bfb42c5dfa4 .dfd-heading-delimiter {margin-top:15px;margin-bottom:15px;}</style>");
									})(jQuery);
								</script></div><div class="ult-spacer spacer-58bfb42c60adc" data-id="58bfb42c60adc" data-height="0" data-height-mobile="45" data-height-tab="40" style="clear:both;display:block;"></div></div></div><div class="columns four"><div class="wpb_wrapper"><div class="dfd-heading-shortcode"><div class="dfd-heading-module-wrap  text-center style_05 dfd-disable-resposive-headings" id="dfd-heading-58bfb42c625c9" ><div class="inline-block"><div class="dfd-heading-module"><h1 class="widget-title " style="font-size: 60px; color: #2f5e7f; ">=</h1><div class="dfd-heading-delimiter"></div><h3 class="widget-sub-title " style="font-family:Raleway; font-weight:600; font-style:normal; font-size: 20px; color: #ffffff; line-height: 25px; ">Divide by Two</h3></div></div></div><script type="text/javascript">
									(function($) {
										$("head").append("<style>#dfd-heading-58bfb42c625c9 .dfd-heading-delimiter {border-bottom-style:solid;border-bottom-width:1px;width:200px;border-bottom-color:#2f5e7f;}#dfd-heading-58bfb42c625c9 .dfd-heading-delimiter {margin-top:15px;margin-bottom:15px;}</style>");
									})(jQuery);
								</script></div><div class="ult-spacer spacer-58bfb42c657eb" data-id="58bfb42c657eb" data-height="0" data-height-mobile="55" data-height-tab="45" style="clear:both;display:block;"></div></div></div><div class="columns four"><div class="wpb_wrapper"><div class="dfd-heading-shortcode"><div class="dfd-heading-module-wrap  text-center style_05 dfd-disable-resposive-headings" id="dfd-heading-58bfb42c674d3" ><div class="inline-block"><div class="dfd-heading-module"><h1 class="widget-title " style="font-size: 60px; color: #2f5e7f; ">SALES</h1><div class="dfd-heading-delimiter"></div><h3 class="widget-sub-title " style="font-family:Raleway; font-weight:600; font-style:normal; font-size: 20px; color: #ffffff; line-height: 25px; ">Then add that number to your average monthly unit sales</h3></div></div></div><script type="text/javascript">
									(function($) {
										$("head").append("<style>#dfd-heading-58bfb42c674d3 .dfd-heading-delimiter {border-bottom-style:solid;border-bottom-width:1px;width:200px;border-bottom-color:#2f5e7f;}#dfd-heading-58bfb42c674d3 .dfd-heading-delimiter {margin-top:15px;margin-bottom:15px;}</style>");
									})(jQuery);
								</script></div><div class="ult-spacer spacer-58bfb42c69cbc" data-id="58bfb42c69cbc" data-height="0" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div></div></div></div></div><div class="ult-spacer spacer-58bfb42c69dae" data-id="58bfb42c69dae" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div id="dfd-announce-58bfb42c69e52-3591" class="dfd-announce-module-wrap align-center no_full_bg_class  style-1 layout-1" ><div class="dfd-announce-module"><div class="module-icon" style="background:#00253d; color:#ffffff;"><i class = "dfd-icon-megaphone"></i></div><div class="module-text" style="font-family:Droid Serif; font-weight:400; font-style:normal; font-size: 35px; line-height: 49px; font-style:italic; ">A dealer that is averaging 100 R.O&#8217;s a day will add 50 units to their monthly sales volume!</div></div></div><div class="ult-spacer spacer-58bfb42c69f1f" data-id="58bfb42c69f1f" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div id="how-it-works" class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488835027460" data-parallax_sense="30" data-dfd-dots-title="How It Works"><div class="row" >
	<div class="eight columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c6e788" data-id="58bfb42c6e788" data-height="60" data-height-mobile="30" data-height-tab="0" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="2097" height="1313" src="/wp-content/uploads/2015/07/servisell-service-screen-final.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/servisell-service-screen-final.png 2097w, /wp-content/uploads/2015/07/servisell-service-screen-final-300x188.png 300w, /wp-content/uploads/2015/07/servisell-service-screen-final-768x481.png 768w, /wp-content/uploads/2015/07/servisell-service-screen-final-1024x641.png 1024w" sizes="(max-width: 2097px) 100vw, 2097px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfb42c79556" data-id="58bfb42c79556" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="four columns vc_custom_1488852523441" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c7b177" data-id="58bfb42c7b177" data-height="0" data-height-mobile="30" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42c7b2b5" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">step one</h3><h2 class="widget-sub-title uvc-sub-heading" style="font-style:italic;font-weight:400;font-size:28px;line-height:49px;">Enter the data</h2></div><div class="ult-spacer spacer-58bfb42c7b3e7" data-id="58bfb42c7b3e7" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p style="text-align: left;"><span style="color: #595959;"><strong>Your Service Writer will enter basic Customer and Vehicle Data into the SERVISELL system.</strong></span></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfb42c818eb" data-id="58bfb42c818eb" data-height="0" data-height-mobile="45" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class=" vc-row-delimiter-6"><div class="vc-row-delimiter-top"><div class="vc-row-delimiter-top-left"></div><div class="vc-row-delimiter-top-right"></div></div></div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="six columns vc_custom_1488852639700" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c85afb" data-id="58bfb42c85afb" data-height="60" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42c85c4b" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">STEP TWO</h3><h2 class="widget-sub-title uvc-sub-heading" style="font-style:italic;font-weight:400;font-size:28px;line-height:49px;">Get the results</h2></div><div class="ult-spacer spacer-58bfb42c85d3d" data-id="58bfb42c85d3d" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><strong>Within <em>seconds</em> SERVISELL provides:</strong></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfb42c87646" data-id="58bfb42c87646" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div class="dfd-icon-list-wrap  del-width-net-icon style-4  " ><ul class="dfd-icon-list"><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#003a63; "><i class = "dfd-icon-browser_website"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; "><strong><span style="color: #595959;"> Quick Screen Data</span></strong></div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#003a63; "><i class = "dfd-icon-browser_website"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; "><span style="color: #595959;"><strong>NADA Vehicle Data</strong></span></div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li></ul></div><div class="ult-spacer spacer-58bfb42c87daa" data-id="58bfb42c87daa" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><span style="color: #595959;"><strong>With this data, SERVISELL creates a <em>Detailed Snapshot</em> of your Service Customer and the Service Vehicle sitting in your Service Drive. </strong></span><span style="color: #595959;"><strong>By applying the SERVISELL Trade Algorithm to this data, the obvious deals become obvious.</strong></span></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfb42c8934f" data-id="58bfb42c8934f" data-height="0" data-height-mobile="20" data-height-tab="40" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="six columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c8aca5" data-id="58bfb42c8aca5" data-height="90" data-height-mobile="0" data-height-tab="40" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="1400" height="1232" src="/wp-content/uploads/2015/07/servisell-step-2.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/servisell-step-2.png 1400w, /wp-content/uploads/2015/07/servisell-step-2-300x264.png 300w, /wp-content/uploads/2015/07/servisell-step-2-768x676.png 768w, /wp-content/uploads/2015/07/servisell-step-2-1024x901.png 1024w" sizes="(max-width: 1400px) 100vw, 1400px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfb42c8dddb" data-id="58bfb42c8dddb" data-height="25" data-height-mobile="15" data-height-tab="15" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class=" vc-row-delimiter-6"><div class="vc-row-delimiter-top"><div class="vc-row-delimiter-top-left"></div><div class="vc-row-delimiter-top-right"></div></div></div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488851482727" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="four columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c91f93" data-id="58bfb42c91f93" data-height="60" data-height-mobile="20" data-height-tab="60" style="clear:both;display:block;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center " >
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey " ><img width="800" height="965" src="/wp-content/uploads/2015/07/servisell-clipboard-trade-alert.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2015/07/servisell-clipboard-trade-alert.png 800w, /wp-content/uploads/2015/07/servisell-clipboard-trade-alert-249x300.png 249w, /wp-content/uploads/2015/07/servisell-clipboard-trade-alert-768x926.png 768w" sizes="(max-width: 800px) 100vw, 800px" /></div>
			
		</div>
	</div>
<div class="ult-spacer spacer-58bfb42c9491c" data-id="58bfb42c9491c" data-height="0" data-height-mobile="0" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 

	<div class="eight columns vc_custom_1488852794310" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c964b1" data-id="58bfb42c964b1" data-height="40" data-height-mobile="15" data-height-tab="0" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42c966c9" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">STEP THREE</h3><h2 class="widget-sub-title uvc-sub-heading" style="font-style:italic;font-weight:400;font-size:28px;line-height:49px;">See the deals</h2></div><div class="ult-spacer spacer-58bfb42c9684e" data-id="58bfb42c9684e" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><strong><span style="color: #595959;">Your Sales Department will see a complete picture of your Service Customers Credit as well as NADA vehicle data including:</span></strong></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfb42c98303" data-id="58bfb42c98303" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div class="dfd-icon-list-wrap  del-width-net-icon style-4  " ><ul class="dfd-icon-list"><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#2a892f; "><i class = "dfd-icon-dollar"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; "><strong><span style="color: #595959;">Current FICO score</span></strong></div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#2a892f; "><i class = "dfd-icon-dollar"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; "><span style="color: #595959;"><strong>Auto inquiries in the last 30 days</strong></span></div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#2a892f; "><i class = "dfd-icon-dollar"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; "><span style="color: #595959;"><strong>NADA trade data on service vehicle</strong></span></div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li><li><div class="dfd-list-content clearfix" style=""><div class="dfd-list-icon-block text-center" style="color:#2a892f; "><i class = "dfd-icon-dollar"></i></div><div class="dfd-list-content-block content" style="font-family:Raleway; font-weight:400; font-style:normal; font-size: 15px; color: #595959; letter-spacing: 0px; font-weight:bold; "><span style="color: #595959;"><strong>Trade probability scoring</strong></span></div></div><span class="dfd-icon-item-delimiter" style="border-bottom-width:1px; border-style:dotted; border-color:; left: 46px; "></span></li></ul></div><div class="ult-spacer spacer-58bfb42c98f31" data-id="58bfb42c98f31" data-height="35" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><strong><span style="color: #595959;">By calculating the <em>Trade Probability</em>, SERVISELL instantly identifies the <em>obvious</em> deals for your Sales Department.</span></strong></p>

		</div> 
	</div> <div class="ult-spacer spacer-58bfb42c9a683" data-id="58bfb42c9a683" data-height="0" data-height-mobile="45" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class=" vc-row-delimiter-6"><div class="vc-row-delimiter-top"><div class="vc-row-delimiter-top-left"></div><div class="vc-row-delimiter-top-right"></div></div></div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488484853790" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42c9e591" data-id="58bfb42c9e591" data-height="100" data-height-mobile="30" data-height-tab="60" style="clear:both;display:block;"></div><div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns twelve"><div class="wpb_wrapper"><div id="ultimate-heading58bfb42ca192f" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-size:35px;color:#ffffff;line-height:49px;">The entire process happens within MINUTES of your customer<br />
entering the service drive.</h2></div></div></div></div></div><div class="ult-spacer spacer-58bfb42ca1a9c" data-id="58bfb42ca1a9c" data-height="100" data-height-mobile="30" data-height-tab="60" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-wrap dfd-row-bg-image dfd_vertical_parallax" id="dfd-image-bg-58bfb42ca3c50"  data-parallax_offset="" data-parallax_sense="30" data-mobile_enable="1"></div><script type="text/javascript">
					(function($) {
						$("head").append("<style>#dfd-image-bg-58bfb42ca3c50 {background-image: url(/wp-content/uploads/2017/03/servisell-background-parallax.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style>");
					})(jQuery);
				</script><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row dfd-background-dark mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488837676148" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42ca8a41" data-id="58bfb42ca8a41" data-height="120" data-height-mobile="30" data-height-tab="30" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42ca8b86" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">one - two - three</h3></div><div id="ultimate-heading58bfb42ca8d32" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-size:28px;color:#ffffff;line-height:49px;">You&#8217;re ready to go!</h2></div><div class="ult-spacer spacer-58bfb42ca8e3d" data-id="58bfb42ca8e3d" data-height="20" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42ca8fbd" class="uvc-heading dfd-delim-middle   heading-config-top dfd-disable-resposive-headings"  data-hspacer="line_only"  data-halign="center" style="text-align:center"><h3 class="widget-title uvc-main-heading" style="font-family:'Montserrat';font-size:20px;color:#ffffff;line-height:24px;letter-spacing:-1px;">Your Sales Department can now accurately consult your Service Customer on:</h3><div class="uvc-heading-spacer line_only" style="margin-top:15px;margin-bottom:15px;height:1px;"><span class="uvc-headings-line" style="border-style:solid;border-bottom-width:1px;border-color:#c0a375;width:100px;"></span></div></div><div class="ult-spacer spacer-58bfb42ca90ec" data-id="58bfb42ca90ec" data-height="20" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div class="vc-row-wrapper vc_inner vc_row-fluid"><div class="row"><div class="columns four"><div class="wpb_wrapper"><div id="info-box-58bfb42cad3b7" class="aio-icon-component   style_1"><div class="aio-icon-box top-icon "  style=""><div class="aio-icon-top"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-car_2"></i>
</div></div></div><div class="aio-icon-header"><div class="feature-title" style="font-size:18px;line-height:24px;letter-spacing:1px;">A better vehicle</div></div><!-- header --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --></div></div><div class="columns four"><div class="wpb_wrapper"><div id="info-box-58bfb42caedf1" class="aio-icon-component   style_1"><div class="aio-icon-box top-icon "  style=""><div class="aio-icon-top"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-ATM_money"></i>
</div></div></div><div class="aio-icon-header"><div class="feature-title" style="font-size:18px;line-height:24px;letter-spacing:1px;">A better payment</div></div><!-- header --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --></div></div><div class="columns four"><div class="wpb_wrapper"><div id="info-box-58bfb42cb0ac9" class="aio-icon-component   style_1"><div class="aio-icon-box top-icon "  style=""><div class="aio-icon-top"><div class="align-icon" style="text-align: center;">
<div class="aio-icon advanced  " style="color:#c0a375;background:rgba(48,48,48,0.01);border-style:;border-color:#333333;border-width:1px;width:90px;height:90px;line-height:90px;border-radius:50px;font-size:45px;display:inline-block !important;" >
	<i class="dfd-icon-sale_3"></i>
</div></div></div><div class="aio-icon-header"><div class="feature-title" style="font-size:18px;line-height:24px;letter-spacing:1px;">A better rate</div></div><!-- header --></div> <!-- aio-icon-box --></div> <!-- aio-icon-component --></div></div></div></div><div class="ult-spacer spacer-58bfb42cb0e0d" data-id="58bfb42cb0e0d" data-height="20" data-height-mobile="20" data-height-tab="25" style="clear:both;display:block;"></div><div id="dfd-announce-58bfb42cb0ed7-4056" class="dfd-announce-module-wrap align-center no_full_bg_class  style-1 layout-1" ><div class="dfd-announce-module"><div class="module-icon" style="background:#00253d; color:#ffffff;"><i class = "dfd-icon-megaphone"></i></div><div class="module-text" style="font-family:Droid Serif; font-weight:400; font-style:normal; font-size: 35px; line-height: 49px; font-style:italic; ">With SERVISELL every 100 service customers will yield  3 or more new units sold</div></div></div><div class="ult-spacer spacer-58bfb42cb0fd4" data-id="58bfb42cb0fd4" data-height="0" data-height-mobile="45" data-height-tab="0" style="clear:both;display:block;"></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488838091649" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="two columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 

	<div class="eight columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42cbad3c" data-id="58bfb42cbad3c" data-height="120" data-height-mobile="50" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42cbae89" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">Three Big Questions</h3></div><div class="ult-spacer spacer-58bfb42cbaf7e" data-id="58bfb42cbaf7e" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div><div class="dfd-service-module-wrap "><ul class="dfd-service-list dfd-mobile-keep-height clearfix full-width-elements"><li class="dfd-service-item " style="border-style: dotted; border-color: #595959; "><div class="dfd-service-item style-01  hover-01 " ><div class="dfd-service-front dfd-equalize-height" style=""><div class="module-icon"style="font-size:90px;"><i class="featured-icon dfd-icon-chat_question" style="font-size:45px; color:#c0a375; opacity:1;" ></i></div><div class="content-wrapper"><h6 class="info-banner-title " style="line-height: 24px; ">QUESTION 1</h6><div class="info-banner-subtitle subtitle" style="font-size: 18px; color: #595959; line-height: 35px; font-style:italic; ">How do you figure that we’ll convert 3% of our Service Traffic Daily?</div></div></div><div class="dfd-service-description dfd-service-back dfd-equalize-height" style=""><div class="description" style=""><h6>ANSWER</h6>
<h6>HERE&#8217;S AN EXAMPLE</h6>
<p>&nbsp;</p>
<ul>
<li>100 R.O.’s a day = 2,000 R.O.’s a month</li>
<li>No less than 50% of the traffic will generate a credit score (1,000 Trade Alerts)</li>
<li>Of those Trade Alerts, no less than 50% will have trade equity and good credit
<ul>
<li><strong>That&#8217;s 500 QUALIFIED UPS for sales</strong></li>
</ul>
</li>
<li>Out of those <strong>500 QUALIFIED UPS</strong>, no less than one in ten will purchase (50 new units)</li>
</ul>
</div></div></div></li></ul><script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									$(".dfd-service-module-wrap").each(function(){
										$(this).find(".dfd-equalize-height").equalHeights();
									});
								});
							})(jQuery);
						</script></div>
		</div> 
	</div> 

	<div class="two columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row dfd-background-dark mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488850259873" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="two columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 

	<div class="eight columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42cc26e5" data-id="58bfb42cc26e5" data-height="120" data-height-mobile="50" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42cc2801" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">Three Big Questions</h3></div><div class="ult-spacer spacer-58bfb42cc28e1" data-id="58bfb42cc28e1" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div><div class="dfd-service-module-wrap "><ul class="dfd-service-list dfd-mobile-keep-height clearfix full-width-elements"><li class="dfd-service-item " style="border-style: dotted; border-color: #595959; "><div class="dfd-service-item style-01  hover-01 " ><div class="dfd-service-front dfd-equalize-height" style=""><div class="module-icon"style="font-size:90px;"><i class="featured-icon dfd-icon-chat_question" style="font-size:45px; color:#c0a375; opacity:1;" ></i></div><div class="content-wrapper"><h6 class="info-banner-title " style="line-height: 24px; ">QUESTION 2</h6><div class="info-banner-subtitle subtitle" style="font-size: 18px; color: #595959; line-height: 35px; font-style:italic; ">How can you pull a customers credit without their consent?</div></div></div><div class="dfd-service-description dfd-service-back dfd-equalize-height" style=""><div class="description" style=""><h6>ANSWER</h6>
<p>&nbsp;</p>
<p>Because Quick Screen is a soft-pull solution that does NOT require consumer consent and has no impact on the customers credit score, we are required by law to provide a pre-sell certificate to each qualified customer. This is done automatically.</p>
</div></div></div></li></ul><script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									$(".dfd-service-module-wrap").each(function(){
										$(this).find(".dfd-equalize-height").equalHeights();
									});
								});
							})(jQuery);
						</script></div>
		</div> 
	</div> 

	<div class="two columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div  class="vc-row-wrapper wpb_row mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488838091649" data-parallax_sense="30" data-dfd-dots-title=""><div class="row" >
	<div class="two columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 

	<div class="eight columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div class="ult-spacer spacer-58bfb42cca44f" data-id="58bfb42cca44f" data-height="120" data-height-mobile="50" data-height-tab="40" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42cca570" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;letter-spacing:-1px;">Three Big Questions</h3></div><div class="ult-spacer spacer-58bfb42cca64c" data-id="58bfb42cca64c" data-height="0" data-height-mobile="50" data-height-tab="0" style="clear:both;display:block;"></div><div class="dfd-service-module-wrap "><ul class="dfd-service-list dfd-mobile-keep-height clearfix full-width-elements"><li class="dfd-service-item " style="border-style: dotted; border-color: #595959; "><div class="dfd-service-item style-01  hover-01 " ><div class="dfd-service-front dfd-equalize-height" style=""><div class="module-icon"style="font-size:90px;"><i class="featured-icon dfd-icon-chat_question" style="font-size:45px; color:#c0a375; opacity:1;" ></i></div><div class="content-wrapper"><h6 class="info-banner-title " style="line-height: 24px; ">QUESTION 3</h6><div class="info-banner-subtitle subtitle" style="font-size: 18px; color: #595959; line-height: 35px; font-style:italic; ">What’s the cost?</div></div></div><div class="dfd-service-description dfd-service-back dfd-equalize-height" style=""><div class="description" style=""><h6>ANSWER</h6>
<p>At a 3% Conversion, your Cost per Acquisition is less than $150 per unit sold.</p>
<p>Most of these new units sold will include the service vehicle as a trade.</p>
</div></div></div></li></ul><script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									$(".dfd-service-module-wrap").each(function(){
										$(this).find(".dfd-equalize-height").equalHeights();
									});
								});
							})(jQuery);
						</script></div>
		</div> 
	</div> 

	<div class="two columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 
</div><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div><div id="#signup" class="vc-row-wrapper wpb_row dfd-background-dark equal-height-columns mobile-destroy-equal-heights aligh-content-verticaly vc_custom_1488757266723" data-parallax_sense="30" data-dfd-dots-title="Demo"><div class="row" >
	<div class="twelve columns" data-parallax_sense="30">
		<div class="wpb_wrapper">
			<div id="ultimate-heading58bfb42cd03f0" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h3 class="widget-title uvc-main-heading" style="font-style:normal;font-weight:600;font-size:48px;color:#ffffff;letter-spacing:-1px;">Thank you for your interest in SERVISELL</h3></div><div class="ult-spacer spacer-58bfb42cd04d6" data-id="58bfb42cd04d6" data-height="20" data-height-mobile="20" data-height-tab="20" style="clear:both;display:block;"></div><div id="ultimate-heading58bfb42cd05d6" class="uvc-heading dfd-delim-top   heading-config-top dfd-disable-resposive-headings"  data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style=""></div><h2 class="widget-sub-title uvc-sub-heading" style="font-size:28px;color:#ffffff;line-height:32px;">The Ultimate Service Conversion Resource</h2></div><div class="ult-spacer spacer-58bfb42cd06a2" data-id="58bfb42cd06a2" data-height="45" data-height-mobile="20" data-height-tab="40" style="clear:both;display:block;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h6 style="text-align: center;">Transform your service department into a sales goldmine</h6>

		</div> 
	</div> <div class="ubtn-ctn-center"><a class="ubtn-link ubtn-center tooltip-58bfb42cd20a0"  href = "/contact/" target="_self"><span class=" ubtn ubtn-custom ubtn-fade-bg    ubtn-sep-icon ubtn-sep-icon-at-left  ubtn-center  "  data-hover="#003a63" data-border-color="" data-hover-bg="#ffffff" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="width:px;min-height:px;line-height:px;padding:15px 80px;border:none;background: #003a63;color: #ffffff;"><span class="ubtn-data ubtn-icon"><i class="icomoon-dfd_twenty_fifth_socicon_android" style="font-size:21px;color:#4f4a43;"></i></span><span class="ubtn-hover"></span><span class="ubtn-data ubtn-text" data-lang="en">GET STARTED TODAY</span></span></a></div>
		</div> 
	</div> 
</div><div class="dfd-row-bg-wrap dfd-row-bg-image dfd_vertical_parallax" id="dfd-image-bg-58bfb42cd3ff2"  data-parallax_offset="" data-parallax_sense="30" data-mobile_enable="1"></div><script type="text/javascript">
					(function($) {
						$("head").append("<style>#dfd-image-bg-58bfb42cd3ff2 {background-image: url(/wp-content/uploads/2017/03/servisell-ready-to-go.jpg);background-repeat: no-repeat;background-size: cover;background-attachment: scroll;}</style>");
					})(jQuery);
				</script><div class="dfd-row-bg-overlay" style="opacity: 0.8;"></div></div>
	
	
</section>			
		</div>
		
		<div class="body-back-to-top align-right"><i class="dfd-added-font-icon-right-open"></i></div>

		


<div id="footer-wrap">
	
	<section id="footer" class="footer-style-2 dfd-background-dark">

			<div class="row">
					<div class="three columns">
				<section id="crum_contacts_widget-17" class="widget widget_crum_contacts_widget"><h3 class="widget-title">About company</h3>SERVISELL is dedicated to developing intuitive and highly effective sales solutions for our dealer clients nationwide. The success of our client is paramount. SERVISELL strives to provide the cutting-edge sales systems needed to give our dealers the advantage in today’s highly competitive marketplace.</section>			</div>
					<div class="three columns">
				<section id="dfd_vcard_simple-12" class="widget widget_dfd_vcard_simple"><h3 class="widget-title">Contact us</h3>
		<div id="dfd-vcard-widget-58bfb42cd999c" class="row">
			<div class="twelve columns">
				<div class="dfd-vcard-wrap">
											<div class="vcard-field">
							<i class="dfd-icon-tablet2"></i>
														<p><font color="#ffffff">877.473.4280</font></p>
						</div>
																<div class="vcard-field">
							<i class="dfd-icon-email_1"></i>
														<p>
															<a href="mailto:info@servisell.com" title="" >info@servisell.com</a>
													</div>
																<div class="vcard-field">
							<i class="dfd-icon-navigation"></i>
														<p>1001 Woodward Ave.<br />5th Floor<br />Detroit, MI 48226</p>
						</div>
														</div>
			</div>
		</div>
				<script type="text/javascript">
			(function($) {
				$('head').append('<style>#dfd-vcard-widget-58bfb42cd999c .dfd-vcard-wrap {background: #003a63;}#dfd-vcard-widget-58bfb42cd999c .dfd-vcard-wrap {border-style: none;}#dfd-vcard-widget-58bfb42cd999c .dfd-vcard-wrap {-webkit-border-radius: 2px;-moz-border-radius: 2px;-o-border-radius: 2px;border-radius: 2px;}</style>');
			})(jQuery);
		</script>
				
		</section>			</div>
					<div class="three columns">
				<section id="text-32" class="widget widget_text"><h3 class="widget-title">Quick Links</h3>			<div class="textwidget"><a href="/login">Client Login</a><br></br>

<a href="/contact/">Email Servisell</a><br></br>

<a href="/privacy-policy-terms-use/">Privacy Policy</a>
</div>
		</section>			</div>
					<div class="three columns">
				<section id="text-33" class="widget widget_text"><h3 class="widget-title">Servisell powered by:</h3>			<div class="textwidget"><img src="/images/CDKGlobalLogo_color.png" alt="CDK Global logo" style="width:100px;height:80px;">
<br>
<img src="/wp-content/uploads/2017/02/NADA-Values_rgb-Jan-2017.png" alt="NADA" style="width:100px;height:72px;">



</div>
		</section>			</div>
			</div>

	</section>

			<section id="sub-footer" class=" dfd-background-dark">
			<div class="row">
				<div class="twelve columns subfooter-copyright text-center">
					© copyright 2017 Servisell				</div>
			</div>
		</section>
	
</div>



</div>

<div id="sidr">
	<div class="sidr-top">
						<div class="logo-for-panel">
		<div class="inline-block">
			<a href="/">
				<img src="/wp-content/uploads/2017/02/servisell-logo-web-260x50.png" alt="Servisell" data-retina="/wp-content/uploads/2017/02/servisell-logo-web.png" data-retina_w="260" data-retina_h="50" style="height: 50px;" />
			</a>
		</div>
	</div>
			</div>
	<div class="sidr-inner"></div>
		</div>
<a href="#sidr-close" class="dl-trigger dfd-sidr-close"></a>

<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2Cregular%2C500%2C600%2C700%2C800%2C900">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Droid+Serif%3Aregular%2Citalic%2C700%2C700italic">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat%3A">
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.form.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/inc/vc_custom/Ultimate_VC_Addons/assets/min-js/ultimate.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/plugins.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/hammer.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.smoothscroll.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.onepagescroll.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajax_var = {"url":"\/wp-admin\/admin-ajax.php","nonce":"ac471e7152"};
/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/post-like.min.js'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/ronneby/assets/js/jquery.mega-menu.run.js'></script>
<script type='text/javascript' src='/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
</body>
</html>
