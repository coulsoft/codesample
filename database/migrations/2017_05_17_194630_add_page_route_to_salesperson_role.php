<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageRouteToSalespersonRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roles = Role::where('name', '=', Role\Names::Salesperson)->get();
        foreach($roles as $role)
        {
            $permissions = Permission::where('role_id', '=', $role->id)->
                    where('url', '=', '/sales/page*')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create([
                    'role_id' => $role->id,
                    'url' => '/sales/page*',
                    'permission' => 'RU'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $roles = Role::where('name', '=', Role\Names::Salesperson )->get();
        foreach($roles as $role)
        {
            $permissions = Permission::where('role_id', '=', $role->id)->
                    where('url', '=', '/sales/page*')->get();
            foreach($permissions as $permission)
            {
                $permission->delete();
            }
        }
    }
}
