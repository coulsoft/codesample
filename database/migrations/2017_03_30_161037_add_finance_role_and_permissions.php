<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinanceRoleAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (count(Role::where('name', '=', Role\Names::Finance)->get()) <= 0)
        {
            $role = Role::create(['name' => Role\Names::Finance]);
            if ($role != null)
            {
                Permission::create([
                    'role_id' => $role->id,
                    'url' => '/sales*',
                    'permission' => 'CRUD'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $roles = Role::where('name', '=', Role\Names::Finance)->get();
        foreach ($roles as $role) {
            $role->delete();
            $permissions = Permission::where('role_id', '=', $role->id)->get();
            foreach($permissions as $permission)
            {
                $permission->delete();
            }
        }
    }
}
