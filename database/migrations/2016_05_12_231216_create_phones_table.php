<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dealership_id', false, true)->foreign('dealership_id')->references('id')->on('dealerships');
            $table->unsignedInteger('user_id', false, true)->foreign('user_id')->references('id')->on('users');
            $table->enum('type', array('land_line','mobile'));
            $table->unsignedInteger('area');
            $table->unsignedInteger('prefix');
            $table->unsignedInteger('line');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phones');
    }
}
