<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDemoPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = Role::getRoleIdByName(Role\Names::Demo);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/demo*')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/demo*', 'permission', 'R']);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         $id = Role::getRoleIdByName(Role\Names::Demo);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/demo*')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
    }
}
