<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCredentialsToDealerships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealerships', function($table)
        {
            $table->dropColumn('experian_password');
            $table->dropColumn('experian_id');
            $table->string('credit_username');
            $table->string('credit_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealerships', function($table)
        {
            $table->dropColumn('credit_password');
            $table->dropColumn('credit_username');
            $table->integer('experian_id');
            $table->string('experian_password');
        });
    }
}
