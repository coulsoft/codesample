`<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDmsFieldsToDealerships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealerships', function($table)
        {
            $table->string('dms_username');
            $table->boolean('dms_initialized');
            $table->enum('dms_vendor', array('CDK', 'COX', 'REYNOLDS'));
            $table->datetime('last_pulled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealerships', function($table)
        {
            $table->dropColumn('last_pulled');
            $table->dropColumn('dms_vendor');
            $table->dropColumn('dms_initialized');
            $table->dropColumn('dms_username');
        });
    }
}
