<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSalesStatus extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function($table) {
            $table->dropColumn('sales_status');
        });
        Schema::table('sales', function($table) {
            $table->enum('sales_status', array('New Opportunity', 'Interested',
                'Not Interested', 'Sold No Trade', 'Sold With Trade'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function($table) {
            $table->dropColumn('sales_status');
        });
        Schema::table('sales', function($table) {
            $table->enum('sales_status', array('new', 'interested', 'declined',
                'sold'));
        });
    }

}
