<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = Role::getRoleIdByName(Role\Names::Administrator);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/*')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/*', 'permission', 'CRUD']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::Promo);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/promo')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/promo', 'permission', 'CR']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::ServiceWriter);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/service')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/service', 'permission', 'CRU']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::Salesperson);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/sales')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/sales', 'permission', 'CRU']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::ServiceManager);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/service*')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/service*', 'permission', 'CRUD']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::SalesManager);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/sales*')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/sales*', 'permission', 'CRUD']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::Dealer);
        if($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                    where('url', '=', '/*')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/sales*', 'permission', 'CRUD']);
                Permission::create(['role_id' => $id,
                    'url' => '/service*', 'permission', 'CRUD']);
                Permission::create(['role_id' => $id,
                    'url' => '/dealer*', 'permission', 'CRUD']);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         $id = Role::getRoleIdByName(Role\Names::Administrator);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/*')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::Promo);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/promo')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::ServiceWriter);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/service')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::Salesperson);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/sales')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::ServiceManager);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/service*')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::SalesManager);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/sales*')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::Administrator);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/service*')->
                     orwhere('url', '=', '/sales*')->
                     orwhere('url', '=', '/dealer*')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
    }
}
