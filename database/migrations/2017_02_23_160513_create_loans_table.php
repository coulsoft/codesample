<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id', false, true)->foreign('service_id')->references('id')->on('service')->onDelete('cascade');
            $table->integer('original');
            $table->integer('term');
            $table->integer('payment');
            $table->integer('rate');
            $table->integer('percent_paid');
            $table->integer('payoff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loans');
    }
}
