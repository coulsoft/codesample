<?php

use App\Dealership;
use App\Profile;
use App\Role;
use App\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateDefaultUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (User::all()->count() <= 0)
        {
            $dealership = Dealership::first();
            $role = Role::where('name', '=', Role\Names::Administrator)->get()->first();
            $user = User::create([
                'name' => 'DeleteMe',
                'email' => 'contact@servisell.com'
            ]);
            if (isset($user->id) && isset($role->id))
            {
                Profile::create([
                    'user_id' => $user->id,
                    'role_id' => $role->id,
                    'dealership_id' => $dealership->id,
                    'landing_page' => '/home',
                    'title' => 'DeleteMe',
                    'first_name' => 'Delete',
                    'last_name' => 'Me'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $users = User::where('name', '=', 'DeleteMe')->get();
        foreach($users as $user)
        {
            $profiles = Profile::where('id', '=', $user->id)->get();
            foreach($profiles as $profile)
            {
                $profile->delete();
            }
            $user->delete();
        }
    }
}
