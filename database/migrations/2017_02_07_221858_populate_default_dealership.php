<?php

use App\Dealership;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateDefaultDealership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Dealership::all()->count() <= 0)
        {
            Dealership::create([
                'name' => 'DeleteMe',
                'address_line1' => '123 Main',
                'city' => 'Grand Rapids',
                'state' => 'MI',
                'zip' => '49546',
                'region' => 5
                    ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dealerships = Dealership::where('name', '=', 'DeleteMe')->get();
        foreach($dealerships as $dealership)
        {
            $dealership->delete();
        }
    }
}
