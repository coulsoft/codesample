<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegalInfoToDealerships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealerships', function($table)
        {
            $table->string('merchant_account_id');
            $table->string('business_license_file');
            $table->string('lender_agreement_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealerships', function($table)
        {
            $table->dropColumn('merchant_account_id');
            $table->dropColumn('business_license_file');
            $table->dropColumn('lender_agreement_file');
        });
    }
}
