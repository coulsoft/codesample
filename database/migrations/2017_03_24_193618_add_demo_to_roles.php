<?php

use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDemoToRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (count(Role::where('name', '=', Role\Names::Demo)->get()) <= 0) {
            Role::create(['name' => Role\Names::Demo]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $roles = Role::where('name', '=', Role\Names::Demo)->get();
        foreach ($roles as $role) {
            $role->delete();
        }
    }
}
