<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditProviderToDealerships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealerships', function($table)
        {
            $table->enum('credit_provider', array('700CREDIT', 'CBC', 'EQUIFAX', 'EXPERIAN', 'IPRECHECK', 'TRANSUNION'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealerships', function($table)
        {
            $table->dropColumn('credit_provider');
        });
    }
}
