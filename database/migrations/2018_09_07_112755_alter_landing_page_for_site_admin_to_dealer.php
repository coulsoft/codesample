<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLandingPageForSiteAdminToDealer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role=\App\Role::where('name', '=', \App\Role\Names::SiteAdmin)->first();
        $permission = \App\Permission::where('role_id', '=', $role->id)->where('url', '=', '/dealer*')->first();
        if ($permission == null)
        {
            \App\Permission::create(['role_id' => $role->id,
                'url' => '/dealer*', 'permission' => 'CRUD']);
        }
        $profiles = \App\Profile::where('role_id', '=', $role->id)->get();
        foreach($profiles as $profile)
        {
            $profile->landing_page = '/dealer';
            $profile->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role=\App\Role::where('name', '=', \App\Role\Names::SiteAdmin)->first();
        $profiles = \App\Profile::where('role_id', '=', $role->id)->get();
        foreach($profiles as $profile)
        {
            $profile->landing_page = '/sales';
            $profile->save();
        }
        $permission = \App\Permission::where('role_id', '=', $role->id)->where('url', '=', '/dealer*')->first();
        if ($permission != null)
        {
            $permission->delete();
        }

    }
}
