<?php

use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDealerToSiteAdminInRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roles = Role::where('name', '=', Role\Names::Dealer)->get();
        foreach($roles as $role)
        {
            $role->name = Role\Names::SiteAdmin;
            $role->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        $roles = Role::where('name', '=', Role\Names::SiteAdmin)->get();
        foreach($roles as $role)
        {
            $role->name = Role\Names::Dealer;
            $role->save();
        }
    }
}
