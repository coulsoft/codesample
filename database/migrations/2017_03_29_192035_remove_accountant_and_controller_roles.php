<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAccountantAndControllerRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roles = Role::where('name', '=', Role\Names::Accountant)->get();
        foreach ($roles as $role) {
            $role->delete();
            $permissions = Permission::where('role_id', '=', $role->id)->get();
            foreach($permissions as $permission)
            {
                $permission->delete();
            }
        }
        $roles = Role::where('name', '=', Role\Names::Controller)->get();
        foreach ($roles as $role) {
            $role->delete();
            $permissions = Permission::where('role_id', '=', $role->id)->get();
            foreach($permissions as $permission)
            {
                $permission->delete();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (count(Role::where('name', '=', Role\Names::Accountant)->get()) <= 0)
        {
            $role = Role::create(['name' => Role\Names::Accountant]);
            if ($role != null)
            {
                Permission::create([
                    'role_id' => $role->id,
                    'url' => '/accounting',
                    'permission' => 'CRUD'
                ]);
            }
        }
        if (count(Role::where('name', '=', Role\Names::Controller)->get()) <= 0)
        {
            $role = Role::create(['name' => Role\Names::Controller]);
            if ($role != null)
            {
                Permission::create([
                    'role_id' => $role->id,
                    'url' => '/controller',
                    'permission' => 'CRUD'
                ]);
            }
        }
    }
}
