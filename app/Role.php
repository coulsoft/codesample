<?php

namespace App\Role;

abstract class Names
{
    const Accountant = 'accountant';
    const Administrator = 'administrator';
    const Controller = 'controller';
    const Dealer = 'dealer';
    const DealershipAdministrator = 'dealership administrator';
    const Demo = 'demo';
    const Finance = 'finance';
    const Promo = 'promo';
    const SalesManager = 'sales manager';
    const Salesperson = 'salesperson';
    const ServiceManager = 'service manager';
    const ServiceWriter = 'service writer';
    const SiteAdmin = 'site administrator';
}

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $fillable = array('name');
    
    static function getRoleIdByName($roleName)
    {
        $result = null;
        
        $roles = Role::where('name', '=', $roleName)->get();
        if (isset($roles) && (count($roles) > 0))
        {
            $result = $roles[0]->id;
        }
        return $result;
    }
    
}
