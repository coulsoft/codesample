<?php

// use Auth;
use App\Profile;

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// Route::get('/', 'WelcomeController@index');

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function ()
{
    Route::get('/', 'WelcomeController@index');
    Route::get('/test-drive/', 'WelcomeController@index');
    Route::get('/contact/', 'WelcomeController@index');
    Route::post('/contact', 'ContactController@index');
    Route::get('/privacy-policy-terms-use/', 'WelcomeController@index');
    Route::get('/contact-servisell/', 'WelcomeController@index');
    Route::get('/wp-admin/', 'WelcomeController@index');
});

Route::group(['middleware' => 'web'], function ()
{
    Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::get('/dealer', 'DealerController@index');
    Route::post('/dealer', 'DealerController@store');
    Route::get('/register', 'HomeController@register');
    Route::post('/register', 'HomeController@store');
    Route::delete('/register/{id}', 'HomeController@destroy');
    Route::get('/register/{id}/edit', 'HomeController@edit');
    Route::put('/register/{id}', 'HomeController@update');
    Route::get('/register/{id}/byDealership', 'HomeController@filterByDealership');
    Route::get('/dealer/register', 'HomeController@register');
    Route::post('/dealer/register', 'HomeController@store');
    Route::delete('/dealer/register/{id}', 'HomeController@destroy');
    Route::get('/dealer/register/{id}/edit', 'HomeController@edit');
    Route::put('/dealer/register/{id}', 'HomeController@update');
    Route::get('/dealer/register/{id}/byDealership', 'HomeController@filterByDealership');
    Route::get('/nada', 'NADAController@index');
    Route::post('/nada', 'NADAController@store');
    Route::get('/experian', 'ExperianController@index');
    Route::post('/experian', 'ExperianController@store');
    Route::get('/sales/page/{page}', 'SalesController@page');
    Route::get('/sales/{id}/page/{page}', 'SalesController@show');
    Route::resource('/downloads', 'DownloadsController');
    Route::resource('/terms', 'TermsController');
    Route::resource('/sales/trade_alert', 'TradeAlertController');
    Route::resource('/product', 'ProductController');
    Route::resource('/promo', 'PromoController');
    Route::resource('/roles', 'RolesController');
    Route::resource('/permissions', 'PermissionsController');
    Route::resource('/profiles', 'ProfileController');
    Route::resource('/administrator', 'AdministratorController');
    Route::resource('/dealerships', 'DealershipController');
    Route::resource('/service/manager', 'ServiceManagerController');
    Route::resource('/sales/manager', 'SalesManagerController');
    Route::resource('/service', 'ServiceController');
    Route::resource('/sales', 'SalesController');
    Route::resource('/credit', 'CreditReportController');
    Route::resource('/demo', 'DemoController');
    Route::resource('/reports/NADAMonthly', 'Reports\NADAMonthly');
});
