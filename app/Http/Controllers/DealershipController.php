<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Dealership;
use App\Profile;
use App\Role;
use Auth;
use Crypt;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Validator;

class DealershipController extends Controller
{
    
    use AuthenticatesAndRegistersUsers;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
                'name' => 'required|max:255',
                'address_line1' => 'required|max:255',
                'city' => 'required|max:255',
                'state' => 'required|max:2|in:AL,AK,AZ,AR,CA,CO,CT,DE,DC,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY',
                'zip' => 'required|regex:/^[0-9]{5}(?:-[0-9]{4})?$/',
                'credit_provider' => 'required|in:700CREDIT,CBC,EQUIFAX,EXPERIAN,IPRECHECK,TRANSUNION',
                'credit_username' => 'required|max:255|unique:dealerships',
                'credit_password' => 'required|confirmed|min:6',
                'credit_password_confirmation' => 'required|min:6'
            ],['zip.regex' => 'Enter a valid zip code']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function update_validator(array $data)
    {
        return Validator::make($data, [
                'name' => 'max:255',
                'address_line1' => 'max:255',
                'city' => 'max:255',
                'state' => 'max:2|in:AL,AK,AZ,AR,CA,CO,CT,DE,DC,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY',
                'zip' => 'regex:/^[0-9]{5}(?:-[0-9]{4})?$/',
                'credit_provider' => 'required|in:700CREDIT,CBC,EQUIFAX,EXPERIAN,IPRECHECK,TRANSUNION',
                'credit_username' => 'max:255',
            ],['zip.regex' => 'Enter a valid zip code']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $currentProfile = Profile::getCurrentProfile();
        if ($currentRole == Role\Names::Administrator)
        {
            $dealerships = Dealership::all();
        }
        else
        {
            $dealerships = Dealership::where('name', '<>', 'DeleteMe')->get();
        }
        return view('data.dealerships', compact('dealerships', 'currentUser', 'currentRole', 'currentProfile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $validator = $this->validator($request->all());
        $fails = $validator->fails();

        if ($fails)
        {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $input = $request->input();
        $newDealership = Dealership::create([
                'name' => $input['name'],
                'address_line1' => $input['address_line1'],
                'address_line2' => ((isset($input['address_line2'])) ? $input['address_line2'] : ''),
                'city' => $input['city'],
                'state' => $input['state'],
                'zip' => $input['zip'],
                'region' => 5,
                'merchant_account_id' => ((isset($input['merchant_account_id'])) ?
                    $input['merchant_account_id'] : ''),
                'credit_provider' => !empty($input['credit_provider']) ? $input['credit_provider'] : '700CREDIT',
                'credit_username' => Crypt::encrypt($input['credit_username']),
                'credit_password' => Crypt::encrypt($input['credit_password']),
                'dms_vendor' => (isset($input['dms_vendor']) ? $input['dms_vendor'] : null ),
                'dms_username' => (isset($input['dms_username']) ? Crypt::encrypt($input['dms_username']) : null )
            ]);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dealerships = Dealership::all();
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $currentProfile = Profile::getCurrentProfile();
        $dealership = Dealership::find($id);
        if (isset($dealership->credit_username) && (strlen($dealership->credit_username) > 0))
        {
            $dealership->credit_username = Crypt::decrypt($dealership->credit_username);
        }
        if (isset($dealership->credit_password) && (strlen($dealership->credit_password) > 0))
        {
            $dealership->credit_password = Crypt::decrypt($dealership->credit_password);
        }
        if (isset($dealership->dms_username) && (strlen($dealership->dms_username) > 0))
        {
            $dealership->dms_username = Crypt::decrypt($dealership->dms_username);
        }
        return view('data.dealerships', compact('dealerships', 'dealership', 'currentUser', 'currentRole', 'currentProfile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->update_validator($request->all());
        $fails = $validator->fails();

        if ($fails)
        {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $dealership = Dealership::find($id);
        $input = $request->input();
        if ($dealership != null)
        {
            $dealership->name = isset($input['name']) ? $input['name'] : $dealership->name;
            $dealership->address_line1 = isset($input['address_line1']) ? $input['address_line1'] : $dealership->address_line1;
            $dealership->address_line2 = isset($input['address_line2']) ? $input['address_line2'] : $dealership->address_line2;
            $dealership->city = isset($input['city']) ? $input['city'] : $dealership->city;
            $dealership->state = isset($input['state']) ? $input['state'] : $dealership->state;
            $dealership->zip = isset($input['zip']) ? $input['zip'] : $dealership->zip;
            $dealership->region = isset($input['region']) ? $input['region'] : $dealership->region;
            $dealership->merchant_account_id = isset($input['merchant_account_id']) ? $input['merchant_account_id'] : $dealership->merchant_account_id;
            $dealership->credit_provider = !empty($input['credit_provider']) ? $input['credit_provider'] : $dealership->credit_provider;
            $dealership->credit_username = (isset($input['credit_username']) && (strlen($input['credit_username']) > 0)) ? Crypt::encrypt($input['credit_username']) : $dealership->credit_username;
            $dealership->credit_password = (!empty($input['credit_password'])&&
                    !empty($input['credit_password_confirmation']) &&
                    ($input['credit_password'] == $input['credit_password_confirmation'])) ?
                    Crypt::encrypt($input['credit_password']) : $dealership->credit_password;
            $dealership->dms_username = (isset($input['dms_username']) && (strlen('dms_username') > 0)) ? Crypt::encrypt($input['dms_username']) : $dealership->dms_username;
            $dealership->dms_vendor = isset($input['dms_vendor']) ? $input['dms_vendor'] : $dealership->dms_vendor;
        }
        $dealership->save();
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dealership = Dealership::find($id);
        
        if (isset($dealership) && ($dealership != null))
        {
            $dealership->delete();
        }
        return $this->index();
    }
}
