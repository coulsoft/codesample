<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Profile;
use App\User;
use Auth;

class PromoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uuid = $request->get('uuid');
        $profiles = null;
        $profile = null;
        if ($uuid == null)
        {
            $currentUser = Auth::user();
            $user = null;
            if ($currentUser != null)
            {
                $user = User::find($currentUser->id);
            }
            $profiles = null;
            if ($user != null)
            {
                $profiles = Profile::where('user_id', '=', $user->id)->get();
            }
            $profile = null;
            if (($profiles != null) && (count($profiles) > 0))
            {
                $profile = $profiles[0];
            }
            if ($profile != null)
            {
                $uuid = $profile->uuid;
            }
        }
        if ($uuid !== null)
        {
            $profiles = \App\Profile::where('uuid', '=', $uuid)->get();
            if (($profiles != null) && (count($profiles) > 0))
            {
                $profile = $profiles[0];
            }
        }
        if ($profile != null)
        {
            $response = view('promo2', compact('profile'));
        } else
        {
            $response = redirect(url('/', [], true));
        }
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
