<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ContactController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'email' => 'required|email|max:255',
            'name' => 'required|max:255',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/']);

        if ($request->has('email')) {

            $sent = \Mail::send('email.contact', array(
                        'name' => $request->get('name'),
                        'email' => $request->get('email'),
                        'phone' => $request->get('phone'),
                        'user_message' => $request->get('message')
                            ), function($message) {
                        $message->from('contact@servisell.com');
                        $message->to('contact@servisell.com', 'Admin')->subject('SERVISELL Contact');
                    });

            if ($sent > 0) {
                $response = view('email.thankyou');
            } else {
                $response = view('email.error');
            }
        } else {
            $errors = array('message' => 'Message could not be sent at this time.  Try again later.');
            $response = redirect()->back()->with('errors');
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
