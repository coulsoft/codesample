<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->create();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::all();
        $selRoles = array();
        foreach ($roles as $role) {
            $selRoles['' . $role->id] = $role->name;
        }
        $permissions = Permission::join('roles', 'permissions.role_id', '=', 'roles.id')->get();
        return view('data.permissions', compact('permissions', 'selRoles', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $message = null;
        $role = null;
        if (array_has($input, 'selRoles'))
        {
            $role = Role::find($input['selRoles']);
        }
        if (array_has($input, 'selRoles') && array_has($input, 'url') &&
            array_has($input, 'permission') && ($role != null))
        {
            Permission::create(['role_id' => $input['selRoles'],
                'url' => $input['url'], 'permission' => $input['permission']]);
            $message = 'Permission for, ' . $role->name . ' on ' . $input['url'] . ', created.';
        }
        $roles = Role::all();
        $selRoles = array();
        foreach ($roles as $role) {
            $selRoles['' . $role->id] = $role->name;
        }
        $permissions = Permission::join('roles', 'permissions.role_id', '=', 'roles.id')->get();
        return view('data.permissions', compact('permissions', 'selRoles', 'roles', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
