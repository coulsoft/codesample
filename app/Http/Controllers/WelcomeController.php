<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Profile;
use App\Role;
use Auth;

class WelcomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $result = view('welcome2');
        $currentProfile = null;

        if ($currentUser != null)
        {
            $currentProfile = Profile::getCurrentProfile();
        }
        if ($currentProfile != null)
        {
            if ($currentProfile->accepted_ip > 0)
            {
                $result = redirect($currentProfile->landing_page);
            }
            else
            {
                $result = redirect(url('/terms/create', [], true));
            }
        }
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function testDrive()
    {
        return view('test-drive');
    }
    
    public function privacyPolicy()
    {
        return view('privacy-policy-terms-use');
    }
    
    public function contact()
    {
        return view('general-contact');
    }

}
