<?php

namespace App\Http\Controllers;

use App\Dealership;
use App\Profile;
use App\Http\Requests;
use Auth;
use Illuminate\Http\Request;

class DealerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $currentProfile = Profile::getCurrentProfile();
        $dealership = Dealership::find($currentProfile->dealership_id);
        // return response(compact('currentUser', 'currentRole', 'currentProfile', 'dealership'));
        return view('dealer', compact('currentUser', 'currentRole', 'currentProfile', 'dealership'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentProfile = Profile::getCurrentProfile();
        $dealership = Dealership::find($currentProfile->dealership_id);
        $omit_new = $request->get('omit_new');
        $include_previous = $request->get('include_previous');
        $errors = collect(array());
        $message = '';
        if ($include_previous === '')
        {
            $include_previous = 0;
        }
        if ($omit_new === '')
        {
            $omit_new = 0;
        }
        if (is_numeric($omit_new) && is_numeric($include_previous))
        {
            $currentYear = \Carbon\Carbon::now()->year;
            $include_previous = intval($include_previous);
            $omit_new = intval($omit_new);
            if (($include_previous >= 0) &&
                ($omit_new >= 0) &&
                ($include_previous <= $currentYear) &&
                ($omit_new <= $currentYear))
            {
                if ( ($omit_new > 0) && ($omit_new < $include_previous))
                {
                    $errors['omit_new'] = 'omit_new';
                    $errors['include_previous'] = 'include_previous';
                    $message = "You must include more years than you exclude.";
                }
                else
                {
                    $dealership->omit_new = ($omit_new > 0) ? $currentYear - $omit_new + 1 : 0;
                    $dealership->include_previous = ($include_previous > 0) ? $currentYear - $include_previous + 1 : 0;
                    $dealership->save();
                }
            }
            else
            {
                if (($include_previous < 0) ||
                    ($include_previous > $currentYear))
                {
                    $errors['include_previous'] = 'include_previous';
                }
                if (($omit_new < 0) ||
                    ($omit_new > $currentYear))
                {
                    $errors['omit_new'] = 'omit_new';
                }
                $message .= 'Values must not be negative or greater than the current year. ';
            }
        }
        else
        {
            if(!is_numeric($include_previous))
            {
                $errors['include_previous'] = 'include_previous';                
            }
            if (!is_numeric($omit_new))
            {
                $errors['omit_new'] = 'omit_new';
            }
            $message .= 'Values must be numbers. ';
        }
        return redirect(url('/dealer', [], true))->
            with('message', $message)->with('errors', $errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
