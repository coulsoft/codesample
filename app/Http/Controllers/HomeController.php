<?php

namespace App\Http\Controllers;

use Auth;
use App\Dealership;
use App\Http\Requests;
use App\Profile;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Validator;

class HomeController extends Controller
{

    use AuthenticatesAndRegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
                    'title' => 'required|max:255',
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $result = false;
        $currentProfile = Profile::getCurrentProfile();
        $currentRole = Profile::getCurrentRole();
        $roles = Role::all();
        $selRoles = array();
        foreach ($roles as $role)
        {
            $selRoles[$role->name] = $role->id;
        }
        switch ((int) $data['selRoles'])
        {
            case $selRoles[Role\Names::SalesManager]:
                $landing_page = '/sales';
                break;
            case $selRoles[Role\Names::Finance]:
                $landing_page = '/sales';
                break;
            case $selRoles[Role\Names::Salesperson]:
                $landing_page = '/sales';
                break;
            case $selRoles[Role\Names::ServiceManager]:
                $landing_page = '/service';
                break;
            case $selRoles[Role\Names::ServiceWriter]:
                $landing_page = '/service';
                break;
            case $selRoles[Role\Names::SiteAdmin]:
                if (($currentRole->name == Role\Names::Administrator) ||
                ($currentRole->name == Role\Names::DealershipAdministrator))
                {
                    $landing_page = '/dealer';
                }
                break;
            case $selRoles[Role\Names::Demo]:
                if ($currentRole->name == Role\Names::Administrator)
                {
                    $landing_page = '/demo';
                }
                break;
            default :
                $landing_page = '/demo';
                break;
        }
        if (isset($data['first_name']) && isset($data['last_name']) &&
                isset($data['email']) && isset($data['password']) &&
                isset($data['selRoles']) && isset($currentProfile->dealership_id) &&
                isset($landing_page) && isset($data['title']) &&
                (($currentRole->name == Role\Names::Administrator) ||
                ((($currentRole->name == Role\Names::SiteAdmin) ||
                ($currentRole->name == Role\Names::DealershipAdministrator)) &&
                ($selRoles != Role\Names::Administrator))))
        {
            $user = User::create([
                        'name' => $data['first_name'] . ' ' . $data['last_name'],
                        'email' => $data['email'],
                        'password' => bcrypt($data['password']),
            ]);
            if (isset($user))
            {
                if (isset($data['selDealerships']))
                {
                    $dealership = (int) $data['selDealerships'];
                }
                else
                {
                    $dealership = $currentProfile->dealership_id;
                }
                $newProfile = Profile::create([
                            'user_id' => $user->id,
                            'role_id' => $data['selRoles'],
                            'dealership_id' => $dealership,
                            'landing_page' => $landing_page,
                            'title' => $data['title'],
                            'first_name' => $data['first_name'],
                            'last_name' => $data['last_name']
                ]);
                if (!isset($newProfile))
                {
                    User::destroy($user->id);
                }
                else
                {
                    $result = true;
                }
            }
        }
        return $result;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $currentProfile = Profile::getCurrentProfile();
        return view('home', compact('currentUser', 'currentRole', 'currentProfile'));
    }

    /**
     * Show the application registration page.
     *
     * @return \Illuminate\Http\Response
     */
    public function register($message = '', $dealership_id = 0)
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $currentProfile = Profile::getCurrentProfile();
        $dealerships = null;
        $selDealerships = array();
        $users = null;

        if ($currentRole->name != Role\Names::Administrator)
        {
            $limitedRoles = array(Role\Names::Finance,
                Role\Names::SalesManager,
                Role\Names::Salesperson,
                Role\Names::ServiceManager,
                Role\Names::ServiceWriter,
                Role\Names::SiteAdmin);
            $roles = Role::wherein('name', $limitedRoles)->
                            orderBy('name')->get();
            $baseURL = url('/dealer/register', [], true);
            if ($currentRole->name == Role\Names::DealershipAdministrator)
            {
                $dealerships = \App\Dealership::where('name', '<>', 'DeleteMe')->get();
                foreach ($dealerships as $dealership)
                {
                    $selDealerships['' . $dealership->id] = $dealership->name;
                }
                if (($dealership_id <= 0) && (count($dealerships) > 0))
                {
                    $dealership_id = $dealerships[0]->id;
                }
            }
            else
            {
                $dealership_id = $currentProfile->dealership_id;
            }
        }
        else
        {
            $roles = Role::orderBy('name')->get();
            $dealerships = \App\Dealership::all();
            foreach ($dealerships as $dealership)
            {
                $selDealerships['' . $dealership->id] = $dealership->name;
            }
            if ($dealership_id <= 0)
            {
                $firstDealership = Dealership::first();
                $dealership_id = $firstDealership->id;
            }
            $baseURL = url('/register', [], true);
        }

        $users = User::join('profiles', 'profiles.user_id', '=', 'users.id')->
            join('roles', 'roles.id', '=', 'profiles.role_id')->
            where('profiles.dealership_id', '=', $dealership_id)->
            select('users.id as id', 'users.name as name',
                'profiles.title as title', 'roles.name as role')->get();
        $selRoles = array();
        foreach ($roles as $role)
        {
            $selRoles['' . $role->id] = $role->name;
        }

        return view('auth.register', compact('selRoles', 'message', 'currentUser', 'currentRole', 'currentProfile', 'selDealerships', 'users', 'dealership_id', 'baseURL'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = '';
        $input = $request->input();
        $validator = $this->validator($request->all());
        $fails = $validator->fails();

        if ($validator->fails())
        {
            $this->throwValidationException(
                    $request, $validator
            );
        }

        $created = $this->create($request->all());
        $dealership_id = 0;
        if ($created)
        {
            $message = 'User created!';
            $dealership_id = $request->get('selDealerships');
        }
        else
        {
            $message = 'Failed to create user.';
        }
        return $this->register($message, $dealership_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = null;
        try
        {
            $user = \App\User::join('profiles', 'profiles.user_id', '=', 'users.id')->where('users.id', '=', $id)->first();
        }
        catch (\Exception $ex)
        {
            return response($ex->getMessage());
        }
        return $this->register('', $user->dealership_id)->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentRole = Profile::getCurrentRole();
        try
        {
            $user = User::findOrFail($id);
            $first_name = (null !== $request->get('first_name')) ? $request->get('first_name') : null;
            $last_name = (null !==$request->get('last_name')) ? $request->get('last_name') : null;
            if (($first_name != null) && ($last_name != null))
            {
                $user->name = $first_name . ' ' . $last_name;
            }
            $user->email = (null !== $request->get('email')) ? $request->get('email') : $user->email;
            $profiles = Profile::where('user_id', '=', $user->id)->get();
            $roles = Role::all();
            $selRoles = array();
            foreach ($roles as $role)
            {
                $selRoles[$role->name] = $role->id;
            }
            foreach ($profiles as $profile)
            {
                $profile->title = (null !== $request->get('title')) ? $request->get('title') : $profile->title;
                $profile->first_name = ($first_name != null) ? $first_name : $profile->first_name;
                $profile->last_name = ($last_name != null) ? $last_name : $profile->last_name;
                $profile->dealership_id = (null !== $request->get('selDealerships')) ? (int) $request->get('selDealerships') : $profile->dealership_id;
                $profile->role_id = (null !== $request->get('selRoles')) ? $request->get('selRoles') : $profile->role_id;

                if ((null !== $request->get('selRoles')))
                {
                    $landing_page = null;
                    switch ((int) $request->get('selRoles'))
                    {
                        case $selRoles[Role\Names::SalesManager]:
                            $landing_page = '/sales';
                            break;
                        case $selRoles[Role\Names::Salesperson]:
                            $landing_page = '/sales';
                            break;
                        case $selRoles[Role\Names::ServiceManager]:
                            $landing_page = '/service';
                            break;
                        case $selRoles[Role\Names::ServiceWriter]:
                            $landing_page = '/service';
                            break;
                        case $selRoles[Role\Names::SiteAdmin]:
                            if ($currentRole->name == Role\Names::Administrator)
                            {
                                $landing_page = '/home';
                            }
                            break;
                        case $selRoles[Role\Names::Demo]:
                            if ($currentRole->name == Role\Names::Administrator)
                            {
                                $landing_page = '/demo';
                            }
                            break;
                        case $selRoles(Role\Names::Finance):
                            $landing_page = '/sales';
                            break;
                        case $selRoles(Role\Names::DealershipAdministrator):
                            $landing_pate = '/dealerships';
                            break;
                        default :
                            $landing_page = '/home';
                            break;
                    }
                    if ($landing_page != null)
                    {
                        $profile->landing_page = $landing_page;
                    }
                }
                $profile->save();
            }
            $user->save();
        }
        catch (\Exception $ex)
        {
            return response($ex->getMessage());
        }
        return $this->register('', (null !== $request->get('selDealerships')) ? (int) $request->get('selDealerships') : 0);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dealership_id = 0;
        try
        {
            $user = User::findOrFail($id);
            if ($user != null)
            {
                $profiles = Profile::where('user_id', '=', $user->id)->get();
                foreach ($profiles as $profile)
                {
                    $dealership_id = $profile->dealership_id;
                    $profile->destroy($profile->id);
                }
                $user->destroy($user->id);
            }
        }
        catch (\Exception $ex)
        {
            return response($ex->getMessage());
        }
        return $this->register('User "' . $user->name . '" deleted.', $dealership_id);
    }

    /**
     * View users filtered by dealership
     */
    public function filterByDealership($id)
    {
        return $this->register('', $id);
    }

}
