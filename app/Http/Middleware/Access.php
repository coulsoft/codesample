<?php

namespace App\Http\Middleware;

use App\Permission;
use App\Role;
use Auth;
use Closure;
use App\User;
use Illuminate\Http\Request;
use App\Profile;

class Access
{

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = redirect()->guest('login');
        $currentProfile = Profile::getCurrentProfile();
        $currentRole = Profile::getCurrentRole();
        $permissions = null;
        $url = null;
        $match = 0;
        
        if ($currentRole != null)
        {
            $permissions = Permission::where('role_id', '=', $currentRole->id)->get();
        }
        if (($permissions != null) && (count($permissions) > 0))
        {
            $url = $request->getRequestUri();
            foreach ($permissions as $permission)
            {
                $pattern = '/' . str_replace('?', '\?[^\/]+', str_replace('*', '.*', str_replace('/', '\/', $permission->url))) . '$/';
                $match = preg_match($pattern, $url);
                if ($match > 0)
                {
                    break;
                }
            }
        }
        if (($currentProfile != null) && ($currentProfile->accepted_ip == 0))
        {
            $result = redirect('/terms/create');
        }
        else if ($match > false)
        {
//            if (isset($profile->landing_page) && (strlen($profile->landing_page) > 0))
//            {
//                return redirect($profile->landing_page);
//            }
//            else
//            {
                $result = $next($request);
//            }
        }

        return $result;
    }

}
