<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealership extends Model
{

    protected $fillable = array('region', 'name', 'address_line1',
        'address_line2', 'city', 'state', 'zip', 'credit_username',
        'credit_password', 'merchant_account_id', 'dms_vendor', 'dms_username',
        'dms_initialized', 'last_pulled', 'credit_provider', 'omit_new', 'include_previous');

}
