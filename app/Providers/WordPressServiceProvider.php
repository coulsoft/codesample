<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WordPressServiceProvider extends ServiceProvider
{

    protected $bootstrapFilePath = '../../../public/wp-load.php';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load assets
        wp_enqueue_style('app', '/app/public/app.css');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Load wordpress bootstrap file
        if (File::exists($this->bootstrapFilePath))
        {
            require_once $this->bootstrapFilePath;
        }
        else
        {
            throw new \RuntimeException('WordPress Bootstrap file not found!');
        }
    }

}
