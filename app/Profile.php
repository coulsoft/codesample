<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $fillable = array('user_id', 'role_id', 'landing_page','uuid',
        'title', 'first_name', 'last_name', 'dealership_id', 'address_line1',
        'address_line2', 'city', 'state', 'zip', 'phone1', 'phone2');


    /**
     * Save a new model and return the instance.
     *
     * @param  array  $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $attributes['uuid'] = \Faker\Provider\Uuid::uuid();
        return parent::create($attributes);
    }
    
    /**
     * Convenience function for getting the profile fo the current user.
     * 
     * @param: none
     * @return: The current users Profile record.
     */
    static public function getCurrentProfile()
    {
        $result = null;
        $currentUser = Auth::user();
        $profiles = null;
        $roles = null;

        if ($currentUser != null)
        {
            $profiles = Profile::where('user_id', '=', $currentUser->id)->get();
        }
        if (($profiles != null) && (count($profiles) > 0))
        {
            $result = $profiles[0];
        }
        return $result;
    }
    
    /**
     * Convenience function for getting the role of the current user.
     * 
     * @param: none
     * @return String containing the role of the current user.
     */
    static public function getCurrentRole()
    {
        $result = null;
        $profile = Profile::getCurrentProfile();
        $roles = null;

        if ($profile != null)
        {
            $roles = Role::where('id', '=', $profile->role_id)->get();
        }
        if (($roles != null) && (count($roles) > 0))
        {
            $result = $roles[0];
        }
        return $result;
    }

    /**
     * Convenience function for determining if the current user is an administrator.
     * 
     * @param none
     * @return True if the current user is an administrator, otherwise false.
     */
    static public function isAdministrator()
    {
        $result = false;
        $role = \App\Profile::getCurrentRole();

        if (($role != null) && (strcasecmp($role->name, 'administrator') == 0))
        {
            $result = true;
        }

        return $result;
    }
    
    /**
     * Convenience function for determining if the current user is an sales manager.
     * 
     * @param none
     * @return True if the current user is a sales manager, otherwise false.
     */
    static public function isSalesManager()
    {
        $role = \App\Profile::getCurrentRole();

        return (($role != null) && (strcasecmp($role->name, 'sales manager') == 0));
    }
}
