<?php

namespace App\Console;

use App\Console\Commands\DMSInitialize;
use App\Console\Commands\DMSPull;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\DMSPull::class,
        Commands\DMSInitialize::class,
        Commands\DMSRecheck::class,
        Commands\DMSImport::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
            ->hourly();
        $schedule->command('dms:pull')->
            timezone('America/New_York')->
            cron('6/15 6-21 * * *');
        $schedule->command('dms:initialize')->
            daily()->
            timezone('America/New_York')->
            at('2:00');
    }
}
