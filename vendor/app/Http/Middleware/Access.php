<?php

namespace App\Http\Middleware;

use App\Permission;
use App\Role;
use Auth;
use Closure;
use App\User;
use Illuminate\Http\Request;
use App\Profile;

class Access
{

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = null;
        $profiles = null;
        $profile = null;
        $role = null;
        $permissions = null;
        $url = null;
        $currentUser = Auth::user();
        $match = 0;

        if ($currentUser != null)
        {
            $user = User::find($currentUser->id);
        } else
        {
            return redirect()->guest('login');
        }
        if ($user != null)
        {
            $profiles = Profile::where('user_id', '=', $user->id)->get();
        }
        if (($profiles != null) && (count($profiles) > 0))
        {
            $profile = $profiles[0];
        }
        if ($profile != null)
        {
            $role = Role::find($profile->role_id);
        }
        if ($role != null)
        {
            $permissions = Permission::where('role_id', '=', $role->id)->get();
        }
        if (($permissions != null) && (count($permissions) > 0))
        {
            $url = $request->getRequestUri();
            foreach ($permissions as $permission)
            {
                $pattern = '/' . str_replace('*', '.*', str_replace('/', '\/', $permission->url)) . '$/';
                $match = preg_match($pattern, $url);
                if ($match > 0)
                {
                    break;
                }
            }
        }
        if ($match > false)
        {
//            if (isset($profile->landing_page) && (strlen($profile->landing_page) > 0))
//            {
//                return redirect($profile->landing_page);
//            }
//            else
//            {
                return $next($request);
//            }
        }

        return redirect()->guest('login');
    }

}
