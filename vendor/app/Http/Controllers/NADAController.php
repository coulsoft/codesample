<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Httpful\Request as SoapRequest;

class NADAController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private static function getToken()
    {
        $token = null;
        $soapResponse = null;
        $soapResponseBody = null;
        $parsedXml = null;

        $strUsername = env('NADA_USERNAME');
        $strPassword = env('NADA_PASSWORD');
        if (($strUsername != null) && ($strPassword != null))
        {
            $soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.nada.com/"> ' .
                '<soapenv:Header/>' .
                '<soapenv:Body>' .
                '<web:getToken>' .
                '<!--Optional:-->' .
                '<web:tokenRequest>' .
                '<!--Optional:-->' .
                '<web:Username>' . $strUsername . '</web:Username>' .
                '<!--Optional:-->' .
                '<web:Password>' . $strPassword . '</web:Password>' .
                '</web:tokenRequest>' .
                '</web:getToken>' .
                '</soapenv:Body>' .
                '</soapenv:Envelope>';
            $soapResponse = SoapRequest::post('https://webservice.nada.com/vehicles/secure/securelogin.asmx')->
                    sendsAndExpects('text/xml')->addHeader('SOAPAction', 'http://webservice.nada.com/getToken')->
                    body($soapBody)->send();
        }
        if (($soapResponse != null) && isset($soapResponse->code) && ($soapResponse->code == 200))
        {
            $soapResponseBody = $soapResponse->body;
        }
        if ($soapResponseBody != null)
        {
            $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponseBody);
            $parsedXml = simplexml_load_string($clean_xml);
        }
        if (($parsedXml !== false) && (isset($parsedXml->Body)) &&
            (isset($parsedXml->Body->getTokenResponse)) &&
            (isset($parsedXml->Body->getTokenResponse->getTokenResult)))
        {
            $token = $parsedXml->Body->getTokenResponse->getTokenResult;
        }
        return $token;
    }

    public static function getRegions()
    {
        $regions = null;
        $token = self::getToken();
        $soapResponse = null;
        $soapResponseBody = null;
        $parsedXml = null;
        $regions = array();
        if ($token != null)
        {
            // Test VIN: JTDKN3DUXD1685925
            $soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.nada.com/">' .
                '<soapenv:Header/>' .
                '<soapenv:Body>' .
                '<web:getRegions>' .
                '<!--Optional:-->' .
                '<web:l_Request>' .
                '<!--Optional:-->' .
                '<web:Token>' . $token . '</web:Token>' .
                '<web:Period>0</web:Period>' .
                '<web:VehicleType>UsedCar</web:VehicleType>' .
                '</web:l_Request>' .
                '</web:getRegions>' .
                '</soapenv:Body>' .
                '</soapenv:Envelope>';
            $soapResponse = SoapRequest::post('http://webservice.nada.com/vehicles/vehicle.asmx')->
                    sendsAndExpects('text/xml')->addHeader('SOAPAction', 'http://webservice.nada.com/getRegions')->
                    body($soapBody)->send();
        }
        if (($soapResponse != null) && isset($soapResponse->code) && ($soapResponse->code == 200))
        {
            $soapResponseBody = $soapResponse->body;
        }
        if ($soapResponseBody != null)
        {
            $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponseBody);
            $parsedXml = simplexml_load_string($clean_xml);
        }
        if (($parsedXml !== false) && (isset($parsedXml->Body)) &&
            (isset($parsedXml->Body->getRegionsResponse)) &&
            (isset($parsedXml->Body->getRegionsResponse->getRegionsResult)) &&
            (isset($parsedXml->Body->getRegionsResponse->getRegionsResult->Lookup_Struc)))
        {
            $rawRegions = $parsedXml->Body->getRegionsResponse->getRegionsResult->Lookup_Struc;
            foreach ($rawRegions as $region) {
                if (!array_has($regions, (int) $region->Code))
                {
                    $regions[(int) $region->Code] = $region->Descr;
                } else
                {
                    $regions[(int) $region->Code] .= ', ' . $region->Descr;
                }
            }
        }
        return $regions;
    }
    
    public static function getVehicleAndValues($vin, $mileage, $region)
    {
        $token = self::getToken();
        $soapResponse = null;
        if ($token != null)
        {
            // Test VIN: JTDKN3DUXD1685925
            $soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.nada.com/">' .
                '<soapenv:Header/>' .
                '<soapenv:Body>' .
                '<web:validateVin>' .
                '<!--Optional:-->' .
                '<web:l_request>' .
                '<!--Optional:-->' .
                '<web:Token>' . $token . '</web:Token>' .
                '<web:Period>0</web:Period>' .
                '<web:VehicleType>UsedCar</web:VehicleType>' .
                '<!--Optional:-->' .
                '<web:VIN>' . $vin . '</web:VIN>' .
                '</web:l_request>' .
                '</web:validateVin>' .
                '</soapenv:Body>' .
                '</soapenv:Envelope>';
            $soapResponse = SoapRequest::post('http://webservice.nada.com/vehicles/vehicle.asmx')->
                    sendsAndExpects('text/xml')->addHeader('SOAPAction', 'http://webservice.nada.com/validateVin')->
                    body($soapBody)->send();
        }
        $soapResponseBody = null;
        if (($soapResponse != null) && isset($soapResponse->code) && ($soapResponse->code == 200))
        {
            $soapResponseBody = $soapResponse->body;
        }
        $parsedXml = null;
        if ($soapResponseBody != null)
        {
            $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponseBody);
            $parsedXml = simplexml_load_string($clean_xml);
        }
        $isValidVin = false;
        if (($parsedXml !== false) && (isset($parsedXml->Body)) &&
            (isset($parsedXml->Body->validateVinResponse)) &&
            (isset($parsedXml->Body->validateVinResponse->validateVinResult)))
        {
            $isValidVin = stripos($parsedXml->Body->validateVinResponse->validateVinResult, 'TRUE') !== false;
        }
        $soapResponse = null;
        if ($isValidVin)
        {
            // Test VIN: JTDKN3DUXD1685925
            $soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.nada.com/">' .
                '<soapenv:Header/>' .
                '<soapenv:Body>' .
                '<web:getMsrpVehicleAndValueByVin>' .
                '<!--Optional:-->' .
                '<web:vehicleRequest>' .
                '<!--Optional:-->' .
                '<web:Token>' . $token . '</web:Token>' .
                '<web:Period>0</web:Period>' .
                '<web:VehicleType>UsedCar</web:VehicleType>' .
                '<!--Optional:-->' .
                '<web:Vin>' . $vin . '</web:Vin>' .
                '<web:Region>' . $region . '</web:Region>' .
                '<web:Mileage>' . $mileage . '</web:Mileage>' .
                '</web:vehicleRequest>' .
                '</web:getMsrpVehicleAndValueByVin>' .
                '</soapenv:Body>' .
                '</soapenv:Envelope>';
            $soapResponse = SoapRequest::post('http://webservice.nada.com/vehicles/vehicle.asmx')->
                    sendsAndExpects('text/xml')->addHeader('SOAPAction', 'http://webservice.nada.com/getMsrpVehicleAndValueByVin')->
                    body($soapBody)->send();
        }
        $soapResponseBody = null;
        if (($soapResponse != null) && isset($soapResponse->code) && ($soapResponse->code == 200))
        {
            $soapResponseBody = $soapResponse->body;
        }
        $parsedXml = null;
        if ($soapResponseBody != null)
        {
            $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponseBody);
            $parsedXml = simplexml_load_string($clean_xml);
        }
        $vehicleAndValues = null;
        if (($parsedXml !== false) && (isset($parsedXml->Body)) &&
            (isset($parsedXml->Body->getMsrpVehicleAndValueByVinResponse)) &&
            (isset($parsedXml->Body->getMsrpVehicleAndValueByVinResponse->getMsrpVehicleAndValueByVinResult)))
        {
            $vehicleAndValues = $parsedXml->Body->getMsrpVehicleAndValueByVinResponse->getMsrpVehicleAndValueByVinResult;
        }
        return $vehicleAndValues;
    }

    public function index()
    {
        $regions = self::getRegions();
        return response(view('nada.nada', array('regions' => $regions)));
    }

    public function store(Request $request)
    {
        $vehicleAndValues = null;
        $input = $request->input();
        if (array_has($input, 'txtVin') && array_has($input, 'selRegions') &&
                array_has($input, 'txtMileage'))
        {
            $vehicleAndValues = self::getVehicleAndValues($input['txtVin'],
                    $input['txtMileage'], $input['selRegions']);
        }
        return response(view('nada.results', array('vehicleAndValues' => $vehicleAndValues)));
    }

}
