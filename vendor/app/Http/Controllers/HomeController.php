<?php

namespace App\Http\Controllers;

use Auth;
use App\Dealership;
use App\Http\Requests;
use App\Profile;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Validator;

class HomeController extends Controller
{

    use AuthenticatesAndRegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
                'title' => 'required|max:255',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $result = false;
        $currentProfile = Profile::getCurrentProfile();
        $currentRole = Profile::getCurrentRole();
        $roles = Role::all();
        $selRoles = array();
        foreach ($roles as $role)
        {
            $selRoles[$role->name] = $role->id;
        }
        switch ($data['selRoles'])
        {
            case $selRoles[Role\Names::Accountant]:
                $landing_page = '/accountant';
                break;
            case $selRoles[Role\Names::Controller]:
                $landing_page = '/controller';
                break;
            case $selRoles[Role\Names::SalesManager]:
                $landing_page = '/sales';
                break;
            case $selRoles[Role\Names::Salesperson]:
                $landing_page = '/sales';
                break;
            case $selRoles[Role\Names::ServiceManager]:
                $landing_page = '/service';
                break;
            case $selRoles[Role\Names::ServiceWriter]:
                $landing_page = '/service';
                break;
            case $selRoles[Role\Names::Dealer]:
                if ($currentRole->name == Role\Names::Administrator)
                {
                    $landing_page = '/dealer';
                }
                break;
            default :
                $landing_page = '/';
                break;
        }
        if (isset($data['first_name']) && isset($data['last_name']) &&
            isset($data['email']) && isset($data['password']) &&
            isset($data['selRoles']) && isset($currentProfile->dealership_id) &&
            isset($landing_page) && isset($data['title']))
        {
            $user = User::create([
                    'name' => $data['first_name'] . ' ' . $data['last_name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
            ]);
            if (isset($user))
            {
                if (isset($data['selDealerships']))
                {
                    $dealership = $data['selDealerships'];
                }
                else
                {
                    $dealership = $currentProfile->dealership_id;
                }
                $newProfile = Profile::create([
                        'user_id' => $user->id,
                        'role_id' => $data['selRoles'],
                        'dealership_id' => $dealership,
                        'landing_page' => $landing_page,
                        'title' => $data['title'],
                        'first_name' => $data['first_name'],
                        'last_name' => $data['last_name']
                ]);
                if (!isset($newProfile))
                {
                    User::destroy($user->id);
                }
                else
                {
                    $result = true;
                }
            }
        }
        return $result;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        return view('home', compact('currentUser', 'currentRole'));
    }

    /**
     * Show the application registration page.
     *
     * @return \Illuminate\Http\Response
     */
    public function register($message = '')
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $dealerships = null;
        $selDealerships = array();
        if ($currentRole->name != Role\Names::Administrator)
        {
            $limitedRoles = array(Role\Names::Accountant,
                Role\Names::Controller,
                Role\Names::SalesManager,
                Role\Names::Salesperson, Role\Names::Controller,
                Role\Names::ServiceManager,
                Role\Names::ServiceWriter);
            $roles = Role::wherein('name', $limitedRoles)->
                    orderBy('name')->get();
        }
        else
        {
            $roles = Role::orderBy('name')->get();
            $dealerships = \App\Dealership::all();
            foreach($dealerships as $dealership)
            {
                $selDealerships['' . $dealership->id] = $dealership->name;
            }
        }
        $selRoles = array();
        foreach ($roles as $role)
        {
            $selRoles['' . $role->id] = $role->name;
        }
        return view('auth.register', compact('selRoles', 'message',
            'currentUser', 'currentRole', 'selDealerships'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = '';
        $input = $request->input();
        $validator = $this->validator($request->all());
        $fails = $validator->fails();

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $created = $this->create($request->all());
        if ($created)
        {
            $message = 'User created!';
        }
        else
        {
            $message = 'Failed to create user.';
        }
        return $this->register($message);
    }

}
