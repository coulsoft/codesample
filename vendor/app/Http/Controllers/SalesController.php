<?php

namespace App\Http\Controllers;

use App\Dealership;
use App\Http\Controllers\CreditReportController;
use App\Http\Controllers\ExperianController;
use App\Http\Requests;
use App\Profile;
use App\Role;
use App\Sale;
use App\Service;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class SalesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->show(null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $services = Service::where('experian_checked', '=', false)->get();
        foreach ($services as $service)
        {
            $passed = false;
            $credit_quality_score = 0;
            $balance = 0;
            $payments_score = 0;
            $interest_score = 0;
            $payment_score = 0;
            $service->experian_checked = CreditReportController::getCreditScoresByServiceId(
                    $service->id, $passed, $credit_quality_score, $balance, $payments_score, $interest_score, $payment_score);
//            $service->experian_checked = ExperianController::getExperianScoresByServiceId(
//                    $service->id, $passed, $credit_quality_score, $balance, $payments_score, $interest_score, $payment_score);
logger('experian_checked: ' . json_encode($service->experian_checked));
            if ($service->experian_checked == true)
            {
                $service->pass_fail = $passed;
                $service->credit_quality = $credit_quality_score;
                $service->payments = $payments_score;
                $service->interest = $interest_score;
                $service->payment_amount = $payment_score;
                $service->trade_equity = $balance;
                $service->save();
            }
        }
        return response(count($services), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $profile = Profile::getCurrentProfile();
        $dealerships = null;
        $dealership = null;
        $serviceWriters = null;
        $serviceWriterIds = null;
        $clients = null;
        $client = null;
        $salesPeople = null;
        $selSalesPeople = array('0' => 'unassigned');
        $selStatus = Sale::SalesStatus;
        $assignedTo = "Unassigned";

        if ($profile != null)
        {
            $dealership = $profile->dealership_id;
            $serviceWriters = Profile::where('dealership_id', '=', $dealership)->
                    join('roles', 'profiles.role_id', '=', 'roles.id')->
                    wherein('name', array(Role\Names::Administrator,
                        Role\Names::Dealer,
                        Role\Names::ServiceManager,
                        Role\Names::ServiceWriter))->get();
            $salesPeople = Profile::where('dealership_id', '=', $dealership)->
                    join('roles', 'profiles.role_id', '=', 'roles.id')->
                    wherein('roles.name', array(Role\Names::Administrator,
                        Role\Names::Dealer,
                        Role\Names::SalesManager,
                        Role\Names::Salesperson
                    ))->get();
        }
        if (($salesPeople != null) && (count($salesPeople) > 0))
        {
            foreach ($salesPeople as $salesPerson)
            {
                $selSalesPeople['' + $salesPerson->id] = $salesPerson->first_name . ' ' . $salesPerson->last_name;
            }
        }
        if (($serviceWriters != null) && (count($serviceWriters) > 0))
        {
            $serviceWriterIds = array();
            foreach ($serviceWriters as $serviceWriter)
            {
                array_push($serviceWriterIds, $serviceWriter->user_id);
            }
        }
        if (($serviceWriterIds != null) && (count($serviceWriterIds) > 0))
        {
logger("servicWriterIds: " . json_encode($serviceWriterIds));
            $today = \Carbon\Carbon::today();
            $clients = Service::join('sales', 'services.id', '=', 'sales.service_id')->
                whereIn(DB::raw('CONCAT(services.id, sales.created_at)'), Sale::select(DB::raw('CONCAT(service_id, MAX(created_at))'))->
                    groupBy('service_id')->get())->
                whereIn('created_by', $serviceWriterIds)->
                where('experian_checked', '=', true)->
                where('services.created_at', '>=', $today->format('Y-m-d'))->
                join('users', 'services.created_by', '=', 'users.id')->
                select(
                        'services.id as id',
                        'users.name as created_by',
                        'services.first_name as first_name',
                        'services.middle_name as middle_name',
                        'services.last_name as last_name',
                        'services.address as address',
                        'services.city as city',
                        'services.state as state',
                        'services.zip as zip',
                        'services.phone as phone',
                        'services.mileage as mileage',
                        'services.experian_checked as experian_checked',
                        'services.pass_fail as pass_fail',
                        'services.nada_checked as nada_checked',
                        'services.letter_sent as letter_sent',
                        'services.letter_barcode as letter_barcode',
                        'services.credit_quality as credit_quality',
                        'services.trade_equity as trade_equity',
                        'services.payments as payments',
                        'services.interest as interest',
                        'services.payment_amount as payment_amount',
                        'services.year as year',
                        'services.make as make',
                        'services.model as model',
                        'services.body as body',
                        'services.weight as weight',
                        'services.msrp as msrp',
                        'services.trade as trade',
                        'services.score as score',
                        'services.monthly as monthly',
                        'services.balance as balance',
                        'services.remaining as remaining',
                        'services.original as original',
                        'services.term as term',
                        'services.percent_interest as percent_interest',
                        'sales.created_at as created_at',
                        'sales.sales_status',
                        'sales.assigned_to'
                    )->orderBy('created_at', 'DESC')->
                get();
logger("clients: " . json_encode($clients));
            if (($clients != null) && (count($clients) > 0) && (isset($id)) &&
                ($id != null))
            {
                foreach($clients as $_client)
                {
                    if ($_client->id == $id)
                    {
                        $client = $_client;
                        break;
                    }
                }
            }
            
            if (($client != null) && ($client->assigned_to != 0))
            {
                $assigned_to = User::find($client->assigned_to);
                if (isset($assigned_to) && ($assigned_to != null))
                {
                    $assignedTo = $assigned_to->name;
                }
            }
        }
        return view('sales.sales2', compact('clients', 'selSalesPeople', 'selStatus', 'currentUser', 'currentRole', 'client', 'assignedTo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $assignedTo = (isset($input['selSalesPeople']) ? 
            $input['selSalesPeople'] : null);
        if (($assignedTo == null) && isset($input['assigned_to']))
        {
            $assignedTo = $input['assigned_to'];
        }
        $salesStatus = (isset($input['selStatus'])) ?
            $input['selStatus'] : null;
        if (($salesStatus == null) && isset($input['sales_status']))
        {
            $salesStatus = $input['sales_status'];
        }
        if (($assignedTo != null) && ($salesStatus != null))
        {
            Sale::create(['service_id' => $id,
                'sales_status' => $salesStatus + 1,
                'assigned_to' => $assignedTo]);
        }
        return redirect(url('/sales', [], true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('You have reached the delete page.');
    }

}
