<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->create();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($message = null)
    {
        $roles = Role::all();
        $selRoles = array();
        foreach ($roles as $role)
        {
            $selRoles['' . $role->id] = $role->name;
        }
        $users = User::all();
        $selUsers = array();
        $selUsers['0'] = 'Promo (Unregistered)';
        foreach ($users as $user)
        {
            $selUsers['' . $user->id] = $user->name;
        }
        $profiles = DB::table('profiles')->
                join('users', 'profiles.user_id', '=', 'users.id')->
                join('roles', 'profiles.role_id', '=', 'roles.id')->
                select('profiles.id', 'users.name', 'users.email', 'roles.name as role', 'profiles.*')->get();
        return view('auth.profiles', compact('profiles', 'selRoles', 'selUsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $message = null;
        $role = null;
        if (array_has($input, 'selRoles'))
        {
            $role = Role::find($input['selRoles']);
        }
        $user = null;
        if (array_has($input, 'selUsers') && ($input['selUsers'] != 0))
        {
            $user = User::find($input['selUsers']);
        }
        else
        {
            $user = (object) array('id' => '0', 'name' => 'Promo', 'email' => 'contact@servisell.com');
        }
        if ($role != null)
        {
            Profile::create([
                'user_id' => $user->id,
                'role_id' => $role->id,
                'landing_page' => ((isset($input['landing_page'])) ? $input['landing_page'] : ''),
                'title' => ((isset($input['title'])) ? $input['title'] : ''),
                'first_name' => ((isset($input['first_name'])) ? $input['first_name'] : ''),
                'last_name' => ((isset($input['last_name'])) ? $input['last_name'] : '')
//                'last_name' => ((isset($input['last_name'])) ? $input['last_name'] : ''),
//                'dealership' => ((isset($input['dealership'])) ? $input['dealership'] : ''),
//                'address_line1' => ((isset($input['address_line1'])) ? $input['address_line1'] : ''),
//                'address_line2' => ((isset($input['address_line2'])) ? $input['address_line2'] : ''),
//                'city' => ((isset($input['city'])) ? $input['city'] : ''),
//                'state' => ((isset($input['state'])) ? $input['state'] : ''),
//                'zip' => ((isset($input['zip'])) ? $input['zip'] : ''),
//                'phone1' => ((isset($input['phone1'])) ? $input['phone1'] : ''),
//                'phone2' => ((isset($input['phone2'])) ? $input['phone2'] : '')
            ]);
            $message = 'Profile for, ' . $user->name . ', created.';
        }
        $this->create($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
