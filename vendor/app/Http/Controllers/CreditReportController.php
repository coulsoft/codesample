<?php

namespace App\Http\Controllers;

use Auth;
use App\Dealership;
use App\Http\Requests;
use App\Profile;
use App\Service;
use Crypt;
use Illuminate\Http\Request;

class CreditReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->create();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        return view('credit.credit_report', compact('currentUser', 'currentRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        $input = $request->input();
        $name = isset($input['name']) ? $input['name'] : null;
        $address = isset($input['address']) ? $input['address'] : null;
        $city = isset($input['address']) ? $input['city'] : null;
        $state = isset($input['state']) ? $input['state'] : null;
        $zip = isset($input['zip']) ? $input['zip'] : null;
        $email = isset($input['email']) ? $input['email'] : null;
        $phone = isset($input['phone']) ? $input['phone'] : null;
        $account = 'servisellqs'; // move to .env
        $password = 'Test1234'; // encrypt and move to .env
        $body = 'ACCOUNT=' . $account .
            '&PASSWD=' . $password .
            '&pass=2&process=PCCREDIT&product=PRESCREEN' .
            '&email=' . $email .
            '&NAME=' . $name .
            '&ADDRESS=' . $address .
            '&city=' . $city .
            '&state=' . $state .
            '&zip=' . $zip;
        $serviceURL = 'https://www.700CreditSolution.com/XCRS/Service.aspx'; // move to .env
        $response = \Httpful\Request::post($serviceURL)->body($body)->send();
        $parsedXml = null;
        if (isset($response->body))
        {
            $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $response);
            $parsedXml = simplexml_load_string($clean_xml);
        }
        $xmlReport = null;
        if (($parsedXml != null) && (isset($parsedXml->XML_Report)))
        {
            $xmlReport = $parsedXml->XML_Report;
        }
        $prescreenReport = null;
        if (($xmlReport != null) && (isset($xmlReport->Prescreen_Report)))
        {
            $prescreenReport = $xmlReport->Prescreen_Report;
        }
        $resultCode = null;
        if (($prescreenReport != null) && (isset($prescreenReport->ResultCode)))
        {
            $resultCode = $prescreenReport->ResultCode;
        }

        return response(compact('serviceURL', 'body', 'input', 'currentUser', 'currentRole', 'prescreenReport'));

        return view('credit.credit_report', compact('response', 'currentUser', 'currentRole'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the servcie record using the given id then calculate the scores using
     * the data returned from Experian.
     * 
     * @param   int $id
     * @return  boolean True if passed, otherwise false
     * @out     $credit_quality: score 1-4
     *          $balance: actual
     *          $remaining: score 1-4
     *          $interest: score 1-4
     *          $payment: score 1-4
     */
    public static function getCreditScoresByServiceId($id, &$passed, &$credit_quality_score, &$balance, &$payments_score, &$interest_score, &$payment_score)
    {
logger('Begin getCreditScoresByServiceId'); return false;
        $service = Service::find($id);
        $result = false;

        if ($service != null)
        {
            $dealership = null;
            $postBody = null;
            $soapResponse = null;
            $parsedXml = null;
            $xmlReport = null;
            $prescreenReport = null;
logger('Before Profile Check');
            $profile = Profile::getCurrentProfile();
            $password = null;
            $current_balance = null;

logger('profile' . json_encode(profile));
            if ($profile != null)
            {
                $dealership = Dealership::find($profile->dealership_id);
            }
            if ($dealership != null)
            {
                try
                {
                    $password = Crypt::decrypt($dealership->credit_password);
                } catch (\Exception $ex)
                {
                    logger('ExperianController.getExperianScoresByServiceId: ' .
                        'Failed to decrypt dealership Experian Password for dealership id #' .
                        $dealership->id);
                }
            }
            if ($password != null)
            {
                /*
                 * Temporary hard coded credentials
                 */
                $postBody = 'ACCOUNT=' . $dealership->credit_username .
                    '&PASSWD=' . $password .
                    '&pass=2&process=PCCREDIT&product=PRESCREEN' .
                    '&email=' . $service->email .
                    '&NAME=' . $service->first_name . ' ' . $service->last_name .
                    '&ADDRESS=' . $service->address .
                    '&city=' . $service->city .
                    '&state=' . $service->state .
                    '&zip=' . $service->zip;
logger('postBody = ' . $postBody);

                $serviceURL = 'https://www.700CreditSolution.com/XCRS/Service.aspx'; // move to .env
logger('serviceURL: ' . $serviceURL);
                $soapResponse = \Httpful\Request::post($serviceURL)->body($postBody)->send();
logger(json_encode($soapResponse));
            }

            if (isset($soapResponse->body))
            {
                $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponse);
                $parsedXml = simplexml_load_string($clean_xml);
            }
            if (($parsedXml != null) && (isset($parsedXml->XML_Report)))
            {
logger("parsedXml: " . json_encode($parsedXml));
                $xmlReport = $parsedXml->XML_Report;
            }
            if (($xmlReport != null) && (isset($xmlReport->Prescreen_Report)))
            {
                $prescreenReport = $xmlReport->Prescreen_Report;
            }
            $status = null;
            $error = null;

            if (($prescreenReport != null) && (isset($prescreenReport->ResultCode)))
            {
                $status = $prescreenReport->ResultCode;
                $error = $prescreenReport->ResultDescription;
            }
            $passed = false;
            $score = null;
            $monthly = null;
            $balance = null;
            $remaining = null;
            $original = null;
            $term = null;
            $interest = null;
            $tradeLines = array();
    
            if (($status != null) && ($status >= 'A') && ($status <= 'Z'))
            {
                $result = true;
                $passed = ($status == 'A');
                $score = isset($prescreenReport->Score) ?
                    str_replace('-', '', str_replace('+', '', $prescreenReport->Score)) : null;
                $autoTradeLines = null;
                if (isset($prescreenReport->AutoTradeLines))
                {
                    $autoTradeLines = $prescreenReport->AutoTradeLines;
                }
                if (($autoTradeLines != null) &&
                    (isset($autoTradeLines->AutoTradeLine1)))
                {
                    $autoTradeLine = $autoTradeLines->AutoTradeLine1;
                    $monthly = isset($autoTradeLine->MonthlyPayment) ?
                        floatval(str_replace(',', '',
                            str_replace('$', '', $autoTradeLine->MonthlyPayment))): null;
                    $current_balance = isset($autoTradeLine->EstimatedPayoff) ?
                        floatval(str_replace(',', '',
                            str_replace('$', '', $autoTradeLine->EstimatedPayoff))): null;
                    $remaining = isset($autoTradeLine->RemainingTerms) ?
                        intval($autoTradeLine->RemainingTerms) : null;
                    $original = isset($autoTradeLine->OriginalAmount) ?
                        floatval(str_replace(',', '',
                            str_replace('$', '', $autoTradeLine->OriginalAmount))): null;
                    
                    $term = isset($autoTradeLine->OriginalTerms) ?
                        intval($autoTradeLine->OriginalTerms) : null;
                    $interest = (isset($autoTradeLine->InterestRate)) ?
                        floatval($autoTradeLine->InterestRate) : null ;
                }
                $service->score = $score;
                $service->monthly = $monthly;
                $service->balance = $current_balance;
                $service->remaining = $remaining;
                $service->original = $original;
                $service->term = $term;
                $service->percent_interest = $interest;
                $service->save();
            }
            if ($score != null)
            {
                $credit_quality_score = ($score >= 760 ) ? 4 :
                    (($score >= 700) ? 3 :
                        (($score >= 640) ? 2 : 1));
            }
            if ($monthly != null)
            {
                $payment_score = ($monthly >= 750) ? 4 :
                    (($monthly >= 500) ? 3 :
                        (($monthly >= 300) ? 2 : 1));
            }
            $vehicleAndValues = NADAController::getVehicleAndValues(
                    $service->vin, $service->mileage, $dealership->region);
            if (isset($vehicleAndValues->TradeInPlusVinAccMileage))
            {
                $service->year = $vehicleAndValues->VehicleYear;
                $service->make = $vehicleAndValues->MakeDescr;
                $service->model = $vehicleAndValues->SeriesDescr;
                $service->body = $vehicleAndValues->BodyDescr;
                $service->weight = $vehicleAndValues->Weight;
                $service->msrp = $vehicleAndValues->Msrp;
                $service->trade = $vehicleAndValues->TradeInPlusVinAccMileage;
                $service->save();
            }
            if ($current_balance != null)
            {
                $balance = 0;
                if (isset($vehicleAndValues->TradeInPlusVinAccMileage))
                {
                    $difference = $current_balance -
                        $vehicleAndValues->TradeInPlusVinAccMileage;
                    $balance = ($difference > 5000) ? 4 :
                        (($difference > 2500) ? 3 :
                            (($difference > 1000) ? 2 : 1));
                }
            }
            if (($remaining != null) && ($term != null) && ($term > 0))
            {
                $payments_made = $remaining / $term;
                $payments_score = ($payments_made > 0.75) ? 4 :
                    (($payments_made > 0.50) ? 3 :
                        (($payments_made > 0.25) ? 2 : 1));
            }
            if ($interest != null)
            {
                $interest_score = ($interest > 10.0) ? 4 :
                    (($interest > 7.5) ? 3 :
                        (($interest > 5.0) ? 2 : 1));
            }
        }

        return $result;
    }

}
