<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users',
            'name' => 'required|max:255',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'dealership' => 'required|max:255',
            'address1' => 'required|max:255',
            'city' => 'required|max:255',
            'state' => 'required|max:2|in:AL,AK,AZ,AR,CA,CO,CT,DE,DC,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY',
            'zip' => 'required|regex:/^[0-9]{5}(?:-[0-9]{4})?$/',
            ], ['phone.regex' => 'Enter a valid phone number.',
            'zip.regex' => 'Enter a valid zip code']);
        $input = $request->input();
        $email = (isset($input['email'])) ? $input['email'] : null;
        $name = (isset($input['name'])) ? $input['name'] : null;
        $phone = (isset($input['phone'])) ? $input['phone'] : null;
        $dealership = (isset($input['dealership'])) ? $input['dealership'] : null;
        $address1 = (isset($input['address1'])) ? $input['address1'] : null;
        $address2 = (isset($input['address2'])) ? $input['address2'] : null;
        $city = (isset($input['city'])) ? $input['city'] : null;
        $state = (isset($input['state'])) ? $input['state'] : null;
        $zip = (isset($input['zip'])) ? $input['zip'] : null;

        if (isset($email))
        {
            $subject = 'New Prospect - ' . $name . ' from ' . $dealership;
            $message = 'Name: ' . $name . "\n" .
                'E-Mail: ' . $email . "\n" .
                'Phone: ' . $phone . "\n" .
                'Dealership: ' . $dealership . "\n" .
                'Address Line 1: ' . $address1 . "\n" .
                'Address Line 2: ' . $address2 . "\n" .
                'City: ' . $city . "\n" .
                'State: ' . $state . "\n" .
                'Zip: ' . $zip . "\n";

            $senderName = bin2hex($name);
            $senderEMail = bin2hex($email);
            $strSubject = bin2hex($subject);
            $strMessage = bin2hex($message);
            $t = time();
            $strDate = date("Y-m-d H:i:s", $t);

            if (mail("scott.coulson@coulsoft.com", $subject, $message, "From: $email"))
            {
                $response = view('email.thankyou');
            } else
            {
                $response = view('email.error');
            }
        } else
        {
            $response = redirect()->back();
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
