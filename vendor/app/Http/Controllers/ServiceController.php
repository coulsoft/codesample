<?php

namespace App\Http\Controllers;

use App\Dealership;
use App\Profile;
use App\Sale;
use App\Service;
use Auth;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $currentRole = Profile::getCurrentRole();
        logger("Start ServiceController.index");
        return view('service.service2', compact('currentUser', 'currentRole'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notice()
    {
        return view('service.notice');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'state' => 'required|max:2|in:AL,AK,AZ,AR,CA,CO,CT,DE,DC,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY',
            'zip' => 'required|regex:/^[0-9]{5}(?:-[0-9]{4})?$/',
            'vin' => 'required',
            'mileage' => 'required|regex:/^[0-9]+$/'
            ], ['phone.regex' => 'Enter a valid phone number.',
            'zip.regex' => 'Enter a valid zip code']);
        $input = $request->input();
        $createdBy = Auth::user();

        $result = Service::create(array('created_by' => $createdBy->id,
            'first_name' => $input['first_name'],
            'middle_name' => $input['middle_name'],
            'last_name' => $input['last_name'],
            'address' => $input['address'],
            'city' => $input['city'],
            'state' => $input['state'],
            'zip' => $input['zip'],
            'phone' => $input['phone'],
            'vin' => $input['vin'],
            'mileage' => $input['mileage']));
        Sale::create(array('service_id' => $result->id));
        $message = 'Customer information entered.';
        return view('service.notice', compact('result', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
