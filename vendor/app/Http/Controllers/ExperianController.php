<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dealership;
use App\Http\Requests;
use App\Profile;
use App\Service;
use Crypt;

class ExperianController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('experian.experian');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dealership = null;
        $soapBody = null;
        $soapResponse = null;
        $token = null;
        $soapResponseBody = null;
        $parsedXml = null;
        $input = $request->input();
        $profile = Profile::getCurrentProfile();
        $creditPassword = null;

        if ($profile != null)
        {
            $dealership = Dealership::find($profile->dealership_id);
        }
        if ($dealership != null)
        {
            try
            {
                $creditPassword = Crypt::decrypt($dealership->credit_password);
            } catch (\Exception $ex)
            {
                logger('ExperianController.getExperianScoresByServiceId: ' .
                    'Failed to decrypt dealership Experian Password for dealership id #' .
                    $dealership->id);
            }
        }
        if ($creditPassword != null)
        {
            $soapBody = '<?xml version="1.0"?>' .
                    '<data_area>' .
                    '<header_data>' .
                    '<user_id>bsd' . $dealership->credit_username . '</user_id>' .
                    '<credit_guid></credit_guid>' .
                    '<app_id></app_id>' .
                    '<cus_id>' . $dealership->credit_username . '</cus_id>' .
                    '<user_pwd>' . $creditPassword . '</user_pwd>' .
                    '<xml_format>1</xml_format>' .
                    '<lookback>0</lookback>' .
                    '<pre_qual>0</pre_qual>' .
                    '<action>XIP</action>' .
                    '<deal_status>Pending</deal_status>' .
                    '</header_data>' .
                    '<applicant_data>' .
                    '<applicant type="primary">' .
                    '<person_name>' .
                    '<first_name>' . strtoupper($input['first_name']) . '</first_name>' .
                    '<middle_name>' . strtoupper($input['middle_name']) . '</middle_name>' .
                    '<last_name>' . strtoupper($input['last_name']) . '</last_name>' .
                    '<salutation></salutation>' .
                    '</person_name>' .
                    '<social></social>' .
                    '<birthdate></birthdate>' .
                    '<address_data>' .
                    '<address type="current">' .
                    '<line_one>' . strtoupper($input['address']) . '</line_one>' .
                    '<city>' . strtoupper($input['city']) . '</city>' .
                    '<state_or_province>' . $input['state'] . '</state_or_province>' .
                    '<postal_code>' . $input['zip'] . '</postal_code>' .
                    '<period_of_residence period="months">0</period_of_residence>' .
                    '<housing_status>4</housing_status>' .
                    '</address>' .
                    '</address_data>' .
                    '</applicant>' .
                    '</applicant_data>' .
                    '</data_area>';
            $soapResponse = \Httpful\Request::post('https://dev.cbcecredit.com/capp/bsdPost.php')->body($soapBody)->send();
        }
        if (($soapResponse != null) && isset($soapResponse->code) && ($soapResponse->code == 200))
        {
            $soapResponseBody = $soapResponse->body;
        }
        if ($soapResponseBody != null)
        {
            $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponseBody);
            $parsedXml = simplexml_load_string($clean_xml);
        }
        $status = null;
        $error = null;
        $prescreen_result = null;
        if ($parsedXml !== false)
        {
            $status = $parsedXml->STATUS;
            $error = $parsedXml->ERROR;
            $prescreen_result = $parsedXml->INSTANT_PRESCREEN->RESULT;
        }
        $passed = false;
        if (($status != null) && ($status == '1') &&
                ($error != null) && (strtoupper($error) == 'FALSE') &&
                ($prescreen_result != null) && (strtoupper($prescreen_result) == 'PASS'))
        {
            $passed = true;
            $score = $parsedXml->INSTANT_PRESCREEN->SCORES->SCORE;
            $payment = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCP->VALUE)) ?
                    $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCP->VALUE : null;
            $balance = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCB->VALUE)) ?
                    $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCB->VALUE : null;
            $remaining = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCM->VALUE)) ?
                    $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCM->VALUE : null;
            $original = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCO->VALUE)) ?
                    $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCO->VALUE : null;
            $term = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCT->VALUE)) ?
                    $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCT->VALUE : null;
            $interest = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCX->VALUE)) ?
                    $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCX->VALUE : null;
        }
        return response(compact('input', 'soapBody', 'soapResponseBody', 'parsedXml', 'status', 'error', 'prescreen_result', 'passed', 'score', 'payment', 'balance', 'remaining', 'original', 'term', 'interest'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the servcie record using the given id then calculate the scores using
     * the data returned from Experian.
     * 
     * @param   int $id
     * @return  boolean True if passed, otherwise false
     * @out     $credit_quality: score 1-4
     *          $balance: actual
     *          $remaining: score 1-4
     *          $interest: score 1-4
     *          $payment: score 1-4
     */
    public static function getExperianScoresByServiceId($id,
            &$passed, &$credit_quality_score, &$balance, &$payments_score,
            &$interest_score, &$payment_score)
    {
        $service = Service::find($id);
        $result = false;

        if ($service != null)
        {
            $dealership = null;
            $soapBody = null;
            $soapResponse = null;
            $token = null;
            $soapResponseBody = null;
            $parsedXml = null;
            $profile = Profile::getCurrentProfile();
            $creditPassword = null;
            $current_balance = null;

            if ($profile != null)
            {
                $dealership = Dealership::find($profile->dealership_id);
            }
            if ($dealership != null)
            {
                try
                {
                    $creditPassword = Crypt::decrypt($dealership->credit_password);
                } catch (\Exception $ex) {
                    logger('ExperianController.getExperianScoresByServiceId: ' .
                        'Failed to decrypt dealership Experian Password for dealership id #' . 
                        $dealership->id);
                }
            }
            if ($creditPassword != null)
            {
                $soapBody = '<?xml version="1.0"?>' .
                        '<data_area>' .
                        '<header_data>' .
                        '<user_id>bsd' . $dealership->credit_username . '</user_id>' .
                        '<credit_guid></credit_guid>' .
                        '<app_id></app_id>' .
                        '<cus_id>' . $dealership->credit_username . '</cus_id>' .
                        '<user_pwd>' . $creditPassword . '</user_pwd>' .
                        '<xml_format>1</xml_format>' .
                        '<lookback>0</lookback>' .
                        '<pre_qual>0</pre_qual>' .
                        '<action>XIP</action>' .
                        '<deal_status>Pending</deal_status>' .
                        '</header_data>' .
                        '<applicant_data>' .
                        '<applicant type="primary">' .
                        '<person_name>' .
                        '<first_name>' . strtoupper($service->first_name) . '</first_name>' .
                        '<middle_name>' . strtoupper($service->middle_name) . '</middle_name>' .
                        '<last_name>' . strtoupper($service->last_name) . '</last_name>' .
                        '<salutation></salutation>' .
                        '</person_name>' .
                        '<social></social>' .
                        '<birthdate></birthdate>' .
                        '<address_data>' .
                        '<address type="current">' .
                        '<line_one>' . strtoupper($service->address) . '</line_one>' .
                        '<city>' . strtoupper($service->city) . '</city>' .
                        '<state_or_province>' . $service->state . '</state_or_province>' .
                        '<postal_code>' . $service->zip . '</postal_code>' .
                        '<period_of_residence period="months">0</period_of_residence>' .
                        '<housing_status>4</housing_status>' .
                        '</address>' .
                        '</address_data>' .
                        '</applicant>' .
                        '</applicant_data>' .
                        '</data_area>';
                $soapResponse = \Httpful\Request::post('https://dev.cbcecredit.com/capp/bsdPost.php')->body($soapBody)->send();
            }

            if (($soapResponse != null) && isset($soapResponse->code) && ($soapResponse->code == 200))
            {
                $soapResponseBody = $soapResponse->body;
            }
            if ($soapResponseBody != null)
            {
                $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $soapResponseBody);
                $parsedXml = simplexml_load_string($clean_xml);
            }
            $status = null;
            $error = null;
            $prescreen_result = null;
            if (($parsedXml !== false) && isset($parsedXml->STATUS) &&
                isset($parsedXml->ERROR) && isset($parsedXml->INSTANT_PRESCREEN->RESULT))
            {
                $status = $parsedXml->STATUS;
                $error = $parsedXml->ERROR;
                $prescreen_result = $parsedXml->INSTANT_PRESCREEN->RESULT;
            }
            $passed = false;
            $score = null;
            $monthly = null;
            $balance = null;
            $remaining = null;
            $original = null;
            $term = null;
            $interest = null;
            if (($status != null) && ($status == '1') &&
                    ($error != null) && (strtoupper($error) == 'FALSE'))
            {
                $result = true;
                $passed =  (($prescreen_result != null) && (strtoupper($prescreen_result) == 'PASS'));
                $score = isset($parsedXml->INSTANT_PRESCREEN->SCORES->SCORE) ?
                        $parsedXml->INSTANT_PRESCREEN->SCORES->SCORE : null;
                $monthly = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCP->VALUE)) ?
                        $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCP->VALUE : null;
                $current_balance = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCB->VALUE)) ?
                        $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCB->VALUE : null;
                $remaining = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCM->VALUE)) ?
                        $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCM->VALUE : null;
                $original = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCO->VALUE)) ?
                        $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCO->VALUE : null;
                $term = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCT->VALUE)) ?
                        $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCT->VALUE : null;
                $interest = (isset($parsedXml->INSTANT_PRESCREEN->EIRC->EIRCX->VALUE)) ?
                        $parsedXml->INSTANT_PRESCREEN->EIRC->EIRCX->VALUE : null;
                $service->score = $score;
                $service->monthly = $monthly;
                $service->balance = $current_balance;
                $service->remaining = $remaining;
                $service->original = $original;
                $service->term = $term;
                $service->percent_interest = $interest;
                $service->save();
            }
            if ($score != null)
            {
                $credit_quality_score = ($score >= 760 ) ? 4 :
                        (($score >= 700) ? 3 :
                                (($score >= 640) ? 2 : 1));
            }
            if ($monthly != null)
            {
                $payment_score = ($monthly >= 750) ? 4 :
                        (($monthly >= 500) ? 3 :
                        (($monthly >= 300) ? 2 : 1));
            }
            $vehicleAndValues = NADAController::getVehicleAndValues(
                $service->vin, $service->mileage, $dealership->region);
            if (isset($vehicleAndValues->TradeInPlusVinAccMileage))
            {
                $service->year = $vehicleAndValues->VehicleYear;
                $service->make = $vehicleAndValues->MakeDescr;
                $service->model = $vehicleAndValues->SeriesDescr;
                $service->body = $vehicleAndValues->BodyDescr;
                $service->weight = $vehicleAndValues->Weight;
                $service->msrp = $vehicleAndValues->Msrp;
                $service->trade = $vehicleAndValues->TradeInPlusVinAccMileage;
                $service->save();
            }
            if ($current_balance != null)
            {
                $balance = 0;
                if (isset($vehicleAndValues->TradeInPlusVinAccMileage))
                {
                    $difference = $current_balance - 
                            $vehicleAndValues->TradeInPlusVinAccMileage;
                    $balance = ($difference > 5000) ? 4 :
                            (($difference > 2500) ? 3 :
                            (($difference > 1000) ? 2 : 1));
                    
                }
            }
            if (($remaining != null) && ($term != null) && ($term > 0))
            {
                $payments_made = $remaining / $term;
                $payments_score = ($payments_made > 0.75) ? 4 :
                        (($payments_made > 0.50) ? 3 :
                        (($payments_made > 0.25) ? 2 : 1));
            }
            if ($interest != null)
            {
                $interest_score = ($interest > 10.0) ? 4 :
                        (($interest > 7.5) ? 3 :
                        (($interest > 5.0) ? 2 : 1));
            }
        }
        
        return $result;
    }

}
