<?php

namespace App\Role;

abstract class Names
{
    const Administrator = 'administrator';
    const Promo = 'promo';
    const ServiceWriter = 'service writer';
    const Salesperson = 'salesperson';
    const ServiceManager = 'service manager';
    const SalesManager = 'sales manager';
    const Dealer = 'dealer';
    const Accountant = 'accountant';
    const Controller = 'controller';
    const Finance = 'finance';
}

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $fillable = array('name');
    
    static function getRoleIdByName($roleName)
    {
        $result = null;
        
        $roles = Role::where('name', '=', $roleName)->get();
        if (isset($roles) && (count($roles) > 0))
        {
            $result = $roles[0]->id;
        }
        return $result;
    }
    
}
