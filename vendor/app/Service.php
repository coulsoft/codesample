<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $fillable = array('created_by',
        'first_name',
        'middle_name',
        'last_name',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'vin',
        'mileage',
        'experian_checked',
        'pass_fail',
        'nada_checked',
        'letter_sent',
        'letter_barcode',
        'credit_quality',
        'trade_equity',
        'payments',
        'interest',
        'payment_amount',
        'year',
        'make',
        'model',
        'body',
        'msrp',
        'trade',
        'score',
        'monthly',
        'balance',
        'remaining',
        'original',
        'term',
        'interest'
        );

}
