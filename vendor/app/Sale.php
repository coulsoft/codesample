<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    const SalesStatus = ['new', 'interested', 'declined', 'sold'];

    protected $fillable = array('service_id', 'sales_status', 'assigned_to');

}
