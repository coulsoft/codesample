<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealership extends Model
{

    protected $fillable = array('region', 'name', 'address_line1', 'address_line2', 'city', 'state', 'zip');

}
