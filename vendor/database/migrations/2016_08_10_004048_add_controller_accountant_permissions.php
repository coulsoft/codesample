<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddControllerAccountantPermissions extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = Role::getRoleIdByName(Role\Names::Accountant);
        if ($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                            where('url', '=', '/accounting')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/accounting', 'permission', 'CRUD']);
            }
        }
        $id = Role::getRoleIdByName(Role\Names::Controller);
        if ($id != null)
        {
            $permissions = Permission::where('role_id', '=', $id)->
                            where('url', '=', '/controller')->get();
            if (!isset($permissions) || (count($permissions) <= 0))
            {
                Permission::create(['role_id' => $id,
                    'url' => '/controller', 'permission', 'CRUD']);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         $id = Role::getRoleIdByName(Role\Names::Controller);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/controller')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
         $id = Role::getRoleIdByName(Role\Names::Accountant);
         if ($id != null)
         {
             $permissions = Permission::where('role_id', '=', $id)->
                     where('url', '=', '/accounting')->get();
             foreach($permissions as $permission)
             {
                 $permission->delete();
             }
         }
    }

}
