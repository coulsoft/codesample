<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id', false, true)->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('role_id', false, true)->foreign('role_id')->references('id')->on('roles');
            $table->unsignedInteger('dealership_id', false, true)->foreign('dealership_id')->references('id')->on('dealerships');
            $table->string('landing_page');
            $table->uuid('uuid');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
