<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by', false, true)->foreign('created_by')->references('id')->on('users');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->date('address');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('phone');
            $table->string('vin');
            $table->integer('region');
            $table->integer('mileage');
            $table->boolean('experian_checked');
            $table->boolean('pass_fail');
            $table->boolean('nada_checked');
            $table->boolean('letter_sent');
            $table->boolean('letter_barcode');
            $table->integer('credit_quality');
            $table->integer('trade_equity');
            $table->integer('payments');
            $table->integer('interest');
            $table->integer('payment_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service');
    }
}
