<?php

use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateControllerAccountRoles extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (count(Role::where('name', '=', Role\Names::Accountant)->get()) <= 0)
        {
            Role::create(['name' => Role\Names::Accountant]);
        }
        if (count(Role::where('name', '=', Role\Names::Controller)->get()) <= 0)
        {
            Role::create(['name' => Role\Names::Controller]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $roles = Role::where('name', '=', Role\Names::Controller)->get();
        foreach ($roles as $role)
        {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::Accountant)->get();
        foreach ($roles as $role)
        {
            $role->delete();
        }
    }

}
