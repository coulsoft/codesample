<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNadaAndExperianToServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function($table)
        {
            $table->string('year');
            $table->string('make');
            $table->string('model');
            $table->string('body');
            $table->string('weight');
            $table->string('msrp');
            $table->string('trade');
            $table->string('score');
            $table->string('monthly');
            $table->string('balance');
            $table->string('remaining');
            $table->string('original');
            $table->string('term');
            $table->string('percent_interest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function($table)
        {
            $table->dropColumn('year');
            $table->dropColumn('make');
            $table->dropColumn('model');
            $table->dropColumn('body');
            $table->dropColumn('weight');
            $table->dropColumn('msrp');
            $table->dropColumn('trade');
            $table->dropColumn('score');
            $table->dropColumn('monthly');
            $table->dropColumn('balance');
            $table->dropColumn('remaining');
            $table->dropColumn('original');
            $table->dropColumn('term');
            $table->dropColumn('percent_interest');
        });
    }
}
