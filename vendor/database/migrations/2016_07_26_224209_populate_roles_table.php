<?php

use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateRolesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (count(Role::where('name', '=', Role\Names::Administrator)->get()) <= 0) {
            Role::create(['name' => Role\Names::Administrator]);
        }
        if (count(Role::where('name', '=', Role\Names::Promo)->get()) <= 0) {
            Role::create(['name' => Role\Names::Promo]);
        }
        if (count(Role::where('name', '=', Role\Names::ServiceWriter)->get()) <= 0) {
            Role::create(['name' => Role\Names::ServiceWriter]);
        }
        if (count(Role::where('name', '=', Role\Names::Salesperson)->get()) <= 0) {
            Role::create(['name' => Role\Names::Salesperson]);
        }
        if (count(Role::where('name', '=', Role\Names::ServiceManager)->get()) <= 0) {
            Role::create(['name' => Role\Names::ServiceManager]);
        }
        if (count(Role::where('name', '=', Role\Names::SalesManager)->get()) <= 0) {
            Role::create(['name' => Role\Names::SalesManager]);
        }
        if (count(Role::where('name', '=', Role\Names::Dealer)->get()) <= 0) {
            Role::create(['name' => Role\Names::Dealer]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $roles = Role::where('name', '=', Role\Names::Administrator)->get();
        foreach ($roles as $role) {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::Promo);
        foreach ($roles as $role) {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::ServiceWriter);
        foreach ($roles as $role) {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::Salesperson);
        foreach ($roles as $role) {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::ServiceManager);
        foreach ($roles as $role) {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::SalesManager);
        foreach ($roles as $role) {
            $role->delete();
        }
        $roles = Role::where('name', '=', Role\Names::Dealer);
        foreach ($roles as $role) {
            $role->delete();
        }
    }

}
