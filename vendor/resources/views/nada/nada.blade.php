@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Enter NADA Search Criteia</div>

                <div class="panel-body">
                    {!! Form::open(['url' => url('/nada', [], true), 'method' => 'POST']) !!}
                    <div>
                        {!! Form::label('txtVin', 'VIN:  ') !!}
                        {!! Form::text('txtVin', '' ) !!}
                    </div>
                    <div>
                        {!! Form::label('selRegions', 'Select a Region:  ') !!}
                        {!! Form::select('selRegions', $regions) !!}
                    </div>
                    <div>
                        {!! Form::label('txtMileage', 'Mileage:  ') !!}
                        {!! Form::text('txtMileage', '') !!}
                    </div>
                    {!! Form::submit() !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
