@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">NADA Search Results</div>

                <div class="panel-body">
                    @if ($vehicleAndValues != null)
                        VehicleYear:  {{ $vehicleAndValues->VehicleYear }} <br />
                        MakeCode:  {{ $vehicleAndValues->MakeCode }} <br />
                        MakeDescr:  {{ $vehicleAndValues->MakeDescr }} <br />
                        SeriesCode:  {{ $vehicleAndValues->SeriesCode }} <br />
                        SeriesDescr:  {{ $vehicleAndValues->SeriesDescr }} <br />
                        BodyCode:  {{ $vehicleAndValues->BodyCode }} <br />
                        BodyDescr:  {{ $vehicleAndValues->BodyDescr }} <br />
                        Weight:  {{ $vehicleAndValues->Weight }} <br />
                        Msrp:  {{ $vehicleAndValues->Msrp }} <br />
                        AveMileage:  {{ $vehicleAndValues->AveMileage }} <br />
                        MileageAdj:  {{ $vehicleAndValues->MileageAdj }} <br />
                        TradeIn:  {{ $vehicleAndValues->TradeIn }} <br />
                        TradeInPlusVinAcc:  {{ $vehicleAndValues->TradeInPlusVinAcc }} <br />
                        TradeInPlusVinAccMileage:  {{ $vehicleAndValues->TradeInPlusVinAccMileage }} <br />
                        Loan:  {{ $vehicleAndValues->Loan }} <br />
                        LoanPlusVinAcc:  {{ $vehicleAndValues->LoanPlusVinAcc }} <br />
                        LoanPlusVinAccMileage:  {{ $vehicleAndValues->LoanPlusVinAccMileage }} <br />
                        Retail:  {{ $vehicleAndValues->Retail }} <br />
                        RetailPlusVinAcc:  {{ $vehicleAndValues->RetailPlusVinAcc }} <br />
                        RetailPlusVinAccMileage:  {{ $vehicleAndValues->RetailPlusVinAccMileage }} <br />
                        AvgTradeIn:  {{ $vehicleAndValues->AvgTradeIn }} <br />
                        AvgTradeInPlusVinAcc:  {{ $vehicleAndValues->AvgTradeInPlusVinAcc }} <br />
                        AvgTradeInPlusVinAccMileage:  {{ $vehicleAndValues->AvgTradeInPlusVinAccMileage }} <br />
                        RoughTradeIn:  {{ $vehicleAndValues->RoughTradeIn }} <br />
                        RoughTradeInPlusVinAcc:  {{ $vehicleAndValues->RoughTradeInPlusVinAcc }} <br />
                        RoughTradeInPlusVinAccMileage:  {{ $vehicleAndValues->RoughTradeInPlusVinAccMileage }} <br />
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
