<?php
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Full Slider - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/full-slider.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		table.center {
		margin-left:auto; 
		margin-right:auto;
		}
	</style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="images/logo_horizontal.png" /></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="/login">Login</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/car_repair.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Every day you see 50 or more service customers running through your Service Drive 25 OR MORE of those customers are in a favorable buying position and you don’t even know it.</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/alert.jpg');"></div>
                <div class="carousel-caption">
                    <h2>By utilizing the SERVISELL Instant Pre-Screen, Pre-Qualification Sales Alert Tool AMAZING THINGS START TO HAPPEN</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/data.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Using only the service customers Name, Address, VIN and Mileage, NADA and Experian Data are INSTANTLY accessed and organized</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/using_computer.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Within Seconds of Service Department Data Entry your Sales Manager receives a<br/ >
					<br />
					SALES ALERT<br />
					<br />
					Outlining the QUALIFIED SALES OPPORTUNITY</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/treasury.jpg');"></div>
                <div class="carousel-caption">
					<h2>
					ONLY ON &nbsp;&nbsp; Credit-Worthy Customers<br />
					WITH &nbsp;&nbsp; Equity in their service vehicle.<br />
					<br />
					Your Sales Manager INSTANTLY receives the following information:<br />
					<br />
					YEAR, MAKE AND MODEL OF THE SERVICE VEHICLE<br />
					CREDIT QUALITY (You set the minimum credit score to access)<br />
					TRADE EQUITY (Clean Trade less Balance Owed)<br />
					NUMBER OF PAYMENTS MADE<br />
					CURRENT INTEREST RATE ON LOAN<br />
					CURRENT PAYMENT<br />
					</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/seconds.jpg');"></div>
                <div class="carousel-caption">
					<h2>
						Within SECONDS of the Service Manager submitting the data, your Sales Manager knows that there is VEHICLE EQUITY sitting in the Service Drive AND a CREDIT-WORTHY CUSTOMER waiting
					</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/keys.jpg');"></div>
                <div class="carousel-caption">
					<h2>
						With each SALES ALERT, your Sales Manager has the opportunity to:<br />
						<br />
						<p style="left-padding: 8px;">
						Sell the Customer a BETTER VEHICLE<br />
						Take in a QUALITY TRADE<br />
						Improve the CUSTOMERS INTEREST RATE<br />
						Improve the CUSTOMERS PAYMENT<br />
						Increase CUSTOMER SATISFACTION<br />
						</p>
					</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/imagine.jpg');"></div>
                <div class="carousel-caption">
					<h2>
						Imagine this process happening 25 OR MORE times a day.<br />
						Imagine improving your MONTHLY UNIT SALES by<br />
						your AVERAGE DAILY R.O. COUNT<br />
						That’s what SERVISELL can do for YOU<br />
						<br />
						<a href="/contact">Click here</a> to have a SERVISELL representative contact you<br />
						within 24 hours to get the ball rolling.<br />
						<br />
						HAPPY SELLING!!
					</h2>
                </div>
            </div>
		</div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1>Full Slider by Start Bootstrap</h1>
                <p>The background images for the slider are set directly in the HTML using inline CSS. The rest of the styles for this template are contained within the <code>full-slider.css</code>file.</p>
            </div>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
<!--    <script>
    $('.carousel').carousel({
        interval: 7000 //changes the speed
    })
    </script> -->

</body>

</html>
