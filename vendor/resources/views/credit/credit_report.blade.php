@extends('layouts.admin')

@section('content')
<?php
$message = isset($message) ? $message : '';
// ACCOUNT, PASSWD, pass, process, product, email, NAME, ADDRESS, city, state, zip
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Credit Information</div>
                <div class="panel-body">
                    @if(isset($response))
                    <div>
                        It would be here. {{ json_encode($response) }}
                    </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/credit', [], true) }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', 'Name: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('name', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {!! Form::label('address', 'Address: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('address', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            {!! Form::label('city', 'City: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('city', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            {!! Form::label('state', 'State: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('state', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                            {!! Form::label('zip', 'Zip: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('zip', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            {!! Form::label('phone', 'Phone: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('phone', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('email', 'E-Mail: ',
                                ['class' => 'col-md-4 control-label']) !!}
                                <div>
                                    {!! Form::text('email', '',
                                        ['class' => 'col-md-4']) !!}
                                </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <span class='col-md-4'>&nbsp;</span>
                                <div>
                                    {!! Form::submit() !!}
                                </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection