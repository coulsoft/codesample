@extends('layouts.app')

@section('content')
<?php
$message = (isset($message)) ? $message : null;
$clients = (isset($clients)) ? $clients : null;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type='text/javascript'>
    var result = $.post({
        url:"{{ url('/sales', [], true) }}", 
        data:{_token:"{{ csrf_token() }}"},
        success:function(){ 
        console.log('eureeka'); 
    }});
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Sales Opportunities</div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Service Associate</th>
                                    <th style="width: 5%">Credit<br />Quality</th>
                                    <th style="width: 5%">Trade<br />Equity</th>
                                    <th style="width: 5%">Payments</th>
                                    <th style="width: 5%">Interest</th>
                                    <th style="width: 5%">Payment<br />Amount</th>
                                    <th>Assigned To</th>
                                    <th>Time Waiting</th>
                                    <th>Status</th>
                                    <th style="width: 10%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($clients == null)
                                <tr>
                                    <td>*</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                    @foreach($clients as $client)
                                    <tr>
                                        <td>{!! $client->id !!}</td>
                                        <td>{!! $client->first_name !!}</td>
                                        <td>{!! $client->last_name !!}</td>
                                        <td>{!! $client->name !!}</td>
                                        <td><div class="progress">
                                                <div class="progress-bar {!! ($client->credit_quality > 2) ? 'progress-bar-success' : (($client->credit_quality == 2) ? 'progress-bar-warning' : 'progress-bar-danger') !!}" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {!! $client->credit_quality * 25 !!}%;"><span class="sr-only">60% Complete (warning)</span></div>
                                            </div></td>
                                        <td><div class="progress">
                                                <div class="progress-bar {!! ($client->trade_equity > 2) ? 'progress-bar-success' : (($client->trade_equity == 2) ? 'progress-bar-warning' : 'progress-bar-danger') !!}" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {!! $client->trade_equity * 25 !!}%;"><span class="sr-only">60% Complete (warning)</span></div>
                                            </div></td>
                                        <td><div class="progress">
                                                <div class="progress-bar {!! ($client->payments > 2) ? 'progress-bar-success' : (($client->payments == 2) ? 'progress-bar-warning' : 'progress-bar-danger') !!}" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {!! $client->payments * 25 !!}%;"><span class="sr-only">60% Complete (warning)</span></div>
                                            </div></td>
                                        <td><div class="progress">
                                                <div class="progress-bar {!! ($client->interest > 2) ? 'progress-bar-success' : (($client->interest == 2) ? 'progress-bar-warning' : 'progress-bar-danger') !!}" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {!! $client->interest * 25 !!}%;"><span class="sr-only">60% Complete (warning)</span></div>
                                            </div></td>
                                        <td><div class="progress">
                                                <div class="progress-bar {!! ($client->payment_amount > 2) ? 'progress-bar-success' : (($client->payment_amount == 2) ? 'progress-bar-warning' : 'progress-bar-danger') !!}" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {!! $client->payment_amount * 25 !!}%;"><span class="sr-only">60% Complete (warning)</span></div>
                                            </div></td>
                                        <td>{!! Form::select('selSalesPeople', $selSalesPeople) !!}</td>
                                        <td>{!! $client->created_at !!}</td>
                                        <td>{!! $client->sales_status !!}</td>
                                        <td>
                                            {!! Form::open(['url' =>url('/sales', [], true), 'method' => 'post', 'id' => 'form_' . $client->id]) !!}
                                            <a href='javascript:void(0);'
                                               class='glyphicon glyphicon-trash'
                                               onclick='document.getElementById("form_" + {!! $client->id !!}).submit();'></a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
