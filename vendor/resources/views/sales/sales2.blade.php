@extends('layouts.admin')

@section('content-header')
@endsection

@section('content')
<?php
$message = (isset($message)) ? $message : null;
$clients = (isset($clients)) ? $clients : null;
$client = (isset($client)) ? $client : null;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type='text/javascript'>
    var intervalValue = 0;
    function checkNew()
    {
        if (intervalValue > 0)
        {
            clearInterval(intervalValue);
            intervalValue = 0;
        }
        var result = $.post({
            url: "{{ url('/sales', [], true) }}",
            data: {_token: "{{ csrf_token() }}"},
            success: function (asyncResult) {
                if (asyncResult > 0)
                {
                    console.log('Need to reload.');
                    location.reload();
                }
                else
                {
                    console.log('Will check again later.');
                    intervalValue = setInterval(checkNew, 10000);
                }
            }});
    }
    checkNew();
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Sales Opportunities</div>
                <div class="panel-body">
                    @if(($client != null) &&
                    (($currentRole->name == \App\Role\Names::Administrator) ||
                    ($currentRole->name == \App\Role\Names::SalesManager)))
                    <div class="col-lg-12 user-panel">
                        {!! Form::open() !!}
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('sales_status', 'Status: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('sales_status', 
                                    $client->sales_status,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('assigned_to', 'Assigned to: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('assigned_to', 
                                    $assignedTo,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('name', 'Customer Name: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('landing_page', 
                                    $client->first_name . ' ' .
                                    $client->last_name,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('address', 'Address: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('address', 
                                    $client->address,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('city', 'City: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('city', 
                                    $client->city,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('state', 'State: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('state', 
                                    $client->state,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('zip', 'ZIP: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('zip', 
                                    $client->zip,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('phone', 'Phone: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('phone', 
                                    $client->phone,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('mileage', 'Mileage: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('mileage', 
                                    $client->mileage,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('year', 'Year: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('year', 
                                    $client->year,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('make', 'Make: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('make', 
                                    $client->make,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('model', 'Model: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('model', 
                                    $client->model,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('weight', 'Weight: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('weight', 
                                    $client->weight,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('msrp', 'MSRP: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('msrp', 
                                    $client->msrp,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('trage', 'Trade In Value: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('trade', 
                                    $client->trade,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('score', 'Credit Score: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('score', 
                                    $client->score,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <span class="col-md-2"><u><strong>Loan #1:</strong></u></span>
                                <div class="col-md-6">
                                    <hr />
                                </div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="form-group">
                                {!! Form::label('monthly', 'Monthly Payment: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('monthly', 
                                    $client->monthly,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('balance', 'Balance: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('balance', 
                                    $client->balance,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('remaining', 'Remaining: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('remaining', 
                                    $client->remaining,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('original', 'Original Amount: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('original', 
                                    $client->original,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('term', 'Term: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('term', 
                                    $client->term,
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('percent_interest', 'Interest Rate: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('percent_interest', 
                                    $client->percent_interest,
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <span class="col-md-2"><u><strong>Loan #2:</strong></u></span>
                                <div class="col-md-6">
                                    <hr />
                                </div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="form-group">
                                {!! Form::label('monthly', 'Monthly Payment: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('monthly', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('balance', 'Balance: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('balance', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('remaining', 'Remaining: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('remaining', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('original', 'Original Amount: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('original', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('term', 'Term: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('term', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('percent_interest', 'Interest Rate: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('percent_interest', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <span class="col-md-2"><u><strong>Loan #3:</strong></u></span>
                                <div class="col-md-6">
                                    <hr />
                                </div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="form-group">
                                {!! Form::label('monthly', 'Monthly Payment: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('monthly', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('balance', 'Balance: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('balance', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('remaining', 'Remaining: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('remaining', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('original', 'Original Amount: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('original', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('term', 'Term: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('term', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::label('percent_interest', 'Interest Rate: ',
                                ['class' => 'col-md-2 control-label', 'style' => 'text-align: right;']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('percent_interest', 
                                    '',
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @endif
                    <div class="col-lg-12 box">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Service Associate</th>
                                    <th style="width: 5%">Credit<br />Quality</th>
                                    <th style="width: 5%">Trade<br />Equity</th>
                                    <th style="width: 5%">Payments</th>
                                    <th style="width: 5%">Interest</th>
                                    <th style="width: 5%">Payment<br />Amount</th>
                                    <th>Assigned To</th>
                                    <!-- <th>Time Waiting</th> -->
                                    <th>Status</th>
                                    <th style="width: 10%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($clients == null)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                @foreach($clients as $client)
                                <tr>
                                    <td>{!! $client->first_name !!}</td>
                                    <td>{!! $client->last_name !!}</td>
                                    <td>{!! $client->created_by !!}</td>
<!--                                    <td><div class="progress">
                                            <div class="progress-bar {!! ($client->credit_quality > 2) ? 'progress-bar-success' : (($client->credit_quality == 2) ? 'progress-bar-warning' : 'progress-bar-danger') !!}" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {!! $client->credit_quality * 25 !!}%;"><span class="sr-only">60% Complete (warning)</span></div>
                                        </div></td> -->
                                    <td><div class="progress">
                                            <div style="text-align: center; color: white; background-color: {!! ($client->credit_quality > 2) ? 'darkgreen' : (($client->credit_quality == 2) ? 'goldenrod' : 'firebrick') !!}">{!! $client->credit_quality !!}</span></div>
                                        </div></td>
                                    <td><div class="progress">
                                            <div style="text-align: center; color: white; background-color: {!! ($client->trade_equity > 2) ? 'darkgreen' : (($client->trade_equity == 2) ? 'goldenrod' : 'firebrick') !!}">{!! $client->trade_equity !!}</div>
                                        </div></td>
                                    <td><div class="progress">
                                            <div style="text-align: center; color: white; background-color: {!! ($client->payments > 2) ? 'darkgreen' : (($client->payments == 2) ? 'goldenrod' : 'firebrick') !!}">{!! $client->payments !!}</div>
                                        </div></td>
                                    <td><div class="progress">
                                            <div style="text-align: center; color: white; background-color: {!! ($client->interest > 2) ? 'darkgreen' : (($client->interest == 2) ? 'goldenrod' : 'firebrick') !!}">{!! $client->interest !!}</div>
                                        </div></td>
                                    <td><div class="progress">
                                            <div style="text-align: center; color: white; background-color: {!! ($client->payment_amount > 2) ? 'darkgreen' : (($client->payment_amount == 2) ? 'goldenrod' : 'firebrick') !!}">{!! $client->payment_amount !!}</div>
                                        </div></td>
                                    <td>{!! Form::open(['url' => url('/sales/' . $client->id, [], true), 'method' => 'post']) !!}
                                        {{ method_field('PUT') }}
                                        {!! Form::hidden('id', $client->id) !!}
                                        {!! Form::hidden('sales_status', $client->sales_status) !!}
                                        {!! Form::select('selSalesPeople', $selSalesPeople, $client->assigned_to,
                                        ['onchange' => 'this.form.submit()']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                    <!-- <td>{!! date('H:i', $client->created_at->timestamp) !!}</td> -->
                                    <td>{!! Form::open(['url' => url('/sales/' . $client->id, [], true), 'method' => 'post']) !!}
                                        {{ method_field('PUT') }}
                                        {!! Form::hidden('id', $client->id) !!}
                                        {!! Form::hidden('assigned_to', $client->assigned_to) !!}
                                        {!! Form::select('selStatus', $selStatus, array_search($client->sales_status, $selStatus),
                                        ['onchange' => 'this.form.submit()']) !!}
                                        {!! Form::close() !!}</td>
                                    <td><a href="{!! url('/sales/' . $client->id, [], true) !!}"><span class='glyphicon glyphicon-search' /></a></td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
