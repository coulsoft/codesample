@extends('layouts.app')

@section('content')
<?php
$regions = isset($regions) ? $regions : array();
$message = isset($message) ? $message : null;
?>
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Customer Information - Complete</div>
            <div class="panel-body">
                {!! Form::open(['url' => url('/service', [], true),
                'method' => 'GET', 'class' => 'form-horizontal',
                'permission' => 'form']) !!}

                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    {!! Form::label('first_name', 'First Name:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('first_name', $result['first_name'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                    {!! Form::label('middle_name', 'Middle Name:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('middle_name', $result['middle_name'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    {!! Form::label('last_name', 'Last Name:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('last_name', $result['last_name'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    {!! Form::label('address', 'Address:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('address', $result['address'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    {!! Form::label('city', 'City:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('city', $result['city'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>

                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                    {!! Form::label('state', 'State: ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('state', $result['state'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                    {!! Form::label('zip', 'ZIP:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('zip', $result['zip'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    {!! Form::label('phone', 'Phone:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('phone', $result['phone'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('vin') ? ' has-error' : '' }}">
                    {!! Form::label('vin', 'VIN:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('vin', $result['vin'], ['class' => 'form-control', 'disabled'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('mileage') ? ' has-error' : '' }}">
                    {!! Form::label('mileage', 'Mileage:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('mileage', $result['mileage'], ['class' => 'form-control', 'disabled']) !!}
                    </div>
                </div>
                <div class="form-group">
                    &nbsp;
                    <div class="col-md-6">
                        {!! Form::submit('Continue') !!}
                    </div>
                </div>
                <div class="form-group{{ (isset($errors) && (count($errors) > 0)) ? ' has-error' : '' }}">
                    <div class="col-md-6">
                        @if (isset($errors) && (count($errors) > 0))
                        <span class="help-block">
                            <strong>Please provide the required fields, highlighted in red.</strong>
                        </span>
                        @endif  
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
