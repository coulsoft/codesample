@extends('layouts.admin')

@section('content-header')
@endsection

@section('content')
<?php
$regions = isset($regions) ? $regions : array();
$message = isset($message) ? $message : null;
?>
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Customer Information</div>
            <div class="panel-body">
                {!! Form::open(['url' => url('/service', [], true),
                'method' => 'POST', 'class' => 'form-horizontal',
                'permission' => 'form']) !!}

                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    {!! Form::label('first_name', 'First Name:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('first_name', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                    {!! Form::label('middle_name', 'Middle Name:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('middle_name', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    {!! Form::label('last_name', 'Last Name:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('last_name', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    {!! Form::label('address', 'Address:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('address', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    {!! Form::label('city', 'City:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('city', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>

                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                    {!! Form::label('state', 'State: ',
                    ['class' => 'col-md-4 control-label']) !!}

                    <div class="col-md-6">
                        <span class='form-control'>{!! Form::select('state', ['  ' => 'Select a state',
                            'AL' => 'Alabama',
                            'AK' => 'Alaska',
                            'AZ' => 'Arizona',
                            'AR' => 'Arkansas',
                            'CA' => 'California',
                            'CO' => 'Colorado',
                            'CT' => 'Connecticut',
                            'DE' => 'Delaware',
                            'DC' => 'District Of Columbia',
                            'FL' => 'Florida',
                            'GA' => 'Georgia',
                            'HI' => 'Hawaii',
                            'ID' => 'Idaho',
                            'IL' => 'Illinois',
                            'IN' => 'Indiana',
                            'IA' => 'Iowa',
                            'KS' => 'Kansas',
                            'KY' => 'Kentucky',
                            'LA' => 'Louisiana',
                            'ME' => 'Maine',
                            'MD' => 'Maryland',
                            'MA' => 'Massachusetts',
                            'MI' => 'Michigan',
                            'MN' => 'Minnesota',
                            'MS' => 'Mississippi',
                            'MO' => 'Missouri',
                            'MT' => 'Montana',
                            'NE' => 'Nebraska',
                            'NV' => 'Nevada',
                            'NH' => 'New Hampshire',
                            'NJ' => 'New Jersey',
                            'NM' => 'New Mexico',
                            'NY' => 'New York',
                            'NC' => 'North Carolina',
                            'ND' => 'North Dakota',
                            'OH' => 'Ohio',
                            'OK' => 'Oklahoma',
                            'OR' => 'Oregon',
                            'PA' => 'Pennsylvania',
                            'RI' => 'Rhode Island',
                            'SC' => 'South Carolina',
                            'SD' => 'South Dakota',
                            'TN' => 'Tennessee',
                            'TX' => 'Texas',
                            'UT' => 'Utah',
                            'VT' => 'Vermont',
                            'VA' => 'Virginia',
                            'WA' => 'Washington',
                            'WV' => 'West Virginia',
                            'WI' => 'Wisconsin',
                            'WY' => 'Wyoming']) !!}</span>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                    {!! Form::label('zip', 'ZIP:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('zip', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    {!! Form::label('phone', 'Phone:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('phone', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('vin') ? ' has-error' : '' }}">
                    {!! Form::label('vin', 'VIN:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('vin', '', ['class' => 'form-control'] ) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('mileage') ? ' has-error' : '' }}">
                    {!! Form::label('mileage', 'Mileage:  ', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('mileage', '', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    &nbsp;
                    <div class="col-md-6">
                        {!! Form::submit('Submit') !!}
                    </div>
                </div>
                <div class="form-group{{ (isset($errors) && (count($errors) > 0)) ? ' has-error' : '' }}">
                    <div class="col-md-6">
                        @if (isset($errors) && (count($errors) > 0))
                        <span class="help-block">
                            <strong>Please provide the required fields, highlighted in red.</strong>
                        </span>
                        @endif  
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
