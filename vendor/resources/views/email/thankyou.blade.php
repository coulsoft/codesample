@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="css/one-page-wonder.css" rel="stylesheet">

<!-- Full Width Image Header -->
<header class="header-image">
    <div class="headline">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Learn More</div>
                        <div class="panel-body">
                            <div>
                                <p>Thank you.  Your message, including the special link, is on it's way.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
