@extends('layouts.single')

@section('content')
<!-- Custom CSS -->
<link href="css/one-page-wonder.css" rel="stylesheet">

<!-- Full Width Image Header -->
<header class="header-image">
    <div class="headline">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Learn More</div>
                        <div class="panel-body">
                            <div>
                                <p>Due to technical difficulties beyond our control, your message was not sent.</p>
                                <p>Please try again later.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
