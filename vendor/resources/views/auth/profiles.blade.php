@extends('layouts.app')

@section('content')
<?php
$profiles = (isset($profiles)) ? $profiles : null;
$message = (isset($message)) ? $message : null;
$selRoles = (isset($selRoles)) ? $selRoles : array();
$selUsers = (isset($selUsers)) ? $selUsers : array();
?>
<div class="container">
    <div class="row">
        <div >
            <div class="panel panel-default">
                <div class="panel-heading">Create User</div>
                <div class="panel-body">
                    {!! Form::open(['url' => url('/profiles', [], true),
                    'method' => 'POST', 'class' => 'form-horizontal',
                    'permission' => 'form']) !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('selRoles', 'Roles: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::select('selRoles', $selRoles) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('selUsers', 'Users: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::select('selUsers', $selUsers) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('landing_page', 'Landing Page: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('landing_page', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'Title: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('title', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('first_name', 'First Name: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('first_name', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('last_name', 'Last Name: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('last_name', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('dealership', 'Dealership Name: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('dealership', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('address_line1', 'Address Line 1: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('address_line1', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('address_line2', 'Address Line 2: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('address_line2', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('city', 'City: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('city', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('state', 'State Abbreviation: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('state', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('zip', 'Zip: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('zip', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('phone1', 'Phone #1: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('phone1', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('phone2', 'Phone #2: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('phone2', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Create', ['class' => 'btn btn-primary fa fa-btn']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ ($message != null) ? ' has-error' : '' }}">
                        <div class="col-md-6">
                            @if ($message != null)
                            <span class="help-block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>E-Mail</th>
                                    <th>Role</th>
                                    <th>Landing Page</th>
                                    <th>Title</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
<!--                                    <th>Dealership</th>
                                    <th>Phone Numbers</th>
                                    <th>&nbsp;</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                @if($profiles == null)
                                <tr>
                                    <td>*</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                    @foreach($profiles as $profile)
                                    <tr>
                                        <td>{!! $profile->id !!}</td>
                                        <td>{!! $profile->name !!}</td>
                                        <td>{!! $profile->email !!}</td>
                                        <td>{!! $profile->role !!}</td>
                                        <td>{!! $profile->landing_page !!}</td>
                                        <td>{!! $profile->title !!}</td>
                                        <td>{!! $profile->first_name !!}</td>
                                        <td>{!! $profile->last_name !!}</td>
<?php /*
<!--                                        <td>{!! $profile->dealership !!}<br />
                                            {!! // $profile->address_line1 !!}<br />
                                            {!! ((isset($profile->address_line2)) ? $profile->address_line2 . '<br />' : '') !!}
                                            {!! $profile->city !!}, {!! $profile->state !!}&nbsp;&nbsp;{!! $profile->zip !!}</td>
                                        <td>{!! $profile->phone1 !!}<br />
                                            {!! $profile->phone2 !!}</td>
                                        <td>
                                            {!! Form::open(['url' =>url('/profiles/' . $profile->id, [], true), 'method' => 'delete', 'id' => 'form_' . $profile->id]) !!}
                                            <a href='javascript:void(0);'
                                               class='glyphicon glyphicon-trash'
                                               onclick='document.getElementById("form_" + {!! $profile->id !!}).submit();'></a>
                                            {!! Form::close() !!}
                                        </td>-->
 */
?>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
