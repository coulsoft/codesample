@extends('layouts.app')

@section('content')
<?php
$message = (isset($message)) ? $message : null;
$roles = (isset($roles)) ? $roles : null;
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Role</div>
                <div class="panel-body">
                    {!! Form::open(['url' => url('/dealerships', [], true),
                    'method' => 'POST', 'class' => 'form-horizontal',
                    'role' => 'form']) !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Role Name: ',
                        ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('name', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Create', ['class' => 'btn btn-primary fa fa-btn']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ ($message != null) ? ' has-error' : '' }}">
                        <div class="col-md-6">
                            @if ($message != null)
                            <span class="help-block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role Name</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($roles == null)
                                <tr>
                                    <td>*</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @else
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>{!! $role->id !!}</td>
                                        <td>{!! $role->name !!}</td>
                                        <td>
                                            {!! Form::open(['url' =>url('/roles/' . $role->id, [], true), 'method' => 'delete', 'id' => 'form_' . $role->id]) !!}
                                            <a href='javascript:void(0);'
                                               class='glyphicon glyphicon-trash'
                                               onclick='document.getElementById("form_" + {!! $role->id !!}).submit();'></a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
