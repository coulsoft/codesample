@extends('layouts.single')

@section('content')
    <!-- Custom CSS -->
    <link href="css/one-page-wonder.css" rel="stylesheet">
    
    <!-- Full Width Image Header -->
    <header class="header-image">
        <div class="headline">
            <div class="container">
                <h1>SERVISELL</h1>
                <h2>Converting Service Customers to Sales Customers in the blink of an eye…</h2>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">

        <hr class="featurette-divider">

        <!-- First Featurette -->
        <div class="featurette" id="about">
            <img class="featurette-image img-circle img-responsive pull-right" src="images/question.png">
            <h2 class="featurette-heading">Ask yourself this one question:
                <span class="text-muted"> What is my daily average RO count?</span>
            </h2>
        </div>

        <hr class="featurette-divider">

        <!-- Second Featurette -->
        <div class="featurette" id="services">
            <img class="featurette-image img-circle img-responsive pull-left" src="images/smile.png">
            <h2 class="featurette-heading">Now add that number to your total UNIT SALES for the month.
                <span class="text-muted">Imagine how that would feel.</span>
            </h2>
        </div>

        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
            <img class="featurette-image img-circle img-responsive pull-right" src="images/servisell.png">
            <h2 class="featurette-heading">THAT’S WHAT SERVISELL WILL DO FOR YOU! &nbsp;
                <span class="text-muted"><a href="{!! url('/contact') !!}">CLICK HERE TO LEARN MORE</a></span>
            </h2>
        </div>

        <hr class="featurette-divider">

    </div>
    <!-- /.container -->
@endsection